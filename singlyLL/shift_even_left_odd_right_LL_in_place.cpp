// Program to shift even before odd in a singly LL
// Approach: quicksort swapping (Time: O(n) Space: O(1) in place)
#include <iostream>
#include <queue>
using namespace std;

class Node{
public:
  int value;
  Node *next;
  Node(int v);
};
Node::Node(int v){
  value = v;
}

class LL{
public:
  Node *head;
  LL();
  void insertAtTheEnd(int v);
  void printLL();
  void shiftEvenAndOdd();
};

LL::LL(){
  head = NULL;
}

void LL::insertAtTheEnd(int v){
  // allocate node
  Node *newNode = new Node(v);

  // Case 1: empty list
  if(head == NULL){
    head = newNode;
    return;
  }

  // Case 2: 1 or more node
  Node *c = head;
  // Make c point to the last node (after traversal)
  while(c->next != NULL){
    c = c->next;
  }

  // Make the last node point to newNode instead of NULL
  c->next = newNode;
}

void LL::printLL(){
  // Check if empty
  if(head == NULL)
    return;

  Node *c = head;
  while(c != NULL){
    cout << c->value << '\t';
    c = c->next;
  }
  cout << endl;
}

// Time: O(n) Space: inPlace
void LL::shiftEvenAndOdd(){
  if(head == NULL)
    return;

  Node *end = head;
  Node *prev = NULL;
  Node *curr = head;

  // Get pointer to the last node
  while(end->next != NULL){
    end = end->next;
  }

  Node *new_end = end;

  // Move all odd nodes before the 1st even to the end
  while(curr->value % 2 != 0 && curr != end){
    // Make the last node point to the current one (that needs to be shifted to the end)
    new_end->next = curr;

    // Increment current to point to the next node
    curr = curr->next;

    // The node we just moved should point to NULL now instead of the 2nd node of the list
    new_end->next->next = NULL;

    // Increment new_node to point the end of the modifited list
    // (basically new_end points to the node we just moved)
    new_end = new_end->next;
  } 

  // When you have hit the even node (if any)
  // Below we first do an if (not an while) cos 
  // We wanna move head only ONCE when we first find the even node
  if(curr->value % 2 == 0){
    // Change the head pointer to point to the first even node
    head = curr;

    // Now curr pointer to the first even node
    while(curr != end){
      // If its an even node, just skip it
      if(curr->value % 2 == 0){
        prev = curr;
        curr = curr->next;
      }
      else{
        // Pointer to the odd node now
        // Break the connection between prev and current
        // As prev points to the last even node 
        // IMP: and curr pointer to the odd node (which we wanna move to end)
        prev->next = curr->next;

        // Make next of curr as NULL (as we moved it to the end)
        curr->next = NULL;

        // Make the previous lastNode to point to the new lastNode
        new_end->next = curr;

        // Make curr(which is the node we are moving to end) new end of the list
        new_end = curr;

        // Update the current pointer to point to next of the moved node
        curr = prev->next;
      }
    }
  }

  else
    prev = curr;

  /* If there are more than 1 odd nodes and end of original list is
    odd then move this node to end to maintain same order of odd
    numbers in modified list */
  if(new_end != end && (end->value) % 2 != 0){
    prev->next = end->next;
    end->next  = NULL;
    new_end->next = end;
  }
}

int main(){
  LL *list = new LL();

  list->insertAtTheEnd(17);
  list->insertAtTheEnd(15);
  list->insertAtTheEnd(8);
  list->insertAtTheEnd(11);
  list->insertAtTheEnd(10);
  list->insertAtTheEnd(7);
  list->insertAtTheEnd(2);
  list->insertAtTheEnd(4);

  
  list->printLL();
  cout << "shift even to left and odd to right: " << endl;
  list->shiftEvenAndOdd();
  list->printLL();

  return 0;
}
