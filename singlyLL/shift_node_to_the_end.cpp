#include <iostream>
using namespace std;

class Node{
public:
  int value;
  Node *next;
  Node();
  Node(int v);
};
Node::Node(){
  next = NULL;
}
Node::Node(int v){
  value = v;
}

class LL{
public:
  Node *head;
  LL();
  void insertAtTheEnd(int v);
  void printLL();
  void shiftFirstNodeToTheEnd();
};
LL::LL(){
  head = NULL;
}
void LL::insertAtTheEnd(int v){
  // Make a new node
  Node *newNode = new Node(v);

  // Case 1: 0 node
  if(head == NULL){
    head = newNode;
    return;
  }

  // Case 2: 1 or more nodes
  Node *c = head;
  // Traverse c so that it points to the last node
  while(c->next != NULL){
    c = c->next;
  }

  // Make the last node point to newNode instead of null
  c->next = newNode;
}

void LL::printLL(){
  // Case 1: 0 node 
  if(head == NULL){
    cout << "sorry empty list" << endl;
    return;
  }

  Node *c = head;
  while(c != NULL){
    cout << c->value << '\t';
    c = c->next;
  }
  cout << endl;
}

void LL::shiftFirstNodeToTheEnd(){
  // Case 1: empty list
  if(head == NULL)
    cout << "sorry empty list, cant move node to the end" << endl;

  // Case 2: 1 node in the list
  else if(head->next == NULL)
    cout << "sorry just 1 node in the list so cant move" << endl;

  // Case 3: More than 1 nodes
  else{
    // Make the last node point to the first node instead of NULL
    Node *l = head;
    while(l->next != NULL){
      l = l->next;
    }
    // So now l points to the last node
    l->next = head;
    // Get a temp pointer to point to the first node
    Node *t = head;
    // Make head point to the 2nd node(as it will be the first node now)
    head = head->next;
    // Make the first node point to NULL (as it will be the last node now)
    t->next = NULL;
  }
}

int main(){
  LL *l = new LL();
  l->printLL();
  l->insertAtTheEnd(1);
  l->insertAtTheEnd(2);
  l->insertAtTheEnd(3);
  l->insertAtTheEnd(4);
  l->insertAtTheEnd(5);
  cout << "original list" << endl;
  l->printLL();

  cout << "Shifting the first node to the end of the list" << endl;
  for (int i = 0 ; i < 10; i++){
    l->shiftFirstNodeToTheEnd();
    l->printLL();
  }
  return 0;
}
