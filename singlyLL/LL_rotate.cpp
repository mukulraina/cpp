/*
  Program to rotate a LL
  input: 10->20->30->40->50->60 and k = 4
  output:  50->60->10->20->30->40
*/
#include <iostream>
#include <stack>
using namespace std;

// Node Class
class Node{
  public:
    int c;
    Node *next;
    Node(int c);
    Node();
};
Node::Node(int c){
  this->c = c;
  next = NULL;
}
Node::Node(){
  next = NULL;
}

// List class
class List{
  public:
    Node *head;
    List();
    void printList();
    void append(int c);
    bool isEmpty();
    void rotate(int k);
};

List::List(){
  head = NULL;
}

bool List::isEmpty(){
  return head == NULL;
}

void List::append(int c){
  Node *newNode = new Node(c);

  if(isEmpty()){
    head = newNode;
    return;
  }

  if(head->next == NULL){
    head->next = newNode;
    return;
  }

  Node *current = head;
  while(current->next != NULL){
    current = current->next;
  }
  current->next = newNode;
}

void List::printList(){
  // Case 1: empty list
  if(isEmpty())
    return;

  // Case 2: 1 node
  if(head->next == NULL)
    cout << head->c << endl;

  // Case 3: Normal Case
  Node *current = head;
  while(current != NULL){
    cout << current->c << '\t';
    current = current->next;
  }
  cout << endl;
}

/*
  - so go to the kth node + make its setNext = null
  - make head point to the k + 1 node
  - make the last node point to the kth node
  - Time: O(n) Space: O(1)
*/
void List::rotate(int k){
  if(isEmpty())
    return;

  int i = 1;
  Node *originalHead = head;
  Node *current = head;
  while(i != k && current != NULL){
    i++;
    current = current->next;
  }

  // Make head point to k + 1 node
  head = current->next;

  // Make the kth node point to NULL
  current->next = NULL;

  // Make the last node point to the kth node
  Node *t = head;
  while(t->next != NULL){
    t = t->next;
  }
  t->next = originalHead;
}
// Test Client
int main(){
  List list;
  list.printList();
  list.append(10);
  list.append(20);
  list.append(30);
  list.append(40);
  list.append(50);
  list.append(60); 
  list.printList();
  list.rotate(3);
  list.printList();

  return 0;
}
