//Reverse a Linked List
#include <iostream>
using std::cout;
using std::endl;

// Node Class
class Node{
  private:
    int value;
    Node *next;
  public:
    Node();
    Node(int value);
    int getValue();
    Node* getNext();
    void setValue(int value);
    void setNext(Node *next);
};

Node::Node(){
  next = NULL;
}
Node::Node(int value){
  this->value = value;
}
int Node::getValue(){
  return value;
}
Node* Node::getNext(){
  return next;
}
void Node::setValue(int value){
  this->value = value;
}
void Node::setNext(Node *next){
  this->next = next;
}

// List Class
class List{
  private:
    Node *head;
  public:
    List();
    bool isEmpty();
    void printList();
    void insertAtTheEnd(int value);
    Node *getHead();
    void reverseList();
};

List::List(){
  head = NULL;
}
/*
 * Prints the contents of the list
 */
void List::printList(){
  if(head == NULL){
    cout << "Sorry empty list" << endl;
    return;
  }
  else if(head->getNext() == NULL){
    cout << head->getValue() << endl;
  }
  else{
    Node *temp = head;
    while(temp != NULL){
      cout << temp->getValue() << '\t';
      temp = temp->getNext();
    }
    cout << endl;
  }
}

/*
 * Insert at the end of the list
 */
void List::insertAtTheEnd(int value){
  Node *newNode = new Node(value);
  if (head == NULL){
    head = newNode;
  }
  else if (head->getNext() == NULL){
    head->setNext(newNode);
  }
  else{
    Node *temp = head;
    while (temp->getNext() != NULL){
      temp = temp->getNext();
    }
    temp->setNext(newNode);
  }
}

bool List::isEmpty(){
  return head == NULL;
}

Node* List::getHead(){
  return head;
}
/*
  Algorithm O(n):
    1) If there's only 1 or 0 nodes in the list, return;
    2) Normal Case:
      a) Make c point to the current node (initially head)
      b) Make p point to the prev node (initially NULL)
      c) Traverse through the list
        - Make n point to the next node after the current one (n = c->getNext())
        - Make current node (c) point to the prev one(p)
        - make p point to the current node
        - make c point to the next node;
*/
void List::reverseList(){
  // Case 1 + 2: empty list + 1 node
  if(!head || !head->getNext())
    return;

  // Normal case
  Node *c = head;
  Node *p = NULL;
  Node *n;
  while (c != NULL){
    n = c->getNext();
    c->setNext(p);
    p = c;
    c = n;
  }
  head = p;
}

// Test Client
int main(){
  List list1;

  list1.insertAtTheEnd(1);
  list1.insertAtTheEnd(2);
  list1.insertAtTheEnd(3);
  list1.insertAtTheEnd(4);
  list1.insertAtTheEnd(5);
  list1.insertAtTheEnd(6);
  list1.insertAtTheEnd(7);
  list1.insertAtTheEnd(8);
  list1.printList();
  
  list1.reverseList();
  list1.printList();
  return 0;
}
