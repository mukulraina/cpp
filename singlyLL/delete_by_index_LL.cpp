#include <iostream>
using namespace std;

class Node{
public:
  int value;
  Node *next;
  Node();
  Node(int v);
};
Node::Node(){
  next = NULL;
}
Node::Node(int v){
  value = v;
}

class LL{
  Node *head;
public:
  LL();
  void insertAtTheEnd(int v);
  void printLL();
  void deleteIndex(int i);
};
LL::LL(){
  head = NULL;
}
void LL::insertAtTheEnd(int v){
  // Make a new node
  Node *newNode = new Node(v);

  // Case 1: 0 node
  if(head == NULL){
    head = newNode;
    return;
  }

  // Case 2: 1 or more nodes
  Node *c = head;
  // Traverse c so that it points to the last node
  while(c->next != NULL){
    c = c->next;
  }

  // Make the last node point to newNode instead of null
  c->next = newNode;
}

void LL::printLL(){
  // Case 1: 0 node 
  if(head == NULL){
    cout << "sorry empty list" << endl;
    return;
  }

  Node *c = head;
  while(c != NULL){
    cout << c->value << '\t';
    c = c->next;
  }
  cout << endl;
}
void LL::deleteIndex(int i){
  // Case 1: 0 node
  if(head == NULL){
    cout << "sorry empty list" << endl;
    return;
  }

  // Case 2: Check if index is < size or not
  int index = 0;
  Node *c = head;
  Node *p = NULL;
  while(c != NULL){
    if(index == i)
      break;
    index++;
    p = c;
    c = c->next;
  }

  // Now if index was not found in the list
  if(c == NULL){
    cout << "sorry index was bigger than the size of the list" << endl;
    return;
  }

  // This means c points to the node to be deleted
  // Case 3: if 1st node needs to be deleted
  // Case 3a: there is only 1 node in the list
  // Case 3b: there are other nodes in the list too
  if(index == 0){
    Node *t = head;
    if(head->next == NULL)
      head = NULL;
    else
      head = head->next;
    delete t;
    return;
  }

  // Case 4: index is somewhere in the middle
  // so p points to the prev node and c points to the current
  p->next = c->next;
  delete c;
  return;
}

void testDeletionIndex(){
  LL *l = new LL();
  l->insertAtTheEnd(1);
  l->insertAtTheEnd(2);
  l->insertAtTheEnd(3);
  l->insertAtTheEnd(4);

  l->deleteIndex(3);

  cout << "\n\n\n\nprinting list after deletion: " << endl;
  l->printLL();
}

int main(){
  testDeletionIndex();
  return 0;
}
