#include <iostream>
using namespace std;

class Node{
public:
  int value;
  Node *next;
  Node();
  Node(int v);
};
Node::Node(){
  next = NULL;
}
Node::Node(int v){
  value = v;
}

class LL{
  Node *head;
public:
  LL();
  void insertAtTheEnd(int v);
  void printLL();
  bool isEmpty();
  void deleteValue(int v);
};
LL::LL(){
  head = NULL;
}
bool LL::isEmpty(){
  return head == NULL;
}
void LL::insertAtTheEnd(int v){
  Node *newNode = new Node(v);

  // Empty LL
  if(isEmpty()){
    head = newNode;
    return;
  }

  Node *current = head;
  // Make current point to the last node
  while(current->next != NULL){
    current = current->next;
  }
  // Make last node point to the newNode
  current->next = newNode;
}

void LL::printLL(){
  if(isEmpty()){
    cout << "Sorry empty list" << endl;
    return;
  }
  cout << "Printing list" << endl;
  Node *current = head;
  // Traverse till current points to NULL
  while(current != NULL){
    cout << current->value << '\t';
    current = current->next;
  }
  cout << endl;
}

void LL::deleteValue(int v){
  // case 1: no node
  if(isEmpty())
    return;

  // case 2: single node LL
  if(head->next == NULL){
    // Case 2a: v is there
    if(head->value == v){
      Node *temp = head;
      delete temp;
      head = NULL;   
      return; 
    }
    // Case 2b: v is NOT there
    else{
      cout << "Sorry the element is not in the list" << endl;
      return;
    }
  }

  // Case 3: More than 2 nodes
  Node *prev = NULL;
  Node *current = head;

  while(current->value != v && current != NULL){
    prev = current;
    current = current->next;
  }
  
  // Case 3a: Node is NOT there
  if(!current){
    cout << "sorry the node is not in the list" << endl;
    return;
  }
  
  // Case 3b: Node is there

    // v is the first node in the list
    if(!prev){
      // Make head point to the second node(instead of first)
      head = head->next;
      delete current;
      return;
    }
    
    // v is somewhere in the middle
    prev->next = current->next;
    delete current;
}

void deletionByValueTestCase2a(){
  LL *list = new LL();
  list->insertAtTheEnd(1);
  list->printLL();
  list->deleteValue(1);
  list->printLL();
}

void deletionByValueTestCase2b(){
  LL *list = new LL();
  list->insertAtTheEnd(1);
  list->printLL();
  list->deleteValue(2);
  list->printLL();  
}

void deletionByValueTestCase3a(){
  LL *list = new LL();
  list->insertAtTheEnd(1);
  list->insertAtTheEnd(2);
  list->insertAtTheEnd(3);
  list->insertAtTheEnd(4);
  list->insertAtTheEnd(5);
  list->printLL();
  list->deleteValue(7);
  list->printLL();  
}

int main(){
  //deletionByValueTestCase2a();
  //deletionByValueTestCase2a();
  deletionByValueTestCase2a();
  return 0;
}
