// Program to sort a LL without extra spcace
#include <iostream>
using namespace std;

class Node{
  public:
    int value;
    Node *next;
    Node();
    Node(int v);
};
Node::Node(){
  next = NULL;
}
Node::Node(int v){
  value = v;
  next = NULL;
}

class List{
  public:
    Node *head;
    List();
    bool isEmpty();
    bool isSingleNodeList();
    void printList();
    void append(int v);
    int getSize(Node *node);
    Node *getMid(Node *node);
    void mergeSort(Node *node);
    Node* merge(Node *a, Node *b);
    void split(Node *node, Node *front, Node *back);
};

List::List(){
  head = NULL;
}

bool List::isEmpty(){
  return head == NULL;
}

bool List::isSingleNodeList(){
  return head->next == NULL;
}

void List::append(int v){
  Node *newNode = new Node(v);

  // Case 1: 0 node
  if(isEmpty()){
    head = newNode;
    return;
  }

  // Case 1: 1 node
  if(isSingleNodeList()){
    head->next = newNode;
    return;
  }

  // Case 2: More than 1 node
  Node *current = head;
  // Traverse list to make current point to the last node
  while(current->next != NULL){
    current = current->next;
  }

  // Make last node point to newNode instead of NULL
  current->next = newNode;
}

void List::printList(){
  // Case 1: 0 node
  if(head == NULL)
    return;

  // Case 2: 1 node
  if(head->next == NULL){
    cout << head->value << endl;
    return;
  }

  // Case 3: Normal case
  Node *current = head;
  while(current){
    cout << current->value << '\t';
    current = current->next;
  }
  cout << endl;
}

// node = head of the tree passed as input parameter
int List::getSize(Node *node){
  if(!node)
    return 0;

  if(!node->next)
    return 1;

  Node *current = node;
  int size = 0;
  while(current != NULL){
    size++;
    current = current->next;
  }
  return size;
}

Node* List::getMid(Node *node){
  int size = getSize(node);
  int mid = size / 2;

  Node *current = node;
  int counter = 0;
  while(counter != mid){
    counter++;
    current = current->next;
  }
  return current;
}

// node = head as in input parameter 
void List::mergeSort(Node *node){
  Node *head = node;
  Node *a;
  Node *b;

  // Base Case: 0 or 1 node
  if(head == NULL || head->next == NULL)
    return;

  // Split the list into 'a' and 'b' sublists
  split(head,a,b);

  // Recursively sort the sublists
  mergeSort(a);
  mergeSort(b);

  // Merge the 2 sorted sublists
  node = merge(a,b);
}

Node * List::merge(Node *a, Node *b){
  Node *result = NULL;

  // Base Cases
  if(a == NULL)
    return(b);
  else if(b == NULL)
    return(a);

  // Pick either a or b and do a recursive call
  if(a->value <= b->value){
    result = a;
    result->next = merge(a->next,b);
  }
  else{
    result=b;
    result->next = merge(a,b->next);
  }
  return(result);
}

void List::split(Node *node, Node* front, Node* back){
  Node *fast;
  Node *slow;
  if(node == NULL || node->next == NULL){
    // Case 1: length < 2
    front = node;
    back = NULL;
  }
  else{
    slow = node;
    fast = node->next;

    // Advance "fast" by 2 nodes and "slow" by 1 node
    while(fast != NULL){
      fast = fast->next;
      if(fast != NULL){
        slow = slow->next;
        fast = fast->next;
      }
    }

    // Slow points to before the midpoint in the list
    front = node;
    back = slow->next;
    slow->next = NULL;
  }
}

// Test Client
int main(){
  // Make a List object
  List *list = new List();

  // Insert values
  list->append(78);
  list->append(32);
  list->append(36);
  list->append(42);
  list->append(18);
  list->append(54);
  list->append(11);
  list->append(7);
  list->append(9);

  cout << "size: " << list->getSize(list->head) << endl;
  cout << "Middle node: " << list->getMid(list->head)->value << endl;
 
  cout << "List before sorting" << endl;
  list->printList();
  list->mergeSort(list->head);
  cout << "List after sorting" << endl;
  list->printList();
  return 0;
}
