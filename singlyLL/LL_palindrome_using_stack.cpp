// Program to check if a LL is palindrome or not
#include <iostream>
#include <stack>
using namespace std;

class Node{
public:
  int value;
  Node *next;
  Node(int v);
};
Node::Node(int v){
  value = v;
}

class LL{
public:
  Node *head;
  LL();
  bool isEmpty();
  void insertAtTheEnd(int v);
  void printLL();
  bool isPalindrome();
};
LL::LL(){
  head = NULL;
}
bool LL::isEmpty(){
  return head == NULL;
}
void LL::insertAtTheEnd(int v){
  // Make the node
  Node *newNode = new Node(v);

  // Case 1: empty list
  if(isEmpty()){
    head = newNode;
    return;
  }

  Node *c = head;
  while(c->next != NULL){
    c = c->next;
  }
  c->next = newNode;
}

void LL::printLL(){
  if(isEmpty())
    return;

  Node *c = head;
  while(c != NULL){
    cout << c->value << '\t';
    c = c->next;
  }
  cout << endl;
}

bool LL::isPalindrome(){
  // case 1: empty list
  if(isEmpty())
    return false;

  // Case 2: 1 node
  if(head->next == NULL)
    return true;

  // Case 3: Normal Case
  stack<int> s;
  
  // Traverse once + push values onto stack
  Node *current = head;
  while(current != NULL){
    s.push(current->value);
    current = current->next;
  }

  // Traverse again + pop if top is same as current
  current = head;
  while(current != NULL){
    if(s.top() == current->value)
      s.pop();
    current = current->next;
  }

  // Check if stack is empty or not
  if(s.empty())
    return true;
  return false;
}

int main(){
  LL list;
  list.insertAtTheEnd(1);
  list.insertAtTheEnd(2);
  list.insertAtTheEnd(3);
  list.insertAtTheEnd(2);
  list.insertAtTheEnd(1);
  cout << list.isPalindrome() << endl;    
  return 0;
}