#include <iostream>
using namespace std;

class Node{
public:
  int value;
  Node *next;
  Node();
  Node(int v);
};
Node::Node(){
  next = NULL;
}
Node::Node(int v){
  value = v;
}

class LL{
  Node *head;
public:
  LL();
  void insertAtTheEnd(int v);
  void printLL();
  void deleteValue(int v);
};
LL::LL(){
  head = NULL;
}
void LL::insertAtTheEnd(int v){
  // Make a new node
  Node *newNode = new Node(v);

  // Case 1: 0 node
  if(head == NULL){
    head = newNode;
    return;
  }

  // Case 2: 1 or more nodes
  Node *c = head;
  // Traverse c so that it points to the last node
  while(c->next != NULL){
    c = c->next;
  }

  // Make the last node point to newNode instead of null
  c->next = newNode;
}

void LL::printLL(){
  // Case 1: 0 node 
  if(head == NULL){
    cout << "sorry empty list" << endl;
    return;
  }

  Node *c = head;
  while(c != NULL){
    cout << c->value << '\t';
    c = c->next;
  }
  cout << endl;
}
void LL::deleteValue(int v){
  // case 1: 0 node
  if(head == NULL){
    cout << "sorry empty list, cant delete" << endl;
    return;
  }

  // Case 2: Value doesn't exist
  Node *c = head;
  Node *p = NULL;
  while(c != NULL){
    if(c->value == v)
      break;
    p = c;
    c = c->next;
  }
  // if c is NULL, value not found
  if(c == NULL){
    cout << "sorry value not in the LL" << endl;
    return;
  }

  // Case 3: 1st node needs to be deleted
  // Case 3a: there is only 1 node in the list
  // Case 3b: there are more than 1 node in the list
  if(head->value == v){
    Node *t = head;
    if(head->next == NULL)
      head = NULL;
    else
      head = head->next;
    delete t;
    return;
  }

  // Case 4: value is somewhere in the middle
  p->next = c->next;
  delete c;
}

void testInsertionAtTheEnd(){
  LL *l = new LL();
  l->printLL();
  l->insertAtTheEnd(1);
  l->insertAtTheEnd(2);
  l->insertAtTheEnd(3);
  l->insertAtTheEnd(4);
  l->insertAtTheEnd(5);
  l->printLL();
}

void testDeletionValueCase1(){
  LL *l = new LL();
  l->deleteValue(1);
}

void testDeletionValueCase2(){
  LL *l = new LL();
  l->insertAtTheEnd(1);
  l->insertAtTheEnd(2);
  l->insertAtTheEnd(3);
  l->insertAtTheEnd(4);
  l->insertAtTheEnd(5);
  l->deleteValue(3);
  l->printLL();
}

int main(){
  //testDeletionValueCase1();
  testDeletionValueCase2();
  return 0;
}
