// Program to check if a linked list is palindrome or not
#include <iostream>
#include <stack>
using namespace std;

// Node Class
class Node{
  public:
    char c;
    Node *next;
    Node(char c);
    Node();
};
Node::Node(char c){
  this->c = c;
  next = NULL;
}
Node::Node(){
  next = NULL;
}

// List class
class List{
  public:
    Node *head;
    List();
    void printList();
    void append(char c);
    bool isEmpty();
    bool isPalindrome();
};

List::List(){
  head = NULL;
}

bool List::isEmpty(){
  return head == NULL;
}

void List::append(char c){
  Node *newNode = new Node(c);

  if(isEmpty()){
    head = newNode;
    return;
  }

  if(head->next == NULL){
    head->next = newNode;
    return;
  }

  Node *current = head;
  while(current->next != NULL){
    current = current->next;
  }
  current->next = newNode;
}

void List::printList(){
  // Case 1: empty list
  if(isEmpty())
    return;

  // Case 2: 1 node
  if(head->next == NULL)
    cout << head->c << endl;

  // Case 3: Normal Case
  Node *current = head;
  while(current != NULL){
    cout << current->c << '\t';
    current = current->next;
  }
  cout << endl;
}

// Time: O(n) Space: O(n)
bool List::isPalindrome(){
  // Make a stack
  stack<char> s;

  // Traverse through the LL + put all nodes in stack
  Node *current = head;
  while(current != NULL){
    s.push(current->c);
    current = current->next;
  }

  // Traverse through the LL again + pop nodes from stack
  current = head;
  while(current != NULL){
    // get the top value of the stack
    char c = s.top();
    
    // Pop that value
    s.pop();
    
    // Compare that value with the current node
    if(c != current->c)
      return false;

    current = current->next;
  }
  // If it came till here, that means all the nodes matched
  return true;
}
// Test Client
int main(){
  List list;
  list.printList();
  list.append('r');
  list.append('a');
  list.append('d');
  list.append('a');
  list.append('r');
  list.printList();
  cout << list.isPalindrome() << endl;
  return 0;
}
