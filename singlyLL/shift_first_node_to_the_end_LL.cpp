// Program to shift the first node to the end of the list
// Approach: swapping (Time: O(n) Space: O(1) in place)
#include <iostream>
#include <queue>
using namespace std;

class Node{
public:
  int value;
  Node *next;
  Node(int v);
};
Node::Node(int v){
  value = v;
}

class LL{
public:
  Node *head;
  LL();
  void insertAtTheEnd(int v);
  void printLL();
  void shiftFirstNodeToTheEnd();
};

LL::LL(){
  head = NULL;
}

void LL::insertAtTheEnd(int v){
  // allocate node
  Node *newNode = new Node(v);

  // Case 1: empty list
  if(head == NULL){
    head = newNode;
    return;
  }

  // Case 2: 1 or more node
  Node *c = head;
  // Make c point to the last node (after traversal)
  while(c->next != NULL){
    c = c->next;
  }

  // Make the last node point to newNode instead of NULL
  c->next = newNode;
}

void LL::printLL(){
  // Check if empty
  if(head == NULL)
    return;

  Node *c = head;
  while(c != NULL){
    cout << c->value << '\t';
    c = c->next;
  }
  cout << endl;
}

// Time: O(n)
void LL::shiftFirstNodeToTheEnd(){
  if(head == NULL)
    return;

  Node *c = head;
  // Get a pointer to point to the last node of the LL
  Node *l = head;
  while(l->next != NULL){
    l = l->next;
  }

  // Make l point to the original firstNode
  l->next = c;

  // Make head point to the original 2nd node (now firstNode)
  head = head->next;

  // Since c points to the original node which is supposed to be
  // the last node now, make it point to NULL
  c->next = NULL;
}

int main(){
  LL *list = new LL();

  list->insertAtTheEnd(17);
  list->insertAtTheEnd(15);
  list->insertAtTheEnd(8);
  list->insertAtTheEnd(12);
  list->insertAtTheEnd(10);
  
  list->printLL();
  cout << "shift first node to the end: " << endl;
  list->shiftFirstNodeToTheEnd();
  list->printLL();
  list->shiftFirstNodeToTheEnd();
  list->printLL();
  
  return 0;
}
