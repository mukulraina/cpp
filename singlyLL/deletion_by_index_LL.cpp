// Program to reverse a LL
#include <iostream>
using namespace std;

class Node{
public:
  int value;
  Node *next;
  Node(int v);
};
Node::Node(int v){
  value = v;
}

class LL{
public:
  Node *head;
  LL();
  bool isEmpty();
  void insertAtTheEnd(int v);
  void printLL();
  void deletionByIndex(int index);
};
LL::LL(){
  head = NULL;
}
bool LL::isEmpty(){
  return head == NULL;
}
void LL::insertAtTheEnd(int v){
  // Make the node
  Node *newNode = new Node(v);

  // Case 1: empty list
  if(isEmpty()){
    head = newNode;
    return;
  }

  Node *c = head;
  while(c->next != NULL){
    c = c->next;
  }
  c->next = newNode;
}

void LL::printLL(){
  if(isEmpty())
    return;

  Node *c = head;
  while(c != NULL){
    cout << c->value << '\t';
    c = c->next;
  }
  cout << endl;
}

// First element is index 0
void LL::deletionByIndex(int index){
  // Case 1: Empty List
  if(isEmpty()){
    cout << "empty list" << endl;
    return;
  }

  // Case 2: Single Node + delete
  if(head->next == NULL && index == 0){
    cout << "Delete 1st node + NOT maintain" << endl;
    Node *temp = head;
    delete temp;
    head = NULL;
    return;
  }

  // Case 3: Delete 1st node in the LL + maintain the rest
  if(index == 0){
    cout << "Delete 1st node + maintain" << endl;
    Node *current = head;
    head = head->next;
    delete current;
    return;
  }

  // Case 4: Somewhere in the middle 
  int counter = 0;
  Node *current = head;
  Node *prev = NULL;
  while(current != NULL && counter != index){
    counter++;
    prev = current;
    current = current->next;
  }

  // Case 5: If the index was bigger than the list size
  if(current == NULL){
    cout << "Sorry the index was bigger than the size of the list" << endl;
    return;
  }

  // Case 6: Current points to the index then
  prev->next = current->next;
  delete current;
}

int main(){
  LL list;
  list.insertAtTheEnd(1);
  list.insertAtTheEnd(2);
  list.insertAtTheEnd(3);
  list.insertAtTheEnd(4);
  
  list.deletionByIndex(3);

  list.printLL();
  return 0;
}
