// Program to get the middle of the LL (iterative approach)
#include <iostream>
using namespace std;

class Node{
  public:
    int value;
    Node *next;
    Node();
    Node(int v);
};
Node::Node(){
  next = NULL;
}
Node::Node(int v){
  value = v;
  next = NULL;
}

class List{
  public:
    Node *head;
    List();
    bool isEmpty();
    bool isSingleNodeList();
    void printList();
    void append(int v);
    int getSize(Node *node);
    Node *getMid(Node *node);
};

List::List(){
  head = NULL;
}

bool List::isEmpty(){
  return head == NULL;
}

bool List::isSingleNodeList(){
  return head->next == NULL;
}

void List::append(int v){
  Node *newNode = new Node(v);

  // Case 1: 0 node
  if(isEmpty()){
    head = newNode;
    return;
  }

  // Case 1: 1 node
  if(isSingleNodeList()){
    head->next = newNode;
    return;
  }

  // Case 2: More than 1 node
  Node *current = head;
  // Traverse list to make current point to the last node
  while(current->next != NULL){
    current = current->next;
  }

  // Make last node point to newNode instead of NULL
  current->next = newNode;
}

void List::printList(){
  // Case 1: 0 node
  if(head == NULL)
    return;

  // Case 2: 1 node
  if(head->next == NULL){
    cout << head->value << endl;
    return;
  }

  // Case 3: Normal case
  Node *current = head;
  while(current){
    cout << current->value << '\t';
    current = current->next;
  }
  cout << endl;
}

// we pass head of the LL as "node" in the input parameter
int List::getSize(Node *node){
  if(!node)
    return 0;

  if(!node->next)
    return 1;

  Node *c = node;
  int size = 0;
  while(c != NULL){
    size++;
    c = c->next;
  }
  return size;
}

// we pass head of the LL as "node" in the input parameter
Node* List::getMid(Node *node){
  int size = getSize(node);
  int mid = size / 2;

  if(size == 0)
    return NULL;

  if(size == 1)
    return node;

  Node *current = node;
  int counter = 0;
  while(counter != mid){
    counter++;
    current = current->next;
  }
  return current;
}


// Test Client
int main(){
  // Make a List object
  List *list = new List();

  // Insert values
  list->append(78);
  list->append(32);
  list->append(36);
  list->append(42);
  list->append(18);
  list->append(54);
  list->append(11);
  list->append(7);
  list->append(9);

  list->printList();
  cout << "Size of the LL: " << list->getSize(list->head) << endl;
  cout << "The middle node: " << list->getMid(list->head)->value << endl;
  return 0;
}
