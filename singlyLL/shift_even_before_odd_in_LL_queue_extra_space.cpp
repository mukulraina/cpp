// Program to shift even before odd in a singly LL
// Approach: use queue (time: O(n) space: O(n))
#include <iostream>
#include <queue>
using namespace std;

class Node{
public:
  int value;
  Node *next;
  Node(int v);
};
Node::Node(int v){
  value = v;
}

class LL{
public:
  Node *head;
  LL();
  void insertAtTheEnd(int v);
  void printLL();
  void shiftEvenAndOdd();
};

LL::LL(){
  head = NULL;
}

void LL::insertAtTheEnd(int v){
  // allocate node
  Node *newNode = new Node(v);

  // Case 1: empty list
  if(head == NULL){
    head = newNode;
    return;
  }

  // Case 2: 1 or more node
  Node *c = head;
  // Make c point to the last node (after traversal)
  while(c->next != NULL){
    c = c->next;
  }

  // Make the last node point to newNode instead of NULL
  c->next = newNode;
}

void LL::printLL(){
  // Check if empty
  if(head == NULL)
    return;

  Node *c = head;
  while(c != NULL){
    cout << c->value << '\t';
    c = c->next;
  }
  cout << endl;
}

// Time: O(n)
void LL::shiftEvenAndOdd(){
  if(head == NULL)
    return;

  // Make 2 queues for even and odd numbers
  queue<int> evenQ;
  queue<int> oddQ;

  // Traverse through the LL and insert in the queues
  Node *c = head;
  while(c != NULL){
    if(c->value % 2 == 0)
      evenQ.push(c->value);
    else
      oddQ.push(c->value);
    c = c->next;
  }

  // Now we have 2 queues with even and odd nos
  // Now traverse the LL and overwrite the elements with
  // First even nos and then odd numbers
  c = head;
  while(!evenQ.empty()){
    int v = evenQ.front();
    c->value = v;
    c = c->next;
    evenQ.pop();
  }

  while(!oddQ.empty()){
    int v = oddQ.front();
    c->value = v;
    c = c->next;
    oddQ.pop();
  }
}

int main(){
  LL *list = new LL();

  list->insertAtTheEnd(17);
  list->insertAtTheEnd(15);
  list->insertAtTheEnd(8);
  list->insertAtTheEnd(12);
  list->insertAtTheEnd(10);
  list->insertAtTheEnd(5);
  list->insertAtTheEnd(4);
  list->insertAtTheEnd(1);
  list->insertAtTheEnd(7);
  list->insertAtTheEnd(6);
  
  list->printLL();
  cout << "shift even to left and odd to right: " << endl;
  list->shiftEvenAndOdd();
  list->printLL();

  return 0;
}
