/*
 *  Write a program to reverse a linked list iteratively
 */
#include <iostream>
using namespace std;

class Node{
  private:
    int value;
    Node *next;
  public:
    Node();
    int getValue();
    Node* getNext();
    void setValue(int v);
    void setNext(Node *n);
};
Node::Node(){
  next = NULL;
}
int Node::getValue(){
  return value;
}
Node* Node::getNext(){
  return next;
}
void Node::setValue(int v){
  value = v;
}
void Node::setNext(Node *n){
  next = n;
}

class List{
  private:
    Node *head;
  public:
    List();
    void addToTheEnd(int v);
    void printList();
    void deleteList();
    void reverseList();
};

List::List(){
  head = NULL;
}

void List::addToTheEnd(int v){
  Node *newNode = new Node();
  newNode->setValue(v);
  /* Check if the list is empty */
  if (head == NULL){
    head = newNode;
  }
  /* Just 1 element in the list */
  else if (head->getNext() == NULL){
    head->setNext(newNode);
  }
  /* General case - Insert in the end of the list */
  else{
    Node *temp = head;
    /* After this loop, temp will point to the last node */
    while(temp->getNext() != NULL){
      temp = temp->getNext();
    }
    /* Make the last node point to the newNode (basically you attached the newNode in the list) */
    temp->setNext(newNode);
  }
}

void List::printList(){
  /* Check if list is empty */
  if (head == NULL){
    cout<<"Sorry empty list, can't print anything"<<endl;
  }
  /* If there's only 1 element in the list */
  else if (head->getNext() == NULL){
    cout<<head->getValue()<<endl;
  }
  /* General case - More than 1 element in the list */
  else{
    Node *temp = head;
    while(temp != NULL){
      cout<<temp->getValue()<<"\n";
      temp = temp->getNext();
    }
  }
}

/*
 * Deletes all the nodes in the linked list and put head = NULL
 */
void List::deleteList(){
  Node *temp = NULL;
  while(head != NULL){
    temp = head;
    head = head->getNext();
    delete temp;
  }
}

/*
  Reverses the linked list
*/
void List::reverseList(){
  Node *prev, *current, *next;
  current = head;
  prev = NULL;
  while(current !=  NULL){
    /* Save the state of the next node */
    next = current->getNext();
    /* Make the current node point to the prev one */
    current->setNext(prev);
    /* Move prev (current element will be prev for next node) */
    prev = current;
    /* Make the next node, the current one */
    current = next;
  }
  /* After reversing, the head should point to the last node */
  head = prev;
}

int main(){
  List list;
  list.printList();
  list.addToTheEnd(1);
  list.addToTheEnd(2);
  list.addToTheEnd(3);
  list.addToTheEnd(4);
  list.addToTheEnd(5);
  list.printList();
  cout<<"Calling the function to delete the list now\n\n";
  list.deleteList();
  cout<<"List delete, lets print it (shouldn't print anything)\n\n";
  list.printList();
  cout<<"adding some elements again\n\n";
  list.addToTheEnd(1);
  list.addToTheEnd(2);
  list.addToTheEnd(3);
  cout<<"Lets print the newly populated LL\n\n";
  list.printList();
  cout<<"Lets reverse the linked list\n\n";
  list.reverseList();
  cout<<"Here's the reversed list:\n\n";
  list.printList();
  return 0;
}
