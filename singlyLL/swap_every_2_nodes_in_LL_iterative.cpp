/*
Given a singly linked list, swap every 2 nodes, for odd number of input; retain the last node as it is.
Eg: Input: 5 13 15 18 20 11 6 7
Output: 13 5 18 15 11 20 7 6
*/
#include <iostream>
using std::cout;
using std::endl;

// Node Class
class Node{
  private:
    int value;
    Node *next;
  public:
    Node();
    Node(int value);
    int getValue();
    Node* getNext();
    void setValue(int value);
    void setNext(Node *next);
};

Node::Node(){
  next = NULL;
}
Node::Node(int value){
  this->value = value;
}
int Node::getValue(){
  return value;
}
Node* Node::getNext(){
  return next;
}
void Node::setValue(int value){
  this->value = value;
}
void Node::setNext(Node *next){
  this->next = next;
}

// List Class
class List{
  private:
    Node *head;
  public:
    List();
    bool isEmpty();
    void printList();
    int getSize();
    void insertAtTheEnd(int value);
    void insertAtTheStart(int value);
    bool exists(int value);
    void deleteByValue(int value);
    void deleteByIndex(int index);
    void iterativeSwapEveryTwoNodes();
};

List::List(){
  head = NULL;
}
/*
 * Prints the contents of the list
 */
void List::printList(){
  if(head == NULL){
    cout << "Sorry empty list" << endl;
    return;
  }
  else if(head->getNext() == NULL){
    cout << head->getValue() << endl;
  }
  else{
    Node *temp = head;
    while(temp != NULL){
      cout << temp->getValue() << '\t';
      temp = temp->getNext();
    }
    cout << endl;
  }
}

/*
 * Returns the length of the list
 */
int List::getSize(){
  if(head == NULL){
    return 0;
  }
  if(head->getNext() == NULL){
    return 1;
  }
  Node *temp = head;
  int count = 0;
  while(temp != NULL){
    temp = temp->getNext();
    count++;
  }
  return count;
}

/*
 * Insert at the end of the list
 */
void List::insertAtTheEnd(int value){
  Node *newNode = new Node(value);
  if (head == NULL){
    head = newNode;
  }
  else if (head->getNext() == NULL){
    head->setNext(newNode);
  }
  else{
    Node *temp = head;
    while (temp->getNext() != NULL){
      temp = temp->getNext();
    }
    temp->setNext(newNode);
  }
}

/*
 * Insert from the start of the list 
 */
void List::insertAtTheStart(int value){
  Node *newNode = new Node(value);
  if(head == NULL){
    head = newNode;
  }
  else {
    newNode->setNext(head);
    head = newNode;
  }
}

/*
 * Checks if a certain value exist in the list or not
 */
bool List::exists(int value){
  if (head == NULL){
    return false;
  }
  else{
    Node *current = head;
    while(current != NULL){
      if(current->getValue() == value)
        return true;
      current = current->getNext();
    }
    return false;
  }
}

/*
 * Deletes a node from the LinkedList by value
 * Algorithm:
 *  1) Check if the list empty
 *  2) Check if the value (that we want to delete) exists in the list
 *  3) Check if its the 1st Node + only Node in the list
 *  4) Check if its the 1st Node (but there are other nodes in the list too)
 *  5) Normal Case (delete somewhere from the middle)
 */
void List::deleteByValue(int value){
  if(head == NULL){
    cout << "Sorry empty list" << endl;
    return;
  }
  if(exists(value) == 0){
    cout << "Sorry the value you want to delete is not in the list" << endl;
    return;
  }
  else{
    if(head->getValue() == value){
      Node *temp = head;
      if(head->getNext() == NULL){
        head = NULL;
        delete temp;
        return;
      }
      else{
        head = head->getNext();
        delete temp;
        return;
      }
    }
    else{
      Node *current = head;
      Node *prev = NULL;

      while(current->getValue() != value){
        prev = current;
        current = current->getNext();
      }
      prev->setNext(current->getNext());
      delete current;
    }
  }
}

/*
  Deletes a node by its index
  Algorithm:
    1) check if list is empty
    2) Check if its 0 index + its only node in the list
    3) Check if its 0 index + other nodes are there too
    4) Normal Case
*/
void List::deleteByIndex(int index){
  if (head == NULL){
    cout << "Sorry empty list" << endl;
    return;
  }
  if (index > getSize()-1){
    cout << "Sorry index not there" << endl;
    return;
  }
  if(index == 0){
    Node *temp = head;
    if(head->getNext() == NULL){
      head = NULL;
      delete temp;
      return;
    }
    else{
      head = head->getNext();
      delete temp;
      return;
    }
  }
  else{
    int count = 0;
    Node *current = head;
    Node *prev = NULL;
    while(count != index){
      prev = current;
      current = current->getNext();
      count++;
    }
    prev->setNext(current->getNext());
    delete current;
  }
}

bool List::isEmpty(){
  return head == NULL;
}


void swap(Node *f, Node *s){
  int temp = f->getValue();
  f->setValue(s->getValue());
  s->setValue(temp);
}

// Time: O(n)
void List::iterativeSwapEveryTwoNodes(){
  if(isEmpty())
    return;

  if(head->getNext() == NULL){
    return;
  }

  Node *current = head;
  while(current != NULL && current->getNext() != NULL){
    swap(current,current->getNext());
    current = current->getNext()->getNext();
  }
}

// Test Client
int main(){
  List list;
  // Odd Case
  list.insertAtTheEnd(5);
  list.insertAtTheEnd(13);
  list.insertAtTheEnd(15);
  list.insertAtTheEnd(18);
  list.insertAtTheEnd(20);
  list.printList();
  list.iterativeSwapEveryTwoNodes();
  list.printList();

  cout << endl;

  // Even Case
  List list2;
  list2.insertAtTheEnd(5);
  list2.insertAtTheEnd(13);
  list2.insertAtTheEnd(15);
  list2.insertAtTheEnd(18);
  list2.insertAtTheEnd(20);
  list2.insertAtTheEnd(11);
  list2.printList();
  list2.iterativeSwapEveryTwoNodes();
  list2.printList();
  
  return 0;
}
