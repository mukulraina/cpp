/*
Notes:
  1) In append function, we created a new node (so we used = new Node()), 
  whereas in print function we used temp pointer

  2) temp is pointer, not a Node
*/
#include <iostream>
using namespace std;

// Node class 
class Node{
  private:
    int data;
    Node *next;
  public:
    Node(); 
    int getData();
    Node* getNext();
    void setData(int aData);
    void setNext(Node* aNext);
};

// Constructor
Node::Node(){}

// Getter for Data
int Node::getData(){
  return data;
}

// Getter for next
Node* Node::getNext(){
  return next;
}

// Setter for data
void Node::setData(int aData){
  data = aData;
}

// Setter for next
void Node::setNext(Node* aNext){
  next = aNext;
} 

// List class
class List {
    Node *head;
  public:
    List();
    void printList();
    void append(int data);
    void deleteNodeByData(int data);
    void deleteNodeByIndex(int index);
};

// Constructor 
List::List(){
  head = NULL;
}

// Inserts an element in the end of the list
void List::append(int data){
  // Create a new node with given data
  Node *newNode = new Node();
  newNode->setData(data);
  newNode->setNext(NULL);

  // Create a temp pointer
  Node *temp = head;

  if (temp != NULL){
    // Nodes are already present in the list
    // Go to the end so that temp can point to the last node
    while (temp->getNext() != NULL){
      temp = temp->getNext();
    }
    // Point the last node to point to the next node
    temp->setNext(newNode);
  }
  else{
    // Empty list
    head = newNode;
  }  
}

// Prints the elements in the list
void List::printList(){
  // Counter
  int counter = 0;

  // Temp pointer
  Node *temp = head;

  // If there's an empty list
  if (head == NULL){
    cout<<"EMPTY LIST - no nodes"<<endl;
    return;
  }

  // If there's only 1 node in the list
  if (temp->getNext() == NULL){
    cout<<"data: "<<temp->getData()<<endl;
  }
  else{
    while(temp != NULL){
      cout<<"data at index: "<<counter<<" is "<<temp->getData()<<endl;
      temp = temp->getNext();
      counter++;
    }
  }
}

/* deletes an element from the list*/
void List::deleteNodeByData(int data){
 // Please check the deleteByValue() in CrackingTheCodingInterview/chap2/LinkedList.cpp
}

/* Deletes an element with a specific index e.g delete the kth element of the list */
void List::deleteNodeByIndex(int index){
  // Please check the deleteByValue() in CrackingTheCodingInterview/chap2/LinkedList.cpp
}

// Main function
int main(){
  
  // Make an instance of List type
  List list;
  // Print an empty list
  list.printList();
  // Populate the list
  list.append(1);
  list.append(2);
  list.append(3);
  list.append(4);
  list.append(5);
  list.append(6);
  list.append(7);
  list.append(8);
  // Print the list
  list.printList();

  // Delete nodes now
  //list.deleteNodeByData(2);
  //list.deleteNodeByData(4);
  //list.deleteNodeByData(6);
  //list.deleteNodeByData(8);
  // Print the list
  cout<<"Printing the list after deleting nodes"<<endl;
  list.printList();

  cout<<"Deleting nodes by Index now\n";
  cout<<"Deleting the node at index: 0 (the first element)\n";
  list.deleteNodeByIndex(0); 
  cout<<"Deleting the node at index: 4\n";
  list.deleteNodeByIndex(4);
  cout<<"Deleting the node at index: 5 (the last element)\n";
  list.deleteNodeByIndex(5);
  list.printList();

  return 0;
}
