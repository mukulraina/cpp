// Program to reverse a LL
#include <iostream>
using namespace std;

class Node{
public:
  int value;
  Node *next;
  Node(int v);
};
Node::Node(int v){
  value = v;
}

class LL{
public:
  Node *head;
  LL();
  void insertAtTheEnd(int v);
  void printLL();
  void reverseLLIterative();
};

LL::LL(){
  head = NULL;
}

void LL::insertAtTheEnd(int v){
  // allocate node
  Node *newNode = new Node(v);

  // Case 1: empty list
  if(head == NULL){
    head = newNode;
    return;
  }

  // Case 2: 1 or more node
  Node *c = head;
  // Make c point to the last node (after traversal)
  while(c->next != NULL){
    c = c->next;
  }

  // Make the last node point to newNode instead of NULL
  c->next = newNode;
}

void LL::printLL(){
  // Check if empty
  if(head == NULL)
    return;

  Node *c = head;
  while(c != NULL){
    cout << c->value << '\t';
    c = c->next;
  }
  cout << endl;
}

// Time: O(n)
void LL::reverseLLIterative(){
  if(head == NULL)
    return;

  Node *p = NULL;
  Node *c = head;
  Node *n;
  while(c != NULL){
    n = c->next;
    c->next = p;
    p = c;
    c = n;
  }
  // Make head point to the last node
  head = p;
}

int main(){
  LL *list = new LL();

  list->insertAtTheEnd(1);
  list->insertAtTheEnd(2);
  list->insertAtTheEnd(3);
  list->insertAtTheEnd(4);
  list->insertAtTheEnd(5);
  
  list->printLL();
  cout << "LL after reversal: " << endl;
  list->reverseLLIterative();
  list->printLL();

  return 0;
}