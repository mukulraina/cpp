/*
Given a singly linked list, swap every 2 nodes, for odd number of input; retain the last node as it is.
Eg: Input: 5 13 15 18 20 11 6 7
Output: 13 5 18 15 11 20 7 6
*/
#include <iostream>
using std::cout;
using std::endl;

// Node Class
class Node{
  private:
    int value;
    Node *next;
  public:
    Node();
    Node(int value);
    int getValue();
    Node* getNext();
    void setValue(int value);
    void setNext(Node *next);
};

Node::Node(){
  next = NULL;
}
Node::Node(int value){
  this->value = value;
}
int Node::getValue(){
  return value;
}
Node* Node::getNext(){
  return next;
}
void Node::setValue(int value){
  this->value = value;
}
void Node::setNext(Node *next){
  this->next = next;
}

// List Class
class List{
  private:
    Node *head;
  public:
    List();
    bool isEmpty();
    void printList();
    int getSize();
    void insertAtTheEnd(int value);
    void insertAtTheStart(int value);
    bool exists(int value);
    void deleteByValue(int value);
    void deleteByIndex(int index);
    void recursiveSwapEveryTwoNodes(Node *head);
    Node *getHead();
};

List::List(){
  head = NULL;
}
/*
 * Prints the contents of the list
 */
void List::printList(){
  if(head == NULL){
    cout << "Sorry empty list" << endl;
    return;
  }
  else if(head->getNext() == NULL){
    cout << head->getValue() << endl;
  }
  else{
    Node *temp = head;
    while(temp != NULL){
      cout << temp->getValue() << '\t';
      temp = temp->getNext();
    }
    cout << endl;
  }
}

/*
 * Insert at the end of the list
 */
void List::insertAtTheEnd(int value){
  Node *newNode = new Node(value);
  if (head == NULL){
    head = newNode;
  }
  else if (head->getNext() == NULL){
    head->setNext(newNode);
  }
  else{
    Node *temp = head;
    while (temp->getNext() != NULL){
      temp = temp->getNext();
    }
    temp->setNext(newNode);
  }
}


bool List::isEmpty(){
  return head == NULL;
}

Node* List::getHead(){
  return head;
}

void swap(Node *f, Node *s){
  int temp = f->getValue();
  f->setValue(s->getValue());
  s->setValue(temp);
}

// Time: O(n)
void List::recursiveSwapEveryTwoNodes(Node *head){
  if(head != NULL && head->getNext() != NULL){
    swap(head,head->getNext());
    recursiveSwapEveryTwoNodes(head->getNext()->getNext());
  }
}

// Test Client
int main(){
  List list;
  // Odd Case
  list.insertAtTheEnd(5);
  list.insertAtTheEnd(13);
  list.insertAtTheEnd(15);
  list.insertAtTheEnd(18);
  list.insertAtTheEnd(20);
  list.printList();
  list.recursiveSwapEveryTwoNodes(list.getHead());
  list.printList();

  cout << endl;

  // Even Case
  List list2;
  list2.insertAtTheEnd(5);
  list2.insertAtTheEnd(13);
  list2.insertAtTheEnd(15);
  list2.insertAtTheEnd(18);
  list2.insertAtTheEnd(20);
  list2.insertAtTheEnd(11);
  list2.printList();
  list2.recursiveSwapEveryTwoNodes(list2.getHead());
  list2.printList();
  
  return 0;
}
