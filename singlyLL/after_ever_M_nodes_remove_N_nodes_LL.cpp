//Given a linked list and 2 integers M and N, Keep M nodes and delete N nodes repetitively till the end of linkedlist.
#include <iostream>
using std::cout;
using std::endl;

// Node Class
class Node{
  private:
    int value;
    Node *next;
  public:
    Node();
    Node(int value);
    int getValue();
    Node* getNext();
    void setValue(int value);
    void setNext(Node *next);
};

Node::Node(){
  next = NULL;
}
Node::Node(int value){
  this->value = value;
}
int Node::getValue(){
  return value;
}
Node* Node::getNext(){
  return next;
}
void Node::setValue(int value){
  this->value = value;
}
void Node::setNext(Node *next){
  this->next = next;
}

// List Class
class List{
  private:
    Node *head;
  public:
    List();
    bool isEmpty();
    void printList();
    void insertAtTheEnd(int value);
    Node *getHead();
    void remove(int m, int n);
};

List::List(){
  head = NULL;
}
/*
 * Prints the contents of the list
 */
void List::printList(){
  if(head == NULL){
    cout << "Sorry empty list" << endl;
    return;
  }
  else if(head->getNext() == NULL){
    cout << head->getValue() << endl;
  }
  else{
    Node *temp = head;
    while(temp != NULL){
      cout << temp->getValue() << '\t';
      temp = temp->getNext();
    }
    cout << endl;
  }
}

/*
 * Insert at the end of the list
 */
void List::insertAtTheEnd(int value){
  Node *newNode = new Node(value);
  if (head == NULL){
    head = newNode;
  }
  else if (head->getNext() == NULL){
    head->setNext(newNode);
  }
  else{
    Node *temp = head;
    while (temp->getNext() != NULL){
      temp = temp->getNext();
    }
    temp->setNext(newNode);
  }
}

void List::remove(int m, int n){
  if(head == NULL)
    return;

  Node *c = head;
  Node *t;
  while(c != NULL){
    // Skip M Nodes
    for (int i = 1; i < m && c != NULL; i++)
      c = c->getNext();

    // If we reached the end of the list
    if(c == NULL)
      return ;

    // Start from the NEXT node and delete N nodes
    t = c->getNext();
    for(int i = 1; i <= n && t != NULL; i++){
      Node *temp = t;
      t = t->getNext();
      delete temp;
    }

    // Link the previous nodes with the remaining ones
    c->setNext(t);

    // Set current pointer for another iteration
    c = t;
  }
}

bool List::isEmpty(){
  return head == NULL;
}

Node* List::getHead(){
  return head;
}

// Test Client
int main(){
  List list1;

  list1.insertAtTheEnd(1);
  list1.insertAtTheEnd(2);
  list1.insertAtTheEnd(3);
  list1.insertAtTheEnd(4);
  list1.insertAtTheEnd(5);
  list1.insertAtTheEnd(6);
  list1.insertAtTheEnd(7);
  list1.insertAtTheEnd(8);
  list1.printList();
  list1.remove(2,2);
  //Linked List: 1->2->5->6
  list1.printList();
/*
  
  List list2;

  list2.insertAtTheEnd(1);
  list2.insertAtTheEnd(2);
  list2.insertAtTheEnd(3);
  list2.insertAtTheEnd(4);
  list2.insertAtTheEnd(5);
  list2.insertAtTheEnd(6);
  list2.insertAtTheEnd(7);
  list2.insertAtTheEnd(8);
  list2.insertAtTheEnd(9);
  list2.insertAtTheEnd(10);
  list2.printList();
  list2.remove(3,2);
  //Linked List: 1->2->3->6->7->8
  list2.printList();


  List list3;

  list3.insertAtTheEnd(1);
  list3.insertAtTheEnd(2);
  list3.insertAtTheEnd(3);
  list3.insertAtTheEnd(4);
  list3.insertAtTheEnd(5);
  list3.insertAtTheEnd(6);
  list3.insertAtTheEnd(7);
  list3.insertAtTheEnd(8);
  list3.remove(1,1);
  //Linked List: 1->3->5->7->9
  list3.printList();


*/
  return 0;
}













































