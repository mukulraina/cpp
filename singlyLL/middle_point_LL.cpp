//Given a linked list and 2 integers M and N, Keep M nodes and delete N nodes repetitively till the end of linkedlist.
#include <iostream>
using std::cout;
using std::endl;

// Node Class
class Node{
  private:
    int value;
    Node *next;
  public:
    Node();
    Node(int value);
    int getValue();
    Node* getNext();
    void setValue(int value);
    void setNext(Node *next);
};

Node::Node(){
  next = NULL;
}
Node::Node(int value){
  this->value = value;
}
int Node::getValue(){
  return value;
}
Node* Node::getNext(){
  return next;
}
void Node::setValue(int value){
  this->value = value;
}
void Node::setNext(Node *next){
  this->next = next;
}

// List Class
class List{
  private:
    Node *head;
  public:
    List();
    bool isEmpty();
    void printList();
    void insertAtTheEnd(int value);
    Node *getHead();
    int middlePoint();
};

List::List(){
  head = NULL;
}
/*
 * Prints the contents of the list
 */
void List::printList(){
  if(head == NULL){
    cout << "Sorry empty list" << endl;
    return;
  }
  else if(head->getNext() == NULL){
    cout << head->getValue() << endl;
  }
  else{
    Node *temp = head;
    while(temp != NULL){
      cout << temp->getValue() << '\t';
      temp = temp->getNext();
    }
    cout << endl;
  }
}

/*
 * Insert at the end of the list
 */
void List::insertAtTheEnd(int value){
  Node *newNode = new Node(value);
  if (head == NULL){
    head = newNode;
  }
  else if (head->getNext() == NULL){
    head->setNext(newNode);
  }
  else{
    Node *temp = head;
    while (temp->getNext() != NULL){
      temp = temp->getNext();
    }
    temp->setNext(newNode);
  }
}

// Function to get the middle node of a LL in a single traversal
int List::middlePoint(){
  if (head == NULL)
    return -1;

  if(head->getNext() == NULL)
    return head->getValue();

  Node *p1, *p2;
  p2 = p1 = head;

  while(p2->getNext() != NULL){
    p1 = p1->getNext();
    p2 = p2->getNext()->getNext();
  }

  return p1->getValue();
}

// Test Client
int main(){
  List list1;

  list1.insertAtTheEnd(1);
  list1.insertAtTheEnd(2);
  list1.insertAtTheEnd(3);
  list1.insertAtTheEnd(4);
  list1.insertAtTheEnd(5);
  list1.insertAtTheEnd(6);
  list1.insertAtTheEnd(7);
  list1.insertAtTheEnd(8);
  list1.insertAtTheEnd(9);
  list1.printList();

  cout << list1.middlePoint() << endl;

  return 0;
}
