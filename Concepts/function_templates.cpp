// This program shows how to implement function templates in C++ 
// For handling multiple data types at the same time, please see the next file
#include <iostream>
using std::cout;
using std::endl;

// Template that calculates sum for int, double, long etc
template <class T>
T sum(T a, T b){
  return a+b;
}

// Test Client
int main(){
  int a = 10;
  int b = 20;
  cout<<sum(a,b)<<endl;

  double c = 12.51;
  double d = 17.51;
  cout<<sum(c,d)<<endl;

  long e = 123456;
  long f = 123231231231;
  cout<<sum(e,f)<<endl;
}
