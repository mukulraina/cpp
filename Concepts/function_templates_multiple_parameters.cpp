// This program shows how to write a single template to handle different data types at the same time
#include <iostream>
using std::cout;
using std::endl;

// Returns the bigger one
// Since we return F, its gonna return int as in main() we passed an int value for F
template <class F, class S>
F bigger(F a, S b){
  return a>b?a:b;
}

// Test Client
int main(){
  int a = 10;
  double b = 20.50;
  cout<<bigger(a,b)<<endl; // will return 20 cos the return type above is F (int)
  cout<<bigger(b,a)<<endl; // will return 20.50 cos now we passed a double value in place of F (F is the return type of the function)
}
