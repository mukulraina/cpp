/* This program shows how command line arguments are passed to main() in C++ */
#include <iostream>

int main(int argc, char** argv){
  /* Print number of arguments (./a.out is one of the arguments) */
  std::cout << "Number of arguments passed: " << argc << std::endl;
  /* Print the arguments */
  for(int i = 0 ; i < argc ; i++){
    std::cout << argv[i] << std::endl;
  }
}
