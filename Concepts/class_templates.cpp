// This program shows how to use class templates in C++
// 3 rules
//  1) every function needs template <class T> again when defining 
//  2) angular brackets T with the class name in the function definition
//  3) Object creation in main() - now you need to tell what values are you passing 
#include <iostream>
using std::cout;
using std::endl;

template <class T>
class Bucky{
  private:
    T first, second;
  public:
    Bucky(T a, T b){
      first = a;
      second = b;
    }
    T bigger();
};

// Function definition of bigger()
template <class T>
T Bucky<T>::bigger(){
  return first>second?first:second;
}

// Test Client
int main(){
  Bucky<int> obj(10,20);
  cout<<obj.bigger()<<endl;
  return 0;
}