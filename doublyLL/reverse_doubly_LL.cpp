#include <iostream>
using namespace std;

class Node{
public:
  int value;
  Node *next;
  Node *prev;
  Node(int v);
};
Node::Node(int v){
  value = v;
  next = prev = NULL;
}

class DLL{
public:
  Node *head;
  DLL();
  bool isEmpty();
  void insertAtTheEnd(int v);
  void printDLL();
  void printFromBehind();
  void reverseDLLIterative();
};

DLL::DLL(){
  head = NULL;
}

bool DLL::isEmpty(){
  return head == NULL;
}

// Time: O(n)
void DLL::insertAtTheEnd(int v){
  // Make the node
  Node *newNode = new Node(v);

  // Case 1: 0 node in DLL
  if(isEmpty()){
    head = newNode;
    return;
  }

  // Case 2: 1 or more node
  Node *c = head;
  // Make c point to the last node
  while(c->next != NULL){
    c = c->next;
  }
  // Attach the last node and newNode
  // 1) Make the lastNode point to the newNode
  c->next = newNode;
  // 2) Make the newNode point to the lastNode 
  newNode->prev = c;
}

// Time: O(n)
void DLL::printDLL(){
  if(isEmpty())
    return;

  Node *c = head;
  while(c != NULL){
    cout << c->value << '\t';
    c = c->next;
  }
  cout << endl;
}

// Use the prev pointer of the nodes to traverse the list
void DLL::printFromBehind(){
  if(isEmpty())
    return;

  // Make a pointer that points to the last node of the list
  Node *last = head;
  while(last->next != NULL){
    last = last->next;
  }

  // Now last points to the last node of the list
  while(last != head){
    cout << last->value << '\t';
    last = last->prev;
  }
  // Now last points to the head + print that value as well
  cout << last->value << endl;
}

void DLL::reverseDLLIterative(){
  if(isEmpty())
    return;

  Node *c = head;
  Node *p = NULL;
  Node *n;
  while(c != NULL){
    n = c->next;
    c->next = p;
    c->prev = n;
    p = c;
    c = n;
  }
  head = p;
}

int main(){
  DLL *list = new DLL();

  list->insertAtTheEnd(1);
  list->insertAtTheEnd(2);
  list->insertAtTheEnd(3);
  list->insertAtTheEnd(4);
  list->insertAtTheEnd(5);

  cout << "normal print before reverse" << endl;
  list->printDLL();
  cout << endl;

  cout << "normal print after reverse" << endl;
  list->reverseDLLIterative();
  list->printDLL();
  cout << endl;

  cout << "print from behing (after reverse) should look the same as original list" << endl;
  list->printFromBehind();
  return 0;
}