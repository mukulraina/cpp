#include <iostream>
using namespace std;

class Node{
public:
  int value;
  Node *next;
  Node *prev;
  Node(int v);
};
Node::Node(int v){
  value = v;
  next = prev = NULL;
}

class DLL{
public:
  Node *head;
  DLL();
  bool isEmpty();
  void insertAtTheEnd(int v);
  void insertAtTheStart(int v);
  void deleteByIndex(int index);
  void deleteByValue(int v);
  void printDLL();
};

DLL::DLL(){
  head = NULL;
}

bool DLL::isEmpty(){
  return head == NULL;
}

// Time: O(n)
void DLL::insertAtTheEnd(int v){
  // Make the node
  Node *newNode = new Node(v);

  // Case 1: 0 node in DLL
  if(isEmpty()){
    head = newNode;
    return;
  }

  // Case 2: 1 or more node
  Node *c = head;
  // Make c point to the last node
  while(c->next != NULL){
    c = c->next;
  }
  // Attach the last node and newNode
  // 1) Make the lastNode point to the newNode
  c->next = newNode;
  // 2) Make the newNode point to the lastNode 
  newNode->prev = c;
}

// Time: O(n)
void DLL::printDLL(){
  if(isEmpty())
    return;

  Node *c = head;
  while(c != NULL){
    cout << c->value << '\t';
    c = c->next;
  }
  cout << endl;
}

// Time: O(1)
void DLL::insertAtTheStart(int v){
  // Step 1: Allocate node
  Node *newNode = new Node(v);

  // Case 1: empty list
  if(isEmpty()){
    head = newNode;
    return;
  }

  // Case 2: 1 or more node
  // Make the newNode point to the 1st node in the list
  newNode->next = head;
  // Make the prev of firstNode point to newNode (it must be null right now)
  head->prev = newNode;
  // Make the head point to the newNode instead of firstNode
  head = newNode;
}

// Time: O(n)
void DLL::deleteByIndex(int index){
  // Case 1: if empty list, can not delete anything
  if(isEmpty())
    return;

  // Case 2: index 0
  if(index == 0){
    Node *c = head;

    // Case 2a: index = 0 + 1 node in DLL
    if(head->next == NULL)
      head = NULL;

    // Case 2b: index = 0 + More than 1 node in the DLL
    if(head->next != NULL)
      head = head->next;
    delete c;
    return;
  }
  
  // Case 3: Index somewhere in the middle
  int counter = 0;
  Node *c = head;
  while(counter != index && c!= NULL){
    counter++;
    c = c->next;
  }

  if(c == NULL){
    cout << "Sorry index bigger than the size of the list" << endl;
    return;
    }

  // c pointer to the node that needs to be deleted
  // Make prev node of c point to the next node of c
  c->prev->next = c->next;
  // Make the next node of c point to the prev node of c
  if(c->next != NULL)
  c->next->prev = c->prev;
  delete c;
}

void DLL::deleteByValue(int v){
  // Case 1: empty list
  if(isEmpty())
    return;

  // Case 2: value is in the 1st node
  if(head->value == v){
    Node *c = head;
    // Case 2a: 1 node in the LL
    if(head->next == NULL)
      head = NULL;
    // Case 2b: more than 1 node in the LL
    if(head->next != NULL)
      head = head->next;
    delete c;
    return;
  }

  // Case 3: Value is in some other node
  Node *c = head;
  while(c != NULL){
    if(c->value == v)
      break;
    c = c->next;
  }
  // If c is pointing to null that means that value was not found
  if(c == NULL){
    cout << "sorry that value was not in the LL" << endl;
    return;
  }

  // Now it means c points to the node that needs to be deleted
  // Make the prev node of c point to the next node of c
  c->prev->next = c->next;
  // Make the next node of c point to the prev node of c
  // if last node
  if(c->next != NULL)
    c->next->prev = c->prev;
  delete c;
}

int main(){
  DLL *list = new DLL();

  list->insertAtTheEnd(1);
  list->insertAtTheEnd(2);
  list->insertAtTheEnd(3);
  list->insertAtTheEnd(4);
  list->insertAtTheEnd(5);

  list->printDLL();
  cout << "deleteing by value now" << endl;
  list->deleteByValue(99);
  list->printDLL();
  return 0;
}