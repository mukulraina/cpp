// Program to check if there is a path between 2 nodes in a directed graph
// We use BFS to check if there is a path or not
// Could have used DFS as well
#include <iostream>
using namespace std;

// Represents a directed graph using adjacency list representation
class Graph{
  int V; // No of vertices
  list<int> *adj; // Pointer to an array containing adjacency lists
public:
  Graph(int V);
  void addEdge(int v, int w);
  bool isReachable(int s, int d);
};

Graph::Graph(int V){
  this->V = V;
  // Makes an array of 'V' no of elements
  // And each element if a list of int type 
  adj = new list<int> [V]; 
}

void Graph::addEdge(int v, int w){
  adj[v].push_back(w);
}

// A BFS based function to check whether d is reachable from s or not
bool Graph::isReachable(int s, int d){
  // Base Case
  if(s == d)
    return true;

  // Mark all vertices as not visited
  bool *visited = new bool[V];
  for(int i = 0; i < V; i++){
    visited[i] = false;
  }

  // Create a queue for BFS
  list<int> queue;

  // Mark the current node as visited and enqueue it
  visited[s] = true;
  queue.push_back(s);

  // To get all the adjacent vertices of a vertex
  list<int>::iterator i;

  while(!queue.empty()){
    // Dequeue a vertex from queue and print it
    s = queue.front();
    queue.pop_front();

    // Get all the adjacent vertices of the dequeued vertex s
    // If an adjacent has not been visited, then mark it visited + put it on queue
    for(i = adj[s].begin(); i != adj[s].end(); i++){
      // If this adjacent node is the destination node, return true
      if(*i == d)
        return true;

      // Else continue to do BFS
      if(!visited[*i]){
        visited[*i] = true;
        queue.push_back(*i);
      }
    }
  }
  return false;
}

int main(){
  Graph g(4);

  g.addEdge(0, 1);
  g.addEdge(0, 2);
  g.addEdge(1, 2);
  g.addEdge(2, 0);
  g.addEdge(2, 3);
  g.addEdge(3, 3);

  int u = 1, v = 3;
  if(g.isReachable(u, v))
      cout<< "\n There is a path from " << u << " to " << v;
  else
      cout<< "\n There is no path from " << u << " to " << v;

  u = 3, v = 1;
  if(g.isReachable(u, v))
      cout<< "\n There is a path from " << u << " to " << v;
  else
      cout<< "\n There is no path from " << u << " to " << v;
  return 0;
}
