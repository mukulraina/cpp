// A C program to demonstrate adjacency list representation of undirected graphs
// Referrence: http://www.geeksforgeeks.org/graph-and-its-representations/
#include <stdio.h>
#include <stdlib.h>

/*
  - Class to represent a node in the adjacency list
  - dest = value in the graph to which the source is connected 
    - e.g. if dest = 4 that means 4 is in the adjacency list of some other node (say 2)
    - that means 4 is connected to 2
  - next = pointer to point to the next node in the adjacency list
*/
struct AdjListNode{
  int dest;
  struct AdjListNode *next;
};

/*
  - Class to represent adjacency list
  - head = pointer to point to the 1st node of the list
*/
struct AdjList{
  struct AdjListNode *head;
};

/*
  - Class to represent the graph
  - V = # of vertices in the graph
  - array = array of AdjList (array of adjacency list for each of the vertices)
    - so this array has 1 adjacency list for every vertex
    - each list is basically a head pointer which points to the first node and so on
*/
struct Graph{
  int V;
  struct AdjList *array;
};

// Create a graph of V vertices
struct Graph * createGraph(int V){
  // Make a new graph
  struct Graph *graph = (struct Graph*) malloc(sizeof(struct Graph));

  // Assign V of the newly created graph same as input
  graph->V = V;

  // Create an array of adjacency lists (size of the array will be V)
  graph->array = (struct AdjList*) malloc(sizeof(V * struct AdjList));

  // Initialize each adjacnency list as empty (by making the head NULL)
  for(int i = 0; i < V; i++){
    graph->array[i].head = NULL;
  }

  retrun graph;
}

// Adds an edge to an undirected graph
void addEdge(struct Graph *graph, int src, int dest){
  // Add an edge from src to dest
  // A new node is added to the adjaceny list of src
  // The new node is added to the beginning
  struct AdjListNode *newNode = (struct AdjListNode*) malloc(sizeof(struct AdjListNode));
  newNode->dest = dest;
  newNode->next = graph->array[src].head;
  graph->array[src].head = newNode;
}

// Print the graph
void printGraph(struct Graph* graph){
  for(int v = 0; v < graph->V; v++){
    struct AdjListNode *current = graph->array[v].head;
    while(current != NULL){
      cout << current->dest << '\t';
      current = current->next;
    }
    cout << endl; 
  }
}

// Test Client
int main(){
  // Create a graph
  int V = 5;
  struct Graph *graph = createGraph(V);
  
  addEdge(graph, 0, 1);
  addEdge(graph, 0, 4);
  addEdge(graph, 1, 2);
  addEdge(graph, 1, 3);
  addEdge(graph, 1, 4);
  addEdge(graph, 2, 3);
  addEdge(graph, 3, 4);

  printGraph(graph);
  return 0;
}
