// Program to print the BFS traversal from a given source vertex for directed graph
#include <iostream>
#include <list>
using namespace std;

/*
  - Class to represent a directed graph using adjacency list representation
  - V   = no of vertices
  - adj = Pointer to an array containing adjacency lists 
*/
class Graph{
  private:
    int V;
    list<int> *adj;
  public:
    Graph(int V);
    void addEdge(int v, int w);
    void BFS(int s);
};

// Make a new graph with # of vertices V
Graph::Graph(int V)
{
  this->V = V;
  adj = new list<int> [V];
}

// Function to add an edge to graph
void Graph::addEdge(int v, int w)
{
  adj[v].push_back(w); // add w to v's list
}

// Print BFS traversal from a given source s
void Graph::BFS(int s)
{
  // Mark all vertices not visited
  bool *visited = new bool[V];
  for(int i = 0; i < V; i++)
  {
    visited[i] = false;
  }

  // Create a queue for BFS
  list<int> queue;

  // Mark the current node as visited and enqueue it
  visited[s] = true;
  queue.push_back(s);

  // 'i' will be used to get all the adjacent vertices of a vertext
  list<int>::iterator i;

  while(!queue.empty())
  {
    // Dequeue a vertex from queue and print it
    s = queue.front();
    cout << s << '\t';
    queue.pop_front();

    // Get all the adjacent vertices of the dequeued vertex s
    // If the adjacent has not been visited, then 
      // 1) mark it visited
      // 2) enqueue it
    for(i = adj[s].begin(); i != adj[s].end(); i++)
    {
      if(!visited[*i])
      {
        visited[*i] = true;
        queue.push_back(*i);
      }
    }
  }  
}

// Driver program to test methods of graph class
int main()
{
    // Create a graph given in the above diagram
    Graph g(4);
    g.addEdge(0, 1);
    g.addEdge(0, 2);
    g.addEdge(1, 2);
    g.addEdge(2, 0);
    g.addEdge(2, 3);
    g.addEdge(3, 3);
 
    cout << "Following is Breadth First Traversal (starting from vertex 2) \n";
    g.BFS(2);
 
    return 0;
}
