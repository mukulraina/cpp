// Program to implement queue using array
#include <iostream>
using std::cout;
using std::endl;

#define size 10

class Queue{
  public:
    int head;
    int tail;
    int a[size];
    
    Queue();
    bool isEmpty();
    bool isFull();
    void enqueue(int value);
    int dequeue();
    void printQueue();
};

Queue::Queue(){
  head = tail = -1;
}

bool Queue::isEmpty(){
  if (head == -1)
    return true;
  return false;
}

bool Queue::isFull(){
  return ((head == 0 && tail == size-1) || (head == tail + 1));
}

// Function to enqueue values in the queue
void Queue::enqueue(int value){
  if(isFull()){
    cout << "Sorry full queue" << endl;
    return;
  }
  else{
    if(tail == size-1 || tail == -1){
      a[0] = value;
      tail = 0;
      if(head == -1)
        head = 0;
    }
    else{
      tail++;
      a[tail] = value;
    }
  }
}

// Function to print the contents of the queue
void Queue::printQueue(){
  if(isEmpty()){
    cout << "Sorry empty queue" << endl;
    return;
  }

  if(tail >= head){
    for(int i = head; i <= tail; i++){
      cout << a[i] << endl;
    }
  }
  else if(tail < head){
    for(int i = head; i < size; i++){
      cout << a[i] << endl;
    }
    for(int j = 0; j <= tail; j++){
      cout << a[j] << endl;
    }
  }
}

// Function to dequeue value from the queue
int Queue::dequeue(){
  if(isEmpty()){
    cout << "Sorry empty queue" << endl;
    return -1;
  }

  int temp;
  temp = a[head];
  if(head == tail)
    head = tail = -1;
  else if(head == size-1)
    head = 0;
  else
    head++;

  return temp;
}

// Test Client
int main(){
  Queue *queue = new Queue();

  cout << "Just dequeued: " << queue->dequeue() << endl;

  queue->enqueue(10);
  queue->enqueue(20);
  queue->enqueue(30);
  cout << "Printing queue: " << endl;
  queue->printQueue();
  cout << endl;

  queue->enqueue(40);
  queue->enqueue(50);
  cout << "Printing queue: " << endl;
  queue->printQueue();
  cout << endl;

  cout << "Just dequeued: " << queue->dequeue() << endl;
  cout << "Just dequeued: " << queue->dequeue() << endl;
  cout << "Printing queue: " << endl;
  queue->printQueue();
  cout << endl;

  return 0;
}
