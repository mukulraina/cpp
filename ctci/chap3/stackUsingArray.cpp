/*
  Stack implementation using arrays
  Note:
    1) Use top = -1 cos S[top] should return the most recently
        inserted element in the stack
    2) If you use top = 0, then top will point to the index where
        we insert new elements wherease the purpose of top is to 
        point to the most recently inserted element (which is done 
        with top = -1)
    3) Time: O(1)
*/

#include <iostream>
using std::cout;
using std::endl;

#define MAX_SIZE 5

class Stack{
  private:
    int a[MAX_SIZE];
    int top;
  public:
    Stack();
    bool isEmpty();
    void push(int value);
    int pop();
    void printStack();
    int peek();
};
Stack::Stack(){
  top = -1;
}

bool Stack::isEmpty(){
  if(top == -1)
    return true;
  return false;
}

void Stack::push(int value){
  if(top == MAX_SIZE - 1){
    cout << "Sorry stack is full" << endl;
    return;
  }
    top++;
    a[top] = value;
}

int Stack::pop(){
  if(isEmpty()){
    cout << "Sorry empty stack" << endl;
    return -1;
  }
  top--;
  return a[top+1];
}

void Stack::printStack(){
  if(isEmpty()){
    cout << "Sorry empty stack" << endl;
    return;
  }
  for(int i = 0; i <= top; i++){
    cout << a[i] << endl;
  }
}

// Returns the top value of the collection
// In this case its the most recently inserted element
// note: but do not remove it from the stack, just return its value
int Stack::peek(){
  if(isEmpty()){
    cout << "Sorry stack is empty" << endl;
    return -1;
  }
  return a[top];
}

// Test Client
int main(){
  Stack stack;

  // Poping an empty stack
  cout << "Just poped: " << stack.pop() << endl;

  // Push some elements
  stack.push(10);
  stack.push(20);
  stack.push(30);
  
  // Print stack
  stack.printStack();
  cout << "peek value: " << stack.peek() << endl;
  cout << endl;

  // Pop and push
  cout << "Just poped: " << stack.pop() << endl;
  stack.push(40);
  stack.printStack();
  cout << "peek value: " << stack.peek() << endl;
  cout << endl;

  //Try fulling the stack now
  stack.push(50);
  stack.push(60);
  stack.printStack();
  cout << "peek value: " << stack.peek() << endl;

  stack.push(70);
  stack.printStack();
  cout << "peek value: " << stack.peek() << endl;
  cout << endl;

  return 0;
}
