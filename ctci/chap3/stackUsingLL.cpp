/*
  Program to implement stack using LL

*/
#include <iostream>
using std::cout;
using std::endl;

class Node{
  private:
    int value;
    Node *next;
  public:
    Node();
    Node(int value);
    ~Node();
    int getValue();
    Node* getNext();
    void setValue(int value);
    void setNext(Node *next);
};
Node::Node(){
  next = NULL;
}
Node::Node(int value){
  this->value = value;
  next = NULL;
}
Node::~Node(){
}
int Node::getValue(){
  return value;
}
Node* Node::getNext(){
  return next;
}
void Node::setValue(int value){
  this->value = value;
}
void Node::setNext(Node *next){
  this->next = next;
}

class Stack{
  private:
    Node *top;
  public:
    Stack();
    bool isEmpty();
    void push(int value);
    int pop();
    int peek();
    void printStack();
};
Stack::Stack(){
  top = NULL;
}

// Function to check if a stack is empty
bool Stack::isEmpty(){
  if(top == NULL)
    return true;
  return false;
}

/*
  1) Function to push a value onto stack
  2) Its basically insertionAtTheStart() for LL
  3) Time: O(1)
*/
void Stack::push(int value){
  Node *newNode = new Node(value);
  if(top == NULL){
    top = newNode;
    return;
  }
  newNode->setNext(top);
  top = newNode;
}

/*
  1) Function to print the stack
  2) Time: O(n)
*/
void Stack::printStack(){
  if(isEmpty()){
    cout << "Sorry empty stack" << endl;
    return;
  }
  Node *current = top;
  while(current != NULL){
    cout << current->getValue() << endl;
    current = current->getNext();
  }
}

/*
  1) Function to pop an element
  2) Basically means delete from the start of LL
*/
int Stack::pop(){
if(isEmpty()){
    cout << "Sorry empty stack" << endl;
    return -1;
  }

  Node *temp = top;
  int v = top->getValue();

  if(top->getNext() == NULL)
    top = NULL;
  else
    top = top->getNext();
  
  delete temp;
  return v;
}

// Gives the value of the most recently inserted element
// Thats the value of top
int Stack::peek(){
  if(isEmpty()){
    cout << "Sorry empty stack" << endl;
    return -1;
  }
  return top->getValue();
}

// Test Client
int main(){
  Stack *stack = new Stack();

  // Poping an empty stack
  cout << "Just poped: " << stack->pop() << endl;

  // Push some elements
  stack->push(10);
  stack->push(20);
  stack->push(30);
  
  // Print stack
  stack->printStack();
  
  cout << "peek value: " << stack->peek() << endl;
  cout << endl;

  // Pop and push
  cout << "Just poped: " << stack->pop() << endl;
  stack->push(40);
  stack->printStack();
  cout << "peek value: " << stack->peek() << endl;
  cout << endl;

  //Try fulling the stack now
  stack->push(50);
  stack->push(60);
  stack->printStack();
  cout << "peek value: " << stack->peek() << endl;

  stack->push(70);
  stack->printStack();
  cout << "peek value: " << stack->peek() << endl;
  cout << endl;
  
  return 0;
}
