// Program to implement 3 stacks using an array
#include <iostream>
#include <vector>
using namespace std;

#define STACK_SIZE 5

class Stack{
  public:
    int stackSize;
    int a[STACK_SIZE * 3];
    int stackPointer[3];

    Stack();
    void push(int stackNum, int v);
    int pop(int stackNum);
    int peek(int stackNum);
    int getTopOfStack(int stackNum); // Returns index of the top of the stack "stackNum"
    bool isEmpty(int stackNum);
};

Stack::Stack(){
  stackSize = STACK_SIZE;
  stackPointer[0] = stackPointer[1] = stackPointer[2] = -1; 
}

bool Stack::isEmpty(int stackNum){
  return stackPointer[stackNum] == -1;
}

int Stack::getTopOfStack(int stackNum){
  return (stackNum * stackSize) + stackPointer[stackNum];
}

void Stack::push(int stackNum, int v){
  // Check if we have space for the next element
  if(stackPointer[stackNum] + 1 >= stackSize)
    return;

  stackPointer[stackNum]++;
  a[getTopOfStack(stackNum)] = v;
}

int Stack::pop(int stackNum){
  if(isEmpty(stackNum))
    return -1;

  int v = a[getTopOfStack(stackNum)];
  stackPointer[stackNum]--;
  return v;
}

int Stack::peek(int stackNum){
  if(isEmpty(stackNum))
    return -1;

  return a[getTopOfStack(stackNum)];
}

int main(){
  Stack stack;
  
  stack.push(2,4); 
  cout << "Peek stack no 2: " << stack.peek(2) << endl;

  stack.push(0,3);
  stack.push(0,7);
  stack.push(0,5);
  cout << "Peek stack no 0: " << stack.peek(0) << endl;

  stack.pop(0);
  cout << "Peek stack no 0: " << stack.peek(0) << endl;

  stack.pop(0);
  cout << "Peek stack no 0: " << stack.peek(0) << endl;

  return 0;
}
