// Implement min() in O(1) for stack
#include <iostream>
using std::cout;
using std::endl;

class Node{
  private:
    int value;
    Node *next;
  public:
    Node();
    Node(int value);
    ~Node();
    int getValue();
    Node* getNext();
    void setValue(int value);
    void setNext(Node *next);
};
Node::Node(){
  next = NULL;
}
Node::Node(int value){
  this->value = value;
  next = NULL;
}
Node::~Node(){
}
int Node::getValue(){
  return value;
}
Node* Node::getNext(){
  return next;
}
void Node::setValue(int value){
  this->value = value;
}
void Node::setNext(Node *next){
  this->next = next;
}

// Normal Stack
class Stack{
  private:
    Node *top;
  public:
    Stack();
    bool isEmpty();
    void push(int value);
    int pop();
    int peek();
    void printStack();
    int min();
};
Stack::Stack(){
  top = NULL;
}

// Function to check if a stack is empty
bool Stack::isEmpty(){
  return top == NULL;
}

/*
  1) Function to push a value onto stack
  2) Its basically insertionAtTheStart() for LL
  3) Time: O(1)
*/
void Stack::push(int value){
  Node *newNode = new Node(value);

  // Case 1: 0 Node
  if(top == NULL){
    top = newNode;
    return;
  }

  // Case 2: Normal case
  newNode->setNext(top);
  top = newNode;
}

/*
  1) Function to print the stack
  2) Time: O(n)
*/
void Stack::printStack(){
  if(isEmpty()){
    cout << "Sorry empty stack" << endl;
    return;
  }
  Node *temp = top;
  while(temp){
    cout << temp->getValue() << '\t';
    temp = temp->getNext();
  }
  cout << endl;
}

/*
  1) Function to pop an element
  2) Basically means delete from the start of LL
*/
int Stack::pop(){
  if(isEmpty()){
    cout << "Sorry empty stack" << endl;
    return -1;
  }

  Node *temp = top;
  int v = top->getValue();

  if(top->getNext() == NULL)
    top = NULL;
  else
    top = top->getNext();
  
  delete temp;
  return v;
}

// Gives the value of the most recently inserted element
// Thats the value of top
int Stack::peek(){
  if(isEmpty()){
    cout << "Sorry no peek stack is empty" << endl;
    return -1;
  }
  return top->getValue();
}

// This stack uses minStack
class customStack{
  private:
    Node *top;
  public:
    Stack minStack;
    customStack();
    bool isEmpty();
    void push(int v);
    int pop();
    int getMin();
    void print();
};

customStack::customStack(){
  top = NULL;
}

bool customStack::isEmpty(){
  return top == NULL;
}

void customStack::push(int v){
  Node *newNode = new Node(v);

  if(isEmpty()){
    minStack.push(v);
    top = newNode;
    return;
  }
  newNode->setNext(top);
  top = newNode;
  if(v < minStack.peek())
    minStack.push(v);
}

int customStack::pop(){
  if(isEmpty())
    return -1;

  Node *temp = top;
  int v = top->getValue();

  if(v == minStack.peek())
    minStack.pop();
  if(top->getNext() == NULL)
    top = NULL;
  else
    top = top->getNext();

  delete temp;
  return v;
}

int customStack::getMin(){
  if(isEmpty())
    return -1;

  return minStack.peek();
}

// Test Client
int main(){
  customStack stack;

  stack.push(3);
  stack.push(2);
  stack.push(1);
  stack.push(5);
  stack.push(2);
  stack.push(6);
  cout << stack.getMin() << endl;
  
  stack.pop();
  stack.pop();
  stack.pop();
  stack.pop();
  cout << stack.getMin() << endl; 

  return 0;
}
