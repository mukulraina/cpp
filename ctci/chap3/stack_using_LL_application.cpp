// Implement stack using LL
#include <iostream>
using namespace std;

// Node
class Node{
public:
  int value;
  Node *next;
  Node();
  Node(int v);
};
Node::Node(){
  next = NULL;
}
Node::Node(int v){
  value = v;
  next = NULL;
}

// Stack
class StackLL{
public:
  Node *top;
  StackLL();
  bool isEmpty();
  void push(int v);
  int pop();
  int peek();
  void printStack();
};

StackLL::StackLL(){
  top = NULL;
}

bool StackLL::isEmpty(){
  return top == NULL;
}

// Time: O(1) - insert at the start
void StackLL::push(int v){
  Node *newNode = new Node(v);

  // Case 1: empty list
  if(isEmpty()){
    top = newNode;
    return;
  }

  // Case 2: 1 or more than 1 node
  newNode->next = top;
  top = newNode;
}

// Time: O(1) - Delete 1st node
int StackLL::pop(){
  // Case 1: empty stack
  if(isEmpty())
    return -1;

  // Case 2: 1 or more element in the stack
  Node *temp = top;
  int popedValue = top->value;

  if(top->next == NULL)
    top = NULL;
  else
    top = top->next;

  delete temp;
  return popedValue;
}

// Top: O(1) - return value of first node
int StackLL::peek(){
  if(isEmpty())
    return -1;

  return top->value;
}

// Print Stack - O(n)
void StackLL::printStack(){
  if(isEmpty())
    return;

  Node *c = top;
  while(c != NULL){
    cout << c->value << endl;
    c = c->next;
  }
}

// Test Client
int main(){
  StackLL *stack = new StackLL();

  // Poping an empty stack
  cout << "Just poped: " << stack->pop() << endl;

  // Push some elements
  stack->push(10);
  stack->push(20);
  stack->push(30);
  
  // Print stack
  stack->printStack();
  
  cout << "peek value: " << stack->peek() << endl;
  cout << endl;

  // Pop and push
  cout << "Just poped: " << stack->pop() << endl;
  stack->push(40);
  stack->printStack();
  cout << "peek value: " << stack->peek() << endl;
  cout << endl;

  //Try fulling the stack now
  stack->push(50);
  stack->push(60);
  stack->printStack();
  cout << "peek value: " << stack->peek() << endl;

  stack->push(70);
  stack->printStack();
  cout << "peek value: " << stack->peek() << endl;
  cout << endl;
  
  return 0;
}
