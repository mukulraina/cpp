#include <iostream>
using namespace std;

void printArray(int *a, int s){
  for(int i = 0; i < s; i++){
    cout << a[i] << '\t';
  }
  cout << endl;
}

// sizeA is the size of array A without the buffer
void merge(int *a, int *b, int sizeA, int sizeB){
  int i = sizeA - 1;
  int j = sizeB - 1;
  int k = sizeA + sizeB - 1;

  while(i >= 0 && j >= 0){
    if(b[j] > a[i]){
      a[k] = b[j];
      j--;
      k--;
    }
    else{
      a[k] = a[i];
      i--;
      k--;
    }
  }

  // Note that you don't need to copy the contents of A 
  //after running out of elements in B. They are already in place.
  while(j >= 0){
    a[k] = b[j];
    j--;
    k--;
  }
}

int main(){
  int a[] = {2,6,8,14,0,0,0,0};
  int sizeA = 4;
  int b[] = {3,7,11,15};
  int sizeB = 4;
  merge(a,b,sizeA,sizeB);
  printArray(a,sizeA + sizeB);
  return 0;
}
