/* 
 * Program to reverse a null terminated string
 * Algorithm:
 *  1) Make a new pointer, point it to the 1st character by equating it to input pointer char *str
 *  2) Traverse the new pointer till the null char and then decrement once so that it points to the last character
 *  3) Then run a loop with str going forward and ptrEnd coming back, run the loop till str < ptrEnd
 *     a) In the loop keep swapping values
 *     b) Increment str and decrement ptrEnd
 */

#include <iostream>

/*
 * Function to reverse a null terminated string
 * @param *s        Pointer to the string that needs to be reversed
 * @param *ptrEnd   Pointer to the last character of the string
 * @param temp      Temporary character used for swapping
 * Time             O(n)
 * str -> whole string (12345)
 * *str -> first element (1)
 * str[0] -> first element (1) 
 */
void reverseString(char* str){
  if (str == NULL)
    return;
  
  char *ptrEnd = str;
  char temp;
  while(*ptrEnd){
    ptrEnd++;
  }
  ptrEnd--;

  while(str<ptrEnd){
    temp = *str;
    *str++ = *ptrEnd;
    *ptrEnd-- = temp;
  }
}
// Test Client
int main(){
  // Normal Case
    // 0 space
    char s1[] = {"Mukul"};
    reverseString(s1);
    std::cout<<s1<<'\n';

    // 1 space
    char s2[] = "Mukul Raina";
    reverseString(s2);
    std::cout<<s2<<'\n';

  // Extreme Case
    // 0
    char s3[] = "";
    reverseString(s3);
    std::cout<<s3<<'\n';

    // 1
    char s4[] = "a";
    reverseString(s4);
    std::cout<<s4<<'\n';

  // Null / illegal 
    // no -ve n in fibonacci 

  // Strange input
    // Already sorted
    // Reverse sorted
  return 0;
}
