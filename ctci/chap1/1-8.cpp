// Giventwostrings,siands2,writecodetocheckIfs2isarotationofsi usingonlyonecalltoisSubstring
#include <iostream>
#include <string>
using std::cout;
using std::endl;
using std::string;

// Checks if a string is a rotation of another
bool isRotation(const string& s1, const string& s2){
  if (s1.size() == s2.size() && s1.size() > 0){
    string s1s1 = s1 + s1;
    return s1s1.find(s2) != string::npos;
  }
  return false;
}

// Test Client
int main(){
  string a = "apple";
  string b = "leapp";
  cout << isRotation(a,b) << endl;

  string c = "james";
  string d = "mesje";
  cout << isRotation(c,d) << endl;
  
  return 0;
}
