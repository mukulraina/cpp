/* 
 * Implement a method to perform basic string compression using the counts of repeated characters. 
 * For example, the string aabcccccaaa would become a2blc5a3.
 * This is called Run Length Encoding
 */
#include <iostream>
#include <string>
#include <sstream>
using std::string;
using std::cout;
using std::endl;
using std::stringstream;

/*
 * Function to convert integer to string using stringstream
 * @param value   Integer value you want to convert
 * @param temp    String version of "value"
 * @return        Converts "value" and returns it
 */
string intToString(int value){
  string temp;
  stringstream convert;
  convert << value;
  temp = convert.str();
  return temp;
}

/*
 * Function to do Run Length Encoding
 * @param s             Original String
 * @param encodedString Encoded String
 * @param noOfOccur     # of Occurences of each character (string version)
 * @param count         # of Occurences of each character (int version)
 * @return              Encoded string
 */
string runLengthEncoding(const string& s){
  if(s.size() == 0)
    return NULL;

  string encodedString;
  int count = 1;
  for(int i = 0; i < s.size(); i++){
    if (s[i] == s[i+1] && i != s.size()-1){
      count++;
    }
    else{
      encodedString += s[i];
      string noOfOccur = intToString(count);
      encodedString += noOfOccur;
      count = 1;
    }
  }
  
  if (encodedString.size() > s.size())
    return s;
  else 
    return encodedString;
}

// Test Client
int main(){
  // Normal Case
  cout<<runLengthEncoding("aabcccccaaa")<<'\n';
  cout<<runLengthEncoding("abbccccccde")<<'\n';
  // Extreme Case
  cout<<runLengthEncoding("a")<<'\n';

  // Null/Illegal Case
  //cout<<runLengthEncoding("")<<'\n';

  // Strange Input

  return 0;
 }
