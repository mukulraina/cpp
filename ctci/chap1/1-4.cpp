/*
 * Program to replace spaces with %20 in a string
 * Note 1: The number of new characters is getting multiplied by 3 but the space of old string increases by +2 for every %20 (or space)
 * Note 2: We copy from the end cos if we were writing %20 in place of ' ' from the start, 2 n 0 of %20 would have overwritten parts of the string
 *         However since you have empty space in the end of string so you don't care about 2 n 0 of %20 as they wont be overwritting anything
 *         (the newLength we calculated must give us enough spaces for 2 n 0 + no overwriting from back + will be in sync)
 * Algo:
 *  1) You get a string, calculate its length
 *  2) Make a new string dynamically of new length (oldLength + 2 * spaceCount)
 *  3) Call replaceSpaces
 *      a) Calculates the spaces
 *      b) put null character at str[newLength] (so there will be lot of spaces between last character and null character)
 *      c) Traverse from length (original length of the string) as that will give us the last character
 *      d) Look at Note 2
 */
#include<iostream>
#include<string>

/*
 * Replaces spaces in a string with %20
 * @param str         Pointer to the original string
 * @param length      Length of the original string
 * @param newLength   Updated length with %20 in account
 * @param spaceCount  No of spaces in the original string
 * Time:              O(n)
 */
void replaceSpaces(char str[], int length) {
  int newLength, spaceCount = 0;
  
  for(int i = 0; i < length; i++){
    if (str[i] == ' ')
      spaceCount++;
  }

  newLength = length + spaceCount * 2;
  str[newLength] = '\0';
  
  for(int i = length - 1; i >= 0; i--) {
    if(str[i] == ' '){
      str[newLength - 1] = '0';
      str[newLength - 2] = '2';
      str[newLength - 3] = '%';
      newLength -= 3;
    } 
    else{
      str[newLength - 1] = str[i];
      newLength--;
    }
  }
}

/* Test Client 
 * @param str         Original String
 * @param spaceCount  No of spaces in the original String 
 */
int main() {
    std::string str = "abc d e f g h";
    int spaceCount = 5;

    char *newStr = new char[str.size() + spaceCount * 2 + 1];
    for (int i = 0; i < str.size(); i++){
      newStr[i] = str[i];
    }

    std::cout<<"The original string is: "<<str<<'\n';
    replaceSpaces(newStr,str.size());
    std::cout<<"New String with %20 is: "<<newStr<<'\n';
    return 0;
}
