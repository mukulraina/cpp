/* 
 * Rotating a matrix (2D array) by 90 degrees
 *  
 * Given an image represented by an NxN matrix, where each pixel in the image is 4 bytes 
 * write a method to rotate the image by 90 degrees. Canyou do this in place?
 * 
 * referrence: http://rajendrauppal.blogspot.ca/2011/12/rotating-2d-array-of-integers-matrix-by.html
 */
#include <iostream>
using std::cout;
using std::endl;

/*
 * Prints a matrix
 */
void printMatrix (int matrix[3][3]){
  for (int i = 0; i < 3; i++){
    for (int j =0; j < 3; j++){
      cout << matrix[i][j];
    }
    cout << endl;
  } 
}

/*
 * Swaps 2 values
 * @param a     First value to be swapped
 * @param b     Second value to be swapped
 * @param temp  Temporary Variable
 * @return      Swaps the values by referrence (actual values get swapped)
 */
void swap(int &a, int &b){
  int temp = a;
  a = b;
  b = temp;
}

/*
 * Transposes a matrix (by referrence - actual matrix gets transposed)
 * @param row   # of rows in the matrix
 * @param col   # of columns in the matrix
 */
void transpose(int matrix[3][3]){
  int row = 3;
  int col = 3;

  for (int i = 0; i < row; i++){
    for (int j = 0; j < col; j++){
      if ( i < j){
        swap(matrix[i][j], matrix[j][i]);
      }
    }
  }
}

/*
 * Reverse a row in the matrix
 * @param *p      Pointer to a row in the matrix
 * @param start   Tracks the start of the row
 * @param end     Tracks the end of the row
 */
void reverse(int *p){
  int col = 3;
  int start = 0;
  int end = col - 1;

  while (start < end){
    swap(p[start], p[end]);
    start++;
    end--;
  }
}

/*
 * Rotates the matrix by 90 degrees
 */
void rotate(int matrix[3][3]){
  int row = 3;
  transpose(matrix);
  for (int i = 0; i < row; i++){
    reverse(matrix[i]);
  }
  printMatrix(matrix);
}

// Test Client
int main(){
  int matrix[3][3] = {{1,2,3},
                      {4,5,6},
                      {7,8,9}}; 
  rotate(matrix);
  return 0;
}
