/*
 * Program to check if 2 strings are anagram - bool asciiSet approach - O(n)
 * Assumption 1: Case Sensitive
 * Assumption 2: Whitespace
 * Assumption 3: ASCII 
 * Algorithm:
 *  1) Make 2 asciiSet bool arrays of size 256
 *  2) Traverse through first string and increment corresponding values in int asciiSet[] for the characters found
 *  3) When traversing through the second string decrement corresponding indexs
 *  4) If they are anagram, Now all the values of asciiSet[] should be equal to 0 
 */
#include <iostream> 

/*
 * Checks if 2 strings are anagram of each other or not
 * @param a         First string to be compared
 * @param b         Second string to be compared
 * @param asciiSet  Keeps track of no of occurences of each character
 * @return          True if the strings are anagram, false if they are not
 * Time             O(n)
 */
bool checkAnagram(const std::string &a, const std::string &b){
  if (a.size() == 0 || b.size() == 0 || a.size() != b.size())
    return false;

  int asciiSet[256] = {0};
  for (int i = 0; i < a.size(); i++){
    int value = a[i];
    asciiSet[value]++;
  }
  for (int i = 0; i < b.size(); i++){
    int value = b[i];
    asciiSet[value]--;
  }
  for (int i = 0; i < 256; i++){
    if (asciiSet[i] != 0)
      return false;
  }
  return true;
} 

// Test Client
int main(){
  // Normal Case
    std::string s1 = "apple";
    std::string s2 = "papel";
    std::cout<<checkAnagram(s1,s2)<<'\n';

    std::string s3 = "madam god";
    std::string s4 = "dog madam";
    std::cout<<checkAnagram(s3,s4)<<'\n';
  
  // Extreme Case
    std::string s5 = "";
    std::string s6 = "dog madam";
    std::cout<<checkAnagram(s5,s6)<<'\n';

    std::string s7 = "dog madam";
    std::string s8 = "";
    std::cout<<checkAnagram(s7,s8)<<'\n';
    
    // large

  // Null / illegal 

  // Strange input

  return 0;
}
