/*
 * Program to check if 2 strings are anagram of each other - Sorting Approach - O(n logn)
 * Assumption 1: Ask if its case sensitive
 * Assumption 2: Ask about whitespace
 * Algorithm:
 *    1) Make copy of both the strings as we'll be modifying them 
 *    2) Sort the strings
 *    3) Now if the strings were anagram, sorted version of both of them should be the same. If not, they aren't anagram
 */
#include <iostream>
#include <string>
#include <algorithm>

/*
 * Checks if 2 strings are anagram or not
 * @param a   First string to be compared
 * @param b   Second string to be compared
 * @param s1  Copy of first string so that the original string is not modified
 * @param s2  Copy of second string so that the original string is not modified
 * @return    True if they are anagram, false if they are not
 * Time       O(log n)
 */
bool checkAnagram(const std::string &a, const std::string &b){
  if(a.size() == 0 || b.size() == 0 || a.size() != b.size())
    return false;

  std::string s1(a);
  std::string s2(b);
  sort(s1.begin(), s1.end());
  sort(s2.begin(), s2.end());

  for (int i = 0; i < s1.size(); i++){
    if(s1[i] != s2[i])
      return false;
  }
  return true;
}

// Test Client
int main(){
  // Normal Case
    std::string s1 = "apple";
    std::string s2 = "papel";
    std::cout<<checkAnagram(s1,s2)<<'\n';

    std::string s3 = "madam god";
    std::string s4 = "dog madam";
    std::cout<<checkAnagram(s3,s4)<<'\n';
  
  // Extreme Case
    std::string s5 = "";
    std::string s6 = "dog madam";
    std::cout<<checkAnagram(s5,s6)<<'\n';

    std::string s7 = "dog madam";
    std::string s8 = "";
    std::cout<<checkAnagram(s7,s8)<<'\n';
    
    // large

  // Null / illegal 

  // Strange input

  return 0;
}
