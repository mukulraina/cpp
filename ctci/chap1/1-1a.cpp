// Program to check if a string has all unique characters - HashTable approach
// Algorithm:
//  1) Put all the characters of the string in the hashtable as key and 1 as value
//  2) If the character is already in the hashtable, increment its value
//  3) Traverse through the string again and check ->second for all the character
//      a) If any of them is > 1, that means it has appeared more than once, so no unique, exit
#include <iostream>
#include <string>
#include <unordered_map>
typedef std::unordered_map<char,int> Mymap;

/*
 * Checks if a string has all unique characters or not
 * @param s     String to be checked
 * @param m     HashTable to keep track of the no of occurences of each character
 * @param iter  Map iterator to traverse through the map
 * @return      Return True if all characters are unique, else return false
 * Time         O(n)
 */
bool isUnique(const std::string& s){
  if (s.size() == 0 || s.size() > 256)
    return false;

  Mymap m;
  Mymap::iterator iter;
  for (int i = 0; i < s.size(); i++){
    iter = m.find(s[i]);
    if (iter == m.end())
      m.insert(Mymap::value_type(s[i],1));
    else 
      iter->second++;
  }

  for (int j = 0; j < s.size(); j++){
    iter = m.find(s[j]);
    if (iter->second > 1)
      return false;
  }
  return true;
}

// Test Client
int main(){
  // Normal Case
    // no space 
    std::cout<<isUnique("abcdef")<<'\n';
    // 1 space
    std::cout<<isUnique("abcdef ghijklmnop")<<'\n';

  // Extreme Case
    // 0
    std::cout<<isUnique("")<<'\n';
    // 1
    std::cout<<isUnique("a")<<'\n';
    // large 
    std::cout<<isUnique("abcdefghabcdefghabcdefghabcdefghabcdefghabcdefghabcdefghabcdefghabcdefghabcdefgh")<<'\n';

  // Null / illegal 
    // whats not possible in problem

  // Strange input
    // Already sorted
    // Reverse sorted
  return 0;
}