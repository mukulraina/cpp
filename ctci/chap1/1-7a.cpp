// Algorithm such that if an element in a matrix is 0, its row and columns are set to 0
// Algorithm:
//    1) Find the index of element which is 0 - gives you row and column - O(n^2)
//    2) Set all the elements in the row to 0 - O(n)
//    3) Set all the elements in the column to 0 - O(n)
//    Time: O(n^2)
#include <iostream>
using std::cout;
using std::endl;

/*
 * Prints a matrix
 */
void printMatrix(int m[][3]){
  for (int i = 0; i < 3; i++){
    for (int j = 0; j < 3; j++){
      cout << m[i][j] << '\t';
    }
    cout << endl;
  }
}

/*
 * Finds the element which is 0 in the matrix and then sets its rows and columns to 0
 * @param m         The original Matrix
 * @param reqdRow   Row # of the element which was 0
 * @param reqdCol   Col # of the element which was 0
 */
void setToZeros(int m[][3]){
  int reqdRow;
  int reqdCol;

  for(int i = 0; i < 3; i++){
    for (int j = 0; j < 3; j++){
      if (m[i][j] == 0){
        reqdRow = i;
        reqdCol = j;
      }
    }
  }

  for (int i = 0; i < 3; i++){
    m[reqdRow][i] = 0;
  }
  for (int j = 0; j < 3; j++){
    m[j][reqdCol] = 0;
  }
}

/* 
 * Test Client
 */
int main(){
  int m[][3] = {{1,2,3},{4,5,0},{7,8,9}};
  cout << "Matrix Before: " << endl;
  printMatrix(m);
  setToZeros(m);
  cout << "Matrix After: " << endl;
  printMatrix(m);
  return 0;
}
