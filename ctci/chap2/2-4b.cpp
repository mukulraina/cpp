// Program to partition a LL around a value x
#include <iostream>
#include <queue>
using namespace std;

class Node{
public:
  int value;
  Node *next;
  Node(int v);
};
Node::Node(int v){
  value = v;
}

class LL{
public:
  Node *head;
  LL();
  bool isEmpty();
  void insertAtTheEnd(int v);
  void printLL();
  void partition(int x);
};
LL::LL(){
  head = NULL;
}
bool LL::isEmpty(){
  return head == NULL;
}
void LL::insertAtTheEnd(int v){
  // Make the node
  Node *newNode = new Node(v);

  // Case 1: empty list
  if(isEmpty()){
    head = newNode;
    return;
  }

  Node *c = head;
  while(c->next != NULL){
    c = c->next;
  }
  c->next = newNode;
}

void LL::printLL(){
  if(isEmpty())
    return;

  Node *c = head;
  while(c != NULL){
    cout << c->value << '\t';
    c = c->next;
  }
  cout << endl;
}

void LL::partition(int x){
  if(isEmpty() || head->next == NULL)
    return;

  queue<int> lowerQueue;
  queue<int> upperQueue;

  // Traverse once
  Node *current = head;
  while(current != NULL){
    if(current->value < x)
      lowerQueue.push(current->value);
    else
      upperQueue.push(current->value);
    current = current->next;
  }

  // Overwrite the LL + pop values
  current = head;

  while(!lowerQueue.empty()){
    current->value = lowerQueue.front();
    lowerQueue.pop();
    current = current->next;
  }

  while(!upperQueue.empty()){
    current->value = upperQueue.front();
    upperQueue.pop();
    current = current->next;
  }
}

int main(){
  LL list;
  list.insertAtTheEnd(2);
  list.insertAtTheEnd(8);
  list.insertAtTheEnd(4);
  list.insertAtTheEnd(5);
  list.insertAtTheEnd(1);
  list.insertAtTheEnd(2);
  list.insertAtTheEnd(6);
  list.insertAtTheEnd(7);
  list.printLL();
  list.partition(5);
  list.printLL(); 
  return 0;
}