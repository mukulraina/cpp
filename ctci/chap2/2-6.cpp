// Detect a loop in the cycle + return the starting of the loop 
#include <iostream>
#include <unordered_map>
using std::cout;
using std::endl;

// Node Class
class Node{
  private:
    int value;
    Node *next;
  public:
    Node();
    Node(int value);
    ~Node();
    int getValue();
    Node* getNext();
    void setValue(int value);
    void setNext(Node *next);
    void removeNode(Node *node);
};

Node::Node(){
  next = NULL;
}
Node::Node(int value){
  this->value = value;
}
Node::~Node(){
  cout << "Node with value: " << value << " is deleted" << endl;
}
int Node::getValue(){
  return value;
}
Node* Node::getNext(){
  return next;
}
void Node::setValue(int value){
  this->value = value;
}
void Node::setNext(Node *next){
  this->next = next;
}

// List Class
class List{
  public:
    Node *head;
    List();
    void printList();
    void insertAtTheEnd(int value);
    void findCycleBegin();
};

List::List(){
  head = NULL;
}

void List::printList(){
  if(head == NULL){
    cout << "Sorry empty list" << endl;
    return;
  }
  else if(head->getNext() == NULL){
    cout << head->getValue() << endl;
  }
  else{
    Node *temp = head;
    while(temp != NULL){
      cout << temp->getValue() << endl;
      temp = temp->getNext();
    }
  }
}

void List::insertAtTheEnd(int value){
  Node *newNode = new Node(value);
  if (head == NULL){
    head = newNode;
  }
  else if (head->getNext() == NULL){
    head->setNext(newNode);
  }
  else{
    Node *temp = head;
    while (temp->getNext() != NULL){
      temp = temp->getNext();
    }
    temp->setNext(newNode);
  }
}

/*
  1) We do fast != NULL cos if the pointer reached to NULL that means
      reached the end of the list that means no cycle
  2) We do fast->next != NULL cos we iterate fast 2 times so if the list
      is even/odd then we need to account for that case too
  3) To detect a cycle we just make sure that fast or fast->next are NOT null
      that means the only reason the while loop above broke cos fast & slow collided
  4) To get the fist node where cycle starts
        we assign slow to head and then iterate it until slow = fast (collision point)
        Once slow and fast again meet (they might meet multiple times in a cycle)
        That gives us the fist node      
*/
void List::findCycleBegin(){
  if(head == NULL)
    return;

  Node *slow = head;
  Node *fast = head;
  while(fast != NULL && fast->getNext() != NULL)
  {
    slow = slow->getNext();
    fast = fast->getNext()->getNext();
    if(fast == slow)
      break;
  }

  if(fast == NULL || fast->getNext() == NULL)
  {
    cout << "Sorry no cycle" << endl;
  }

  slow = head;
  while (slow != fast)
  {
    slow = slow->next;
    fast = fast->next;
  }

  return slow;
}

int main(){
  List *list = new List();

  list->insertAtTheEnd(5);
  list->insertAtTheEnd(14);
  list->insertAtTheEnd(19);
  list->insertAtTheEnd(17);
  list->insertAtTheEnd(22);
  list->insertAtTheEnd(16);
  list->insertAtTheEnd(18);
  list->insertAtTheEnd(2);
  list->insertAtTheEnd(1);
  list->insertAtTheEnd(5);

  list->partition(17);
  list->printList();
  return 0;
}
