// Write code to remove duplicates from an unsorted linked list.
// Referrence: http://cslibrary.stanford.edu/103/LinkedListBasics.pdf
// Assumption: We are not passing the head pointer and assuming the functions
//             are member functions of the class
#include <iostream>
using std::cout;
using std::endl;

// Node Class
class Node{
  private:
    int value;
    Node *next;
  public:
    Node();
    Node(int value);
    int getValue();
    Node* getNext();
    void setValue(int value);
    void setNext(Node *next);
};

Node::Node(){
  next = NULL;
}
Node::Node(int value){
  this->value = value;
}
int Node::getValue(){
  return value;
}
Node* Node::getNext(){
  return next;
}
void Node::setValue(int value){
  this->value = value;
}
void Node::setNext(Node *next){
  this->next = next;
}

// List Class
class List{
  private:
    Node *head;
  public:
    List();
    void printList();
    int getSize();
    void insertAtTheEnd(int value);
    void insertAtTheStart(int value);
    bool exists(int value);
    void deleteByValue(int value);
    void deleteByIndex(int index);
};

List::List(){
  head = NULL;
}
/*
 * Prints the contents of the list
 */
void List::printList(){
  if(head == NULL){
    cout << "Sorry empty list" << endl;
    return;
  }
  else if(head->getNext() == NULL){
    cout << head->getValue() << endl;
  }
  else{
    Node *temp = head;
    while(temp != NULL){
      cout << temp->getValue() << endl;
      temp = temp->getNext();
    }
  }
}

/*
 * Returns the length of the list
 */
int List::getSize(){
  if(head == NULL){
    return 0;
  }
  if(head->getNext() == NULL){
    return 1;
  }
  Node *temp = head;
  int count = 0;
  while(temp != NULL){
    temp = temp->getNext();
    count++;
  }
  return count;
}

/*
 * Insert at the end of the list
 */
void List::insertAtTheEnd(int value){
  Node *newNode = new Node(value);
  if (head == NULL){
    head = newNode;
  }
  else if (head->getNext() == NULL){
    head->setNext(newNode);
  }
  else{
    Node *temp = head;
    while (temp->getNext() != NULL){
      temp = temp->getNext();
    }
    temp->setNext(newNode);
  }
}

/*
 * Insert from the start of the list 
 */
void List::insertAtTheStart(int value){
  Node *newNode = new Node(value);
  if(head == NULL){
    head = newNode;
  }
  else {
    newNode->setNext(head);
    head = newNode;
  }
}

/*
 * Checks if a certain value exist in the list or not
 */
bool List::exists(int value){
  if (head == NULL){
    return false;
  }
  else{
    Node *current = head;
    while(current != NULL){
      if(current->getValue() == value)
        return true;
      current = current->getNext();
    }
    return false;
  }
}

/*
 * Deletes a node from the LinkedList by value
 * Algorithm:
 *  1) Check if the list empty
 *  2) Check if the value (that we want to delete) exists in the list
 *  3) Check if its the 1st Node + only Node in the list
 *  4) Check if its the 1st Node (but there are other nodes in the list too)
 *  5) Normal Case (delete somewhere from the middle)
 */
void List::deleteByValue(int value){
  if(head == NULL){
    cout << "Sorry empty list" << endl;
    return;
  }
  if(exists(value) == 0){
    cout << "Sorry the value you want to delete is not in the list" << endl;
    return;
  }
  else{
    if(head->getValue() == value){
      Node *temp = head;
      if(head->getNext() == NULL){
        head = NULL;
        delete temp;
        return;
      }
      else{
        head = head->getNext();
        delete temp;
        return;
      }
    }
    else{
      Node *current = head;
      Node *prev = NULL;

      while(current->getValue() != value){
        prev = current;
        current = current->getNext();
      }
      prev->setNext(current->getNext());
      delete current;
    }
  }
}

/*
  Deletes a node by its index
  Algorithm:
    1) check if list is empty
    2) Check if its 0 index + its only node in the list
    3) Check if its 0 index + other nodes are there too
    4) Normal Case
*/
void List::deleteByIndex(int index){
  if (head == NULL){
    cout << "Sorry empty list" << endl;
    return;
  }
  if (index > getSize()-1){
    cout << "Sorry index not there" << endl;
    return;
  }
  if(index == 0){
    Node *temp = head;
    if(head->getNext() == NULL){
      head = NULL;
      delete temp;
      return;
    }
    else{
      head = head->getNext();
      delete temp;
      return;
    }
  }
  else{
    int count = 0;
    Node *current = head;
    Node *prev = NULL;
    while(count != index){
      prev = current;
      current = current->getNext();
      count++;
    }
    prev->setNext(current->getNext());
    delete current;
  }
}

// Test Client
int main(){
  List list;
  list.printList();

  for (int i = 5; i < 10; i++){
    list.insertAtTheEnd(i);
  }
  for (int j = 4; j >=0 ; j--){
    list.insertAtTheStart(j);
  }
  list.printList();

  cout << "Checking if 0 exists in the list: " << list.exists(0) << endl;
  cout << "Checking if 9 exists in the list: " << list.exists(9) << endl;
  cout << "Checking if 6 exists in the list: " << list.exists(6) << endl;
  cout << "Checking if 99 exists in the list: " << list.exists(99) << endl; 

  /*
  // Node does not exist in the list
  cout << "Deleting 99: " << endl;
  list.deleteByValue(99);
  // Deleting 1st node
  cout << "Deleting 0: " << endl;
  list.deleteByValue(0);
  // Deleting normal node
  cout << "Deleting 3: " << endl;
  list.deleteByValue(3);
  // Deleting last node
  cout << "Deleting 9: " << endl;
  list.deleteByValue(9);
  list.printList();
  // Deleting the 1st node BUT its also the only node in the list (in this case: delete 1)
  list.deleteByValue(2);
  list.deleteByValue(4);
  list.deleteByValue(5);
  list.deleteByValue(6);
  list.deleteByValue(7);
  list.deleteByValue(8);
  list.printList();
  // Now delete 1
  list.deleteByValue(1);
  list.printList();
  */

  // Index does not exist in the list

  // Deleting 1st node
  cout << "Deleting index 0: " << endl;
  list.deleteByIndex(0);
  // Deleting normal node
  cout << "Deleting index 3: " << endl;
  list.deleteByIndex(3);
  // Deleting last node
  cout << "Deleting index 9: " << endl;
  list.deleteByIndex(9);
  list.printList();

  return 0;
}
