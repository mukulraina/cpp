// Write code to partition a linked list around a value x, 
// such that all nodes less than x come before alt nodes greater than or equal to x.
#include <iostream>
#include <unordered_map>
using std::cout;
using std::endl;

// Node Class
class Node{
  private:
    int value;
    Node *next;
  public:
    Node();
    Node(int value);
    ~Node();
    int getValue();
    Node* getNext();
    void setValue(int value);
    void setNext(Node *next);
    void removeNode(Node *node);
};

Node::Node(){
  next = NULL;
}
Node::Node(int value){
  this->value = value;
}
Node::~Node(){
  cout << "Node with value: " << value << " is deleted" << endl;
}
int Node::getValue(){
  return value;
}
Node* Node::getNext(){
  return next;
}
void Node::setValue(int value){
  this->value = value;
}
void Node::setNext(Node *next){
  this->next = next;
}

// List Class
class List{
  public:
    Node *head;
    List();
    void printList();
    int getSize();
    void insertAtTheEnd(int value);
    void insertAtTheStart(int value);
    bool exists(int value);
    void deleteByValue(int value);
    void deleteByIndex(int index);
    void partition(int value);
    void deleteList();
};

List::List(){
  head = NULL;
}

void List::printList(){
  if(head == NULL){
    cout << "Sorry empty list" << endl;
    return;
  }
  else if(head->getNext() == NULL){
    cout << head->getValue() << endl;
  }
  else{
    Node *temp = head;
    while(temp != NULL){
      cout << temp->getValue() << endl;
      temp = temp->getNext();
    }
  }
}

int List::getSize(){
  if(head == NULL){
    return 0;
  }
  if(head->getNext() == NULL){
    return 1;
  }
  Node *temp = head;
  int count = 0;
  while(temp != NULL){
    temp = temp->getNext();
    count++;
  }
  return count;
}

void List::insertAtTheEnd(int value){
  Node *newNode = new Node(value);
  if (head == NULL){
    head = newNode;
  }
  else if (head->getNext() == NULL){
    head->setNext(newNode);
  }
  else{
    Node *temp = head;
    while (temp->getNext() != NULL){
      temp = temp->getNext();
    }
    temp->setNext(newNode);
  }
}

void List::insertAtTheStart(int value){
  Node *newNode = new Node(value);
  if(head == NULL){
    head = newNode;
  }
  else {
    newNode->setNext(head);
    head = newNode;
  }
}

bool List::exists(int value){
  if (head == NULL){
    return false;
  }
  else{
    Node *current = head;
    while(current != NULL){
      if(current->getValue() == value)
        return true;
      current = current->getNext();
    }
    return false;
  }
}

void List::deleteByValue(int value){
  if(head == NULL){
    cout << "Sorry empty list" << endl;
    return;
  }
  if(exists(value) == 0){
    cout << "Sorry the value you want to delete is not in the list" << endl;
    return;
  }
  else{
    if(head->getValue() == value){
      Node *temp = head;
      if(head->getNext() == NULL){
        head = NULL;
        delete temp;
        return;
      }
      else{
        head = head->getNext();
        delete temp;
        return;
      }
    }
    else{
      Node *current = head;
      Node *prev = NULL;

      while(current->getValue() != value){
        prev = current;
        current = current->getNext();
      }
      prev->setNext(current->getNext());
      delete current;
    }
  }
}

void List::deleteByIndex(int index){
  if (head == NULL){
    cout << "Sorry empty list" << endl;
    return;
  }
  if (index > getSize()-1){
    cout << "Sorry index not there" << endl;
    return;
  }
  if(index == 0){
    Node *temp = head;
    if(head->getNext() == NULL){
      head = NULL;
      delete temp;
      return;
    }
    else{
      head = head->getNext();
      delete temp;
      return;
    }
  }
  else{
    int count = 0;
    Node *current = head;
    Node *prev = NULL;
    while(count != index){
      prev = current;
      current = current->getNext();
      count++;
    }
    prev->setNext(current->getNext());
    delete current;
  }
}

// Deletes the list - free all the nodes + head is set to NULL
void List::deleteList(){
  if(head == NULL)
  {
    cout << "Sorry list is already empty, nothing to delete" << endl;
  }

  Node *current = head;
  while(current != NULL)
  {
    Node *temp = current;
    current = current->getNext();
    delete temp;
  }
  head = NULL;
}

// Partitions the list around a value 
// Time: O(n^2) Space: O(n)
void List::partition(int value){
  if (head == NULL){
    cout << "Sorry empty list" << endl;
    return;
  }

  List *smaller = new List();
  List *bigger  = new List();
  Node *current = head;
  
  while (current != NULL)
  {
    if(current->getValue() < value)
    {
      smaller->insertAtTheEnd(current->getValue());
    }
    if(current->getValue() > value)
    {
      bigger->insertAtTheEnd(current->getValue());
    }
    current = current->getNext();
  }

  head = smaller->head;
  Node *temp = smaller->head;
  while(temp->getNext() != NULL)
  {
    temp = temp->getNext();
  }
  Node *newNode = new Node(value);
  temp->setNext(newNode);
  newNode->setNext(bigger->head);
}

int main(){
  List *list = new List();

  list->insertAtTheEnd(5);
  list->insertAtTheEnd(14);
  list->insertAtTheEnd(19);
  list->insertAtTheEnd(17);
  list->insertAtTheEnd(22);
  list->insertAtTheEnd(16);
  list->insertAtTheEnd(18);
  list->insertAtTheEnd(2);
  list->insertAtTheEnd(1);
  list->insertAtTheEnd(5);

  list->partition(17);
  list->printList();
  return 0;
}
