// Write code to remove duplicates from an unsorted linked list
// Additional Data Structure NOT allowed
// e.g. 12->11->12->21->41->43->21  ->  12->11->21->41->43
// run time is O(n^2) with O(1) space 
#include <iostream>
#include <unordered_map>
using std::cout;
using std::endl;

// Node Class
class Node{
  private:
    int value;
    Node *next;
  public:
    Node();
    Node(int value);
    ~Node();
    int getValue();
    Node* getNext();
    void setValue(int value);
    void setNext(Node *next);
    void removeNode(Node *node);
};

Node::Node(){
  next = NULL;
}
Node::Node(int value){
  this->value = value;
}
Node::~Node(){
  cout << "Node with value: " << value << " is deleted" << endl;
}
int Node::getValue(){
  return value;
}
Node* Node::getNext(){
  return next;
}
void Node::setValue(int value){
  this->value = value;
}
void Node::setNext(Node *next){
  this->next = next;
}

// List Class
class List{
  private:
    Node *head;
  public:
    List();
    void printList();
    int getSize();
    void insertAtTheEnd(int value);
    void insertAtTheStart(int value);
    bool exists(int value);
    void deleteByValue(int value);
    void deleteByIndex(int index);
    void removeDuplicates();
};

List::List(){
  head = NULL;
}
/*
 * Prints the contents of the list
 */
void List::printList(){
  if(head == NULL){
    cout << "Sorry empty list" << endl;
    return;
  }
  else if(head->getNext() == NULL){
    cout << head->getValue() << endl;
  }
  else{
    Node *temp = head;
    while(temp != NULL){
      cout << temp->getValue() << endl;
      temp = temp->getNext();
    }
  }
}

/*
 * Returns the length of the list
 */
int List::getSize(){
  if(head == NULL){
    return 0;
  }
  if(head->getNext() == NULL){
    return 1;
  }
  Node *temp = head;
  int count = 0;
  while(temp != NULL){
    temp = temp->getNext();
    count++;
  }
  return count;
}

/*
 * Insert at the end of the list
 */
void List::insertAtTheEnd(int value){
  Node *newNode = new Node(value);
  if (head == NULL){
    head = newNode;
  }
  else if (head->getNext() == NULL){
    head->setNext(newNode);
  }
  else{
    Node *temp = head;
    while (temp->getNext() != NULL){
      temp = temp->getNext();
    }
    temp->setNext(newNode);
  }
}

/*
 * Insert from the start of the list 
 */
void List::insertAtTheStart(int value){
  Node *newNode = new Node(value);
  if(head == NULL){
    head = newNode;
  }
  else {
    newNode->setNext(head);
    head = newNode;
  }
}

/*
 * Checks if a certain value exist in the list or not
 */
bool List::exists(int value){
  if (head == NULL){
    return false;
  }
  else{
    Node *current = head;
    while(current != NULL){
      if(current->getValue() == value)
        return true;
      current = current->getNext();
    }
    return false;
  }
}

/*
 * Deletes a node from the LinkedList by value
 * Algorithm:
 *  1) Check if the list empty
 *  2) Check if the value (that we want to delete) exists in the list
 *  3) Check if its the 1st Node + only Node in the list
 *  4) Check if its the 1st Node (but there are other nodes in the list too)
 *  5) Normal Case (delete somewhere from the middle)
 */
void List::deleteByValue(int value){
  if(head == NULL){
    cout << "Sorry empty list" << endl;
    return;
  }
  if(exists(value) == 0){
    cout << "Sorry the value you want to delete is not in the list" << endl;
    return;
  }
  else{
    if(head->getValue() == value){
      Node *temp = head;
      if(head->getNext() == NULL){
        head = NULL;
        delete temp;
        return;
      }
      else{
        head = head->getNext();
        delete temp;
        return;
      }
    }
    else{
      Node *current = head;
      Node *prev = NULL;

      while(current->getValue() != value){
        prev = current;
        current = current->getNext();
      }
      prev->setNext(current->getNext());
      delete current;
    }
  }
}

/*
  Deletes a node by its index
  Algorithm:
    1) check if list is empty
    2) Check if its 0 index + its only node in the list
    3) Check if its 0 index + other nodes are there too
    4) Normal Case
*/
void List::deleteByIndex(int index){
  if (head == NULL){
    cout << "Sorry empty list" << endl;
    return;
  }
  if (index > getSize()-1){
    cout << "Sorry index not there" << endl;
    return;
  }
  if(index == 0){
    Node *temp = head;
    if(head->getNext() == NULL){
      head = NULL;
      delete temp;
      return;
    }
    else{
      head = head->getNext();
      delete temp;
      return;
    }
  }
  else{
    int count = 0;
    Node *current = head;
    Node *prev = NULL;
    while(count != index){
      prev = current;
      current = current->getNext();
      count++;
    }
    prev->setNext(current->getNext());
    delete current;
  }
}

/*
  Function to remove duplicates from a LL without using any additional data structures
  Algorithm:
    1) "current" pointer to traverse through the outer loop
    2) "runner" pointer to traverse through the innner loop
    3) please look at the code
*/
void List::removeDuplicates(){
  if(head == NULL)
  {
    cout << "Sorry empty list" << endl;
    return;
  }
  if(head->getNext() == NULL)
  {
    return;
  }

  Node *current = head;
  Node *runner;

  while(current != NULL)
  {
    runner = current;
    while(runner->getNext() != NULL)
    {
      if(current->getValue() == runner->getNext()->getValue())
      {
        Node *temp = runner->getNext();
        runner->setNext(runner->getNext()->getNext());
        delete temp;
      }
      else
      {
        runner = runner->getNext();
      }
    }
    current = current->getNext();
  }
}

// Test Client
int main(){
  List list;

  
  list.insertAtTheEnd(12);
  list.insertAtTheEnd(11);
  list.insertAtTheEnd(12);
  list.insertAtTheEnd(21);
  list.insertAtTheEnd(41);
  list.insertAtTheEnd(43);
  list.insertAtTheEnd(21);
  //list.printList();

  list.removeDuplicates();
  list.printList();

  return 0;
}
