// You have two numbers represented by a linked list, 
// where each node contains a single digit. Thedigits are stored in reverse order, 
// such that the 1'sdigit isat the head of the list. Write a function that 
// adds the two numbers and returns the sum as a linked list.
#include <iostream>
#include <unordered_map>
#include <math.h>
using std::cout;
using std::endl;

// Node Class
class Node{
  private:
    int value;
    Node *next;
  public:
    Node();
    Node(int value);
    ~Node();
    int getValue();
    Node* getNext();
    void setValue(int value);
    void setNext(Node *next);
    void removeNode(Node *node);
};

Node::Node(){
  next = NULL;
}
Node::Node(int value){
  this->value = value;
}
Node::~Node(){
  cout << "Node with value: " << value << " is deleted" << endl;
}
int Node::getValue(){
  return value;
}
Node* Node::getNext(){
  return next;
}
void Node::setValue(int value){
  this->value = value;
}
void Node::setNext(Node *next){
  this->next = next;
}

// List Class
class List{
  public:
    Node *head;
    List();
    void printList();
    void insertAtTheEnd(int value);
};

List::List(){
  head = NULL;
}

void List::printList(){
  if(head == NULL){
    cout << "Sorry empty list" << endl;
    return;
  }
  else if(head->getNext() == NULL){
    cout << head->getValue() << endl;
  }
  else{
    Node *temp = head;
    while(temp != NULL){
      cout << temp->getValue() << endl;
      temp = temp->getNext();
    }
  }
}


void List::insertAtTheEnd(int value){
  Node *newNode = new Node(value);
  if (head == NULL){
    head = newNode;
  }
  else if (head->getNext() == NULL){
    head->setNext(newNode);
  }
  else{
    Node *temp = head;
    while (temp->getNext() != NULL){
      temp = temp->getNext();
    }
    temp->setNext(newNode);
  }
}

// Test Client
int main(){
  // Case 1: When both numbers have same # of digits
 
  List *list1 = new List();
  List *list2 = new List();
  List *finalSumList = new List();

  list1->insertAtTheEnd(7);
  list1->insertAtTheEnd(1);
  list1->insertAtTheEnd(6);

  list2->insertAtTheEnd(2);
  list2->insertAtTheEnd(3);

  // Calculate the sum of the 1st list - O(n)
  Node *r1 = list1->head;
  int sum1 = 0;
  int i = 0;

  while(r1 != NULL)
  {
    int v = r1->getValue();
    sum1 = sum1 + v * pow(10,i);
    i++;
    r1 = r1->getNext();
  }

  // Calculate the sum of the 2nd list - O(n)
  Node *r2 = list2->head;
  int sum2 = 0;
  i = 0;
  while(r2 != NULL)
  {
    int v = r2->getValue();
    sum2 = sum2 + v * pow(10,i);
    i++;
    r2 = r2->getNext();
  }

  // Calculate the total sum
  int finalSum = sum1 + sum2;

  // Reverse the sum into a linkeded list - O(n)
  while(finalSum != 0){
    int digit = finalSum % 10;
    finalSumList->insertAtTheEnd(digit);
    finalSum = finalSum / 10;
  }
  finalSumList->printList();
  return 0;
}
