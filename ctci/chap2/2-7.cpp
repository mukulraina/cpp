// Program to check if a LinkedList is palindrome or not
#include <iostream>
#include <stack>
using std::cout;
using std::endl;
using std::stack;

// Node Class
class Node{
  private:
    int value;
    Node *next;
  public:
    Node();
    Node(int value);
    ~Node();
    int getValue();
    Node* getNext();
    void setValue(int value);
    void setNext(Node *next);
    void removeNode(Node *node);
};

Node::Node(){
  next = NULL;
}
Node::Node(int value){
  this->value = value;
}
Node::~Node(){
  cout << "Node with value: " << value << " is deleted" << endl;
}
int Node::getValue(){
  return value;
}
Node* Node::getNext(){
  return next;
}
void Node::setValue(int value){
  this->value = value;
}
void Node::setNext(Node *next){
  this->next = next;
}

// List Class
class List{
  public:
    Node *head;
    List();
    void printList();
    void insertAtTheEnd(int value);
    bool isPalindrome();
    int getSize();
};

List::List(){
  head = NULL;
}

void List::printList(){
  if(head == NULL){
    cout << "Sorry empty list" << endl;
    return;
  }
  else if(head->getNext() == NULL){
    cout << head->getValue() << endl;
  }
  else{
    Node *temp = head;
    while(temp != NULL){
      cout << temp->getValue() << endl;
      temp = temp->getNext();
    }
  }
}

void List::insertAtTheEnd(int value){
  Node *newNode = new Node(value);
  if (head == NULL){
    head = newNode;
  }
  else if (head->getNext() == NULL){
    head->setNext(newNode);
  }
  else{
    Node *temp = head;
    while (temp->getNext() != NULL){
      temp = temp->getNext();
    }
    temp->setNext(newNode);
  }
}

int List::getSize(){
  if(!head)
    return 0;

  Node *current = head;
  int size = 0;
  while(current){
    size++;
    current = current->getNext();
  }
  return size;
}
/*
  Function to check if a LL is palindrome or not
  Algorithm:
    1) check stringPalindromStack in Interview Folder
*/
bool List::isPalindrome(){
  if (head == NULL)
    return false;
  
  stack<int> LLStack;
  Node *current = head;
  for(int i = 0; i < getSize(); i++){
    if(i < getSize()/2)
      LLStack.push(current->getValue());
    else{
      if(LLStack.top() == current->getValue())
        LLStack.pop();
    }
    current = current->getNext();
  }

  if(LLStack.empty())
    return true;
  return false;
}

int main(){
  List *list = new List();

  /*
  list->insertAtTheEnd(5);
  list->insertAtTheEnd(3);
  list->insertAtTheEnd(6);
  list->insertAtTheEnd(6);
  list->insertAtTheEnd(3);
  //list->insertAtTheEnd(5);
  */

  list->insertAtTheEnd(5);
  list->insertAtTheEnd(3);
  list->insertAtTheEnd(6);
  list->insertAtTheEnd(3);
  list->insertAtTheEnd(5);
  
  cout << list->getSize() << endl;
  cout << "Result: " << list->isPalindrome() << endl;
  return 0;
}
