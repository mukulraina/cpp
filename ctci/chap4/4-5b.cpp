// Implemen t a function to check if a binary tree is a binary search tree.
// Approach 2: inOrder traversal without queue (just use 1 variable)
#include <iostream>
#include <vector>
#include <limits.h>
using namespace std;

class Node{
public:
  int value;
  Node *left;
  Node *right;
  Node(int v);
};
Node::Node(int v){
  value = v;
  left = right = NULL;
}

class BST{
public:
  Node *root;
  BST();
  void insert(int v);
  void inOrderTraversal(Node *node);
  bool isBST(Node *node);
  bool isBSTHelper(Node *node, int &last_printed);
};
BST::BST(){
  root = NULL;
}
void BST::insert(int v){
  // Make a new node
  Node *newNode = new Node(v);

  // Case 1: empty tree
  if(!root){
    root = newNode;
    return;
  }

  // Case 2: Normal case
  // Traverse through the tree + find the parent
  Node *parent = NULL;
  Node *current = root;
  while(current != NULL){
    parent = current;

    if(v < current->value)
      current = current->left;
    else
      current = current->right;
  }

  // Now parent points to the parent 
  if(v < parent->value)
    parent->left = newNode;
  else
    parent->right = newNode;
}

void BST::inOrderTraversal(Node *node){
  if(node != NULL){
    inOrderTraversal(node->left);
    cout << node->value << '\t';
    inOrderTraversal(node->right);
  }
}

// modified version of inorder traversal
// So basically it builds up all sub recursive calls 
// return either 0 or 1, if any of them returned 0, its done
bool BST::isBSTHelper(Node *node, int &last){
  if(node == NULL){
    cout << true;
    return true;
  }

  // Check / Recurse left
  if(isBSTHelper(node->left,last) == false)
    return false;

  // Check current
  if(last != INT_MIN && node->value <= last)
    return false;
  else
    last = node->value;

  // Check / recurse right
  if(isBSTHelper(node->right,last) == false)
    return false;

  // This true is for the root node of all the subtrees
  {
    cout << "true from the root of the subtree" << endl;
    return true;
  }
}

bool BST::isBST(Node *node){
  int last = INT_MIN;
  return isBSTHelper(node,last);
}

int main(){
  BST *tree = new BST();

  tree->insert(10);
  tree->insert(5);
  tree->insert(15);
  tree->insert(3);
  tree->insert(6);
  tree->insert(11);

  tree->root->right->left->value = 16;

  tree->inOrderTraversal(tree->root);
  cout << endl;

  cout << tree->isBST(tree->root);
  cout << endl;

  return 0;
};