// Implemen t a function to check if a binary tree is a binary search tree.
// Approach 1: inOrder traversal with queue
#include <iostream>
#include <vector>
using namespace std;

class Node{
public:
  int value;
  Node *left;
  Node *right;
  Node(int v);
};
Node::Node(int v){
  value = v;
  left = right = NULL;
}

class BST{
public:
  Node *root;
  BST();
  void insert(int v);
  void inOrderTraversal(Node *node);
  bool isBST(Node *node);
  void isBSTHelper(Node *node, vector<int> &v);
};
BST::BST(){
  root = NULL;
}
void BST::insert(int v){
  // Make a new node
  Node *newNode = new Node(v);

  // Case 1: empty tree
  if(!root){
    root = newNode;
    return;
  }

  // Case 2: Normal case
  // Traverse through the tree + find the parent
  Node *parent = NULL;
  Node *current = root;
  while(current != NULL){
    parent = current;

    if(v < current->value)
      current = current->left;
    else
      current = current->right;
  }

  // Now parent points to the parent 
  if(v < parent->value)
    parent->left = newNode;
  else
    parent->right = newNode;
}

void BST::inOrderTraversal(Node *node){
  if(node != NULL){
    inOrderTraversal(node->left);
    cout << node->value << '\t';
    inOrderTraversal(node->right);
  }
}

void BST::isBSTHelper(Node *node, vector<int> &v){
  if(node != NULL){
    isBSTHelper(node->left,v);
    v.push_back(node->value);
    isBSTHelper(node->right,v);
  }
}

bool isSorted(vector<int> &v){
  for(int i = 0; i < v.size() - 1; i++){
    if(v[i + 1] < v[i])
      return false;
  }
  return true;
}

bool BST::isBST(Node *node){
  // Step 1: Do inorder traversal and put it into an array
  vector<int> v;
  isBSTHelper(node,v);

  // Step 2: Now that vector 'v' has the elements in inOrder fashion
  // If they are sorted its BST, if they are not then its not BST
  return isSorted(v);
}

int main(){
  BST *tree = new BST();

  tree->insert(10);
  tree->insert(5);
  tree->insert(15);
  tree->insert(3);
  tree->insert(6);
  tree->insert(11);

  cout << tree->isBST(tree->root);
  cout << endl;

  return 0;
};
