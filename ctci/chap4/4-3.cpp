//Given a sorted (increasing order) array with unique integer elements,
//write an algorithm to create a binary search tree with minimal height.
#include <iostream>
using namespace std;

// Node
class Node{
public:
  int value;
  Node *left;
  Node *right;
  Node(int v);
  Node();
};
Node::Node(){
  left = right = NULL;
}
Node::Node(int v){
  value = v;
  left = right = NULL;
}

class BST{
public:
  Node *root;
  BST();
  void inOrderTraversal(Node *node);
  Node* makeMinimalHeightBST(int *a, int low, int high);
};

BST::BST(){
  root = NULL;
}

void BST::inOrderTraversal(Node *node){
  if(node != NULL){
    inOrderTraversal(node->left);
    cout << node->value << '\t';
    inOrderTraversal(node->right);
  }
}

Node* BST::makeMinimalHeightBST(int *a, int low, int high){
  if(low > high)
    return NULL;

  // Get the middle element and make it root
  int mid = (low + high) / 2;
  Node *node = new Node(a[mid]);

  // Recursively construct the left subtree and make it left child of the root
  node->left = makeMinimalHeightBST(a,low,mid - 1);

  // Recursively construct the right subtree and make it right child of the root
  node->right = makeMinimalHeightBST(a,mid + 1,high);

  return node;
}

int main(){
  int a[] = {3,4,5,6,7,8,9,10,12,13,14};
  int size = 11;

  BST *tree = new BST();
  tree->root = tree->makeMinimalHeightBST(a,0,10);
  tree->inOrderTraversal(tree->root);
  cout << endl;

  return 0;
}
