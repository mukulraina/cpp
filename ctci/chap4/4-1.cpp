// Find the max depth of the BST
// Referrence: http://www.geeksforgeeks.org/how-to-determine-if-a-binary-tree-is-balanced/
#include <iostream>
#include <cmath>
using namespace std;

// Node
class Node{
  public:
    int value;
    Node *left;
    Node *right;
    Node();
    Node(int v);
};
Node::Node(){
  left = right = NULL;
}
Node::Node(int v){
  value = v;
  left = right = NULL;
}

// Tree
class BST{
  public:
    Node *root;
    BST();
    bool isEmpty();
    void insert(int v);
    void printInOrder(Node *node);
    int getHeight(Node *node);
    bool isBalanced(Node *node);
};
BST::BST(){
  root = NULL;
}
bool BST::isEmpty(){
  return root == NULL;
}
void BST::insert(int v){
  Node *newNode = new Node(v);
  
  // Case 1: If empty tree
  if(isEmpty()){
    root = newNode;
    return;
  }

  // Case 2: Find parent (by traversing the tree)
  Node *parent = NULL;
  Node *current = root;
  while(current != NULL){
    parent = current;
    if(v < current->value)
      current = current->left;
    else
      current = current->right;
  }

  // Now parent points to the parent 
  if(v < parent->value)
    parent->left = newNode;
  else
    parent->right = newNode;
}

void BST::printInOrder(Node *node){
  if(node != NULL){
    printInOrder(node->left);
    cout << node->value << '\t';
    printInOrder(node->right);
  }
}

int max(int a, int b){
  return ((a > b) ? a : b);
}

int BST::getHeight(Node *node){
  if(node == NULL)
    return 0;

  int leftHeight = getHeight(node->left);
  if(leftHeight == -1)
    return -1;

  int rightHeight = getHeight(node->right);
  if(rightHeight == -1)
    return -1;

  if(abs(leftHeight - rightHeight) > 1)
    return -1;
  else
    return max(leftHeight,rightHeight) + 1;
}

bool BST::isBalanced(Node *node){
  if(getHeight(node) == -1)
    return false;
  else
    return true;
}

int main(){
  BST *tree = new BST();
  tree->insert(8);
  tree->insert(6);
  tree->insert(10);
  tree->insert(4);
  tree->insert(7);
  tree->insert(9);
  tree->insert(13);
  tree->insert(3);
  tree->insert(5);
  tree->insert(12);
  tree->insert(14);

  tree->printInOrder(tree->root);
  cout << endl;
  
  cout << tree->isBalanced(tree->root) << endl;
  return 0;
}
