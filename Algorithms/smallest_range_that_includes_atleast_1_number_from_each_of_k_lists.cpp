/* 
  referrence: 
    http://goo.gl/oLoVXv
    http://goo.gl/Yh1CRC
  
  Algorithm to find the smallest range that includes atleast 1 number from each of the k lists

  For example, 
    List 1: [4, 10, 15, 24, 26] 
    List 2: [0, 9, 12, 20] 
    List 3: [5, 18, 22, 30]

  Answer: [20, 24]
*/
Algorithm:

1) make a minHeap of size k + insert 1st element from each of the lists
2) Delete the root element of the minHeap and replace it with the next element of the same list
3) Repeat this process
4) Check notes
