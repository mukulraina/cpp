#include <iostream>
using namespace std;

#define MAX_SIZE 5

class Stack{
public:
  int a[MAX_SIZE];
  int top;
  Stack();
  void push(int v);
  int pop();
  int peek();
  void printStack();
};
Stack::Stack(){
  top = -1;
}
// Time: O(1)
void Stack::push(int v){
  // Case 1: full stack
  if(top == MAX_SIZE - 1)
    cout << "sorry stack is full: stack overflow" <<endl;
  else{
    top++;
    a[top] = v;
  }
}
// Time: O(1)
int Stack::pop(){
  // Case 1: empty stack
  if(top == -1){
    cout << "sorry cant pop an empty stack: stack underflow" << endl;
    return -1;
  }
  else{
    top--;
    return a[top + 1];
  }
}
int Stack::peek(){
  // Case 1: empty stack
  if(top == -1){
    cout << "sorry can not peek, empty stack" << endl;
    return -1;
  }
  else
    return a[top];
}
void Stack::printStack(){
  // Case 1: empty stack
  if(top == -1)
    cout << "sorry cant print anything, empty stack" <<endl;
  else{
    for(int i = 0; i <= top; i++){
      cout << a[i] << '\t';
    }
    cout << endl;
  }
}
int main(){
  Stack stack;

  // Poping an empty stack
  cout << "Just poped: " << stack.pop() << endl;

  // Push some elements
  stack.push(10);
  stack.push(20);
  stack.push(30);
  
  // Print stack
  stack.printStack();
  cout << "peek value: " << stack.peek() << endl;
  cout << endl;

  // Pop and push
  cout << "Just poped: " << stack.pop() << endl;
  stack.push(40);
  stack.printStack();
  cout << "peek value: " << stack.peek() << endl;
  cout << endl;

  //Try fulling the stack now
  stack.push(50);
  stack.push(60);
  stack.printStack();
  cout << "peek value: " << stack.peek() << endl;

  stack.push(70);
  stack.printStack();
  cout << "peek value: " << stack.peek() << endl;
  cout << endl;

  return 0;
}
