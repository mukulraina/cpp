#include <iostream>
using namespace std;

class Node{
public:
  int value;
  Node *next;
  Node(int v);
};
Node::Node(int v){
  value = v;
}

class StackLL{
public:
  Node *top;
  void push(int v);
  int pop();
  int peek();
  void printStack();
};

// Time: O(1)
void StackLL::push(int v){
  // Make a newNode with value v
  Node *newNode = new Node(v);

  // Case 1: empty stack
  if(top == NULL){
    top = newNode;
  }
  // Case 2: 1 or more nodes
  else{
    // Insert at the start of the list
    newNode->next = top;
    top = newNode;
  }
}

// Time: O(1)
int StackLL::pop(){
  // Case 1: empty stack
  if(top == NULL){
    cout << "sorry empty stack, so can't pop a value" << endl;
    return -1;  
  }
  else{
    int v = top->value;
    Node *t = top;
    top = top->next;
    delete t;
    return v;
  }
}

// Time: O(1)
int StackLL::peek(){
  if(top == NULL){
    cout << "sorry empty stack can't peek" << endl;
    return -1;
  }
  else
    return top->value;
}

// Time: O(n)
void StackLL::printStack(){
  if(top == NULL){
    cout << "sorry empty stack, can't print" << endl;
  }
  else{
    Node *c = top;
    while(c != NULL){
      cout << c->value << '\t';
      c = c->next;
    }
    cout << endl;
  }
}

// Test Client
int main(){
  StackLL *stack = new StackLL();

  // Poping an empty stack
  cout << "Just poped: " << stack->pop() << endl;

  // Push some elements
  stack->push(10);
  stack->push(20);
  stack->push(30);
  
  // Print stack
  stack->printStack();
  
  cout << "peek value: " << stack->peek() << endl;
  cout << endl;

  // Pop and push
  cout << "Just poped: " << stack->pop() << endl;
  stack->push(40);
  stack->printStack();
  cout << "peek value: " << stack->peek() << endl;
  cout << endl;

  //Try fulling the stack now
  stack->push(50);
  stack->push(60);
  stack->printStack();
  cout << "peek value: " << stack->peek() << endl;

  stack->push(70);
  stack->printStack();
  cout << "peek value: " << stack->peek() << endl;
  cout << endl;
  
  return 0;
}
