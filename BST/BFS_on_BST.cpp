// Given a binary tree, design an algorithm which creates a linked list of all the nodes 
// at each depth (e.g., if you have a tree with depth D,you'll have D linked lists).
#include <iostream>
#include <queue>
using namespace std;

class Node{
public:
  int value;
  Node *left;
  Node *right;
  Node(int v);
};
Node::Node(int v){
  value = v;
  left = right = NULL;
}

class BST{
public:
  Node *root;
  BST();
  void insert(int v);
  void inOrderTraversal(Node *node);
  void BFSTraversal(Node *node);
};
BST::BST(){
  root = NULL;
}
void BST::insert(int v){
  // Make a new node
  Node *newNode = new Node(v);

  // Case 1: empty tree
  if(!root){
    root = newNode;
    return;
  }

  // Case 2: Normal case
  // Traverse through the tree + find the parent
  Node *parent = NULL;
  Node *current = root;
  while(current != NULL){
    parent = current;

    if(v < current->value)
      current = current->left;
    else
      current = current->right;
  }

  // Now parent points to the parent 
  if(v < parent->value)
    parent->left = newNode;
  else
    parent->right = newNode;
}

void BST::inOrderTraversal(Node *node){
  if(node != NULL){
    inOrderTraversal(node->left);
    cout << node->value << '\t';
    inOrderTraversal(node->right);
  }
}

// Time: O(n) where n is the number of nodes in the tree
void BST::BFSTraversal(Node *node){
  queue<Node*> q;

  // Put the first node in the queue
  q.push(node);

  while(q.empty() != true){
    // print first value
    node = q.front();
    cout << node->value << '\t';

    // Insert the left and right child of "node" in queue
    if(node->left != NULL)
      q.push(node->left);
    
    if(node->right != NULL)
    q.push(node->right);

    // Delete the first node from the queue as we dont need it anymore
    q.pop();
  }
}

int main(){
  BST *tree = new BST();

  tree->insert(10);
  tree->insert(5);
  tree->insert(15);
  tree->insert(3);
  tree->insert(6);
  tree->insert(11);

  tree->BFSTraversal(tree->root);
  cout << endl;

  return 0;
};
