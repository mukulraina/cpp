#include <iostream>
using namespace std;

class Node{
public:
  int value;
  Node *left;
  Node *right;
  Node(int v);
};
Node::Node(int v){
  this->value = v;
  left = right = NULL;
}

class BST{
public:
  Node *root;
  BST();
  void insert(int v);
  int getParent(int v);
  void inOrderTraversal(Node *n);
  };
BST::BST(){
  root = NULL;
}

void BST::insert(int v){
  // Make a new node with value v
  Node *newNode = new Node(v);

  //Case 1: 0 node
  if(root == NULL){
    root = newNode;
    return;
  }

  //Case 2: 1 or more nodes
  Node *p = NULL;
  Node *c = root;
  
  while(c != NULL){
    p = c;
    if(v < c->value)
      c = c->left;
    else
      c = c->right;
  }
  // now p points to the parent where node needs to be inserted
  if(v < p->value)
    p->left = newNode;
  else
    p->right = newNode;
}

void BST::inOrderTraversal(Node *n){
  if(n != NULL){
    inOrderTraversal(n->left);
    cout << n->value << '\t';
    inOrderTraversal(n->right);
  }
}

int BST::getParent(int v){
  if(root->value == v){
    cout << "sorry root node doesn't have a parent" <<endl;
    return -1;
  }
  else{
    Node *c = root;
    Node *p = NULL;
    while(c != NULL){
      if(c->value == v)
        break;
      p = c;
      if(v < c->value)
        c = c->left;
      else
        c = c->right;
    }
    // Now if c is null that means the value was not found in the tree
    if(c == NULL){
      cout << "sorry the value was not found in the tree" << endl;
      return -1;
    }
    else
    // If c is not null, that means c points the node v and p points to its parent
      return p->value;
  }
}

int main(){
  int a[] = {10,8,14,6,9,12,15};
  BST *tree = new BST();

  for (int i = 0; i < 7; i++){
    tree->insert(a[i]);
  }

  tree->inOrderTraversal(tree->root);
  cout << endl;

  cout << tree->getParent(99) << endl;
  return 0;
}
