#include <iostream>
using namespace std;

class Node{
public:
  int value;
  Node *left;
  Node *right;
  Node(int v);
};
Node::Node(int v){
  this->value = v;
  left = right = NULL;
}

class BST{
public:
  Node *root;
  BST();
  bool isEmpty();
  void insert(int v);
  void inOrderTraversal(Node *node);
  int predecessor(int v);
  int getMin(Node *node);
  int getMax(Node *node);
  bool recursiveSearch(Node *node, int v);
};
BST::BST(){
  root = NULL;
}
bool BST::isEmpty(){
  return root == NULL;
}
void BST::insert(int v){
  // Make a new Node with value "n"
  Node *newNode = new Node(v);

  // Check if tree is empty
  if(isEmpty()){
    root = newNode;
    return;
  }

  // Find Parent by traversing the tree
  Node *parent = NULL;
  Node *current = root;

  while(current != NULL){
    parent = current;
    if(v < current->value)
      current = current->left;
    else
      current = current->right;
  }

  // Insert it as a child to the parent
  if(v < parent->value)
    parent->left = newNode;
  else
    parent->right = newNode;
}

void BST::inOrderTraversal(Node *node){
  if(node != NULL){
    inOrderTraversal(node->left);
    cout << node->value << '\t';
    inOrderTraversal(node->right);
  }
}

int BST::getMin(Node *node){
  if(node->left == NULL)
    return node->value;

  return getMin(node->left);
}

int BST::getMax(Node *node){
  if(node->right == NULL)
    return node->value;

  return getMax(node->right); 
}

bool BST::recursiveSearch(Node *node, int v){
  if(node == NULL)
    return false;

  if(node->value == v)
    return true;
  else if(v < node->value)
    return recursiveSearch(node->left,v);
  else
    return recursiveSearch(node->right,v);
}

int BST::predecessor(int v){
  // Case 1: empty tree
  if(isEmpty())
    return -1;

  // Case 2: not present in the tree
  if(!recursiveSearch(root,v))
    return -1;

  // Case 3: lowest value in the subtree
  if(v == getMin(root))
    return -1;

  // Locate the node
  Node *c = root;
  while(c->value != v){
    if(v < c->value)
      c = c->left;
    else
      c = c->right;
  }

  // Case 4a: if left subtree is there
  if(c->left != NULL)
    return getMax(c->left);

  // Case 4b: if no left subtree
  Node *pred = NULL;
  Node *p = root;
  while(p != c){
    if(v < p->value)
      p = p->left;
    else{
      pred = p;
      p = p->right;
    }
  }

  return pred->value;
}

int main(){
  int a[] = {10,8,14,6,9,12,15};
  BST *tree = new BST();

  for (int i = 0; i < 7; i++){
    tree->insert(a[i]);
  }

  cout << "min: " << tree->getMin(tree->root) << endl;
  cout << "max: " << tree->getMax(tree->root) << endl;

  // Search test cases
  cout << "Search 10: " << tree->recursiveSearch(tree->root,10) << endl;
  cout << "Search 6: " << tree->recursiveSearch(tree->root,6) << endl;
  cout << "Search 14: " << tree->recursiveSearch(tree->root,14) << endl;
  cout << "Search 99: " << tree->recursiveSearch(tree->root,99) << endl;
  
  std::cout<<"The predecessor of 8 is: "<<tree->predecessor(8)<<'\n';
  std::cout<<"The predecessor of 15 is: "<<tree->predecessor(15)<<'\n';
  std::cout<<"The predecessor of 12 is: "<<tree->predecessor(12)<<'\n';
  std::cout<<"The predecessor of 6 is: "<<tree->predecessor(6)<<'\n';
  std::cout<<"The predecessor of 99 is: "<<tree->predecessor(99)<<'\n';
  return 0;
}