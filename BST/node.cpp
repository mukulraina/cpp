/* This class implements a tree node in C++ */
#include <iostream>

/* Node Class */
class Node{
  private:
    int value;
    Node *left;
    Node *right;
  public:
    Node();
    Node(int n);
    int getValue();
    Node* getLeft();
    Node* getRight();
    void setValue(int n);
    void setLeft(Node *l);
    void setRight(Node *r);
    void printInfo();
};
Node::Node(){
  left = right = NULL;
}
Node::Node(int n){
  value = n;
}
int Node::getValue(){
  return value;
}
Node* Node::getLeft(){
  return left;
}
Node* Node::getRight(){
  return right;
}
void Node::setValue(int n){
  value = n;
}
void Node::setLeft(Node *l){
  left = l;
}
void Node::setRight(Node *r){
  right = r;
}
void Node::printInfo(){
  std::cout<<"Value: "<<value<<'\n';
}

/* Test Client */
int main(){
  Node obj1;
  obj1.setValue(1);
  std::cout<<"Info about Object 1"<<'\n';
  obj1.printInfo();
  Node obj2;
  obj2.setValue(2);
  std::cout<<"Info about Object 2"<<'\n';
  obj2.printInfo();
  return 0;
}
