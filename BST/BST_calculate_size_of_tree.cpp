// Program to calculcate # of leaf nodes
#include <iostream>
using namespace std;

// Node Class
class Node{
  public:
    int value;
    Node *left;
    Node *right;
    Node();
    Node(int value);
    void printInfo();
};
Node::Node(){
  left = right = NULL;
}
Node::Node(int value){
  this->value = value;
  left = right = NULL;
}
void Node::printInfo(){
  cout << "value: " << value << endl;
}


// Tree Class
class BST{
  public:
    Node *root;
    BST();
    bool isEmpty();
    void insert(int v);
    void inOrderTraversal(Node *node);
    int getSize(Node *node);
};
BST::BST(){
  root = NULL;
}
bool BST::isEmpty(){
  return root == NULL;
}
void BST::insert(int v){
  // Make a node
  Node *newNode = new Node(v);

  // Case 1: if tree is empty
  if(isEmpty()){
    root = newNode;
    return;
  }

  // Normal Case
  Node *current = root;
  Node *parent = NULL;
  while(current != NULL){
    parent = current;
    if (v < current->value)
      current = current->left;
    else
      current = current->right;
  }

  // Now current points to the parent
  // Decide whether to put as left or right child
  if(v < parent->value)
    parent->left = newNode;
  else
    parent->right = newNode;
}

void BST::inOrderTraversal(Node *node){
  if(node != NULL){
    inOrderTraversal(node->left);
    cout << node->value << endl;
    inOrderTraversal(node->right);
  }
}

// Time: O(n)
// NOTE: 
int BST::getSize(Node *node){
  if(node == NULL)
    return 0;
  else{
    return getSize(node->left) + 1 + getSize(node->right);
  }
}

// Test Client
int main(){
  BST *tree = new BST();

  tree->insert(8);
  tree->insert(3);
  tree->insert(10);
  tree->insert(1);
  tree->insert(6);
  tree->insert(14);
  tree->insert(4);
  tree->insert(7);
  tree->insert(13);
  
  cout << tree->getSize(tree->root) << endl;
  return 0;
}
