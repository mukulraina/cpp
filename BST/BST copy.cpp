// Program to implement BST
#include <iostream>
using namespace std;

// Node Class
class Node{
  public:
    int value;
    Node *left;
    Node *right;
    Node();
    Node(int value);
    void printInfo();
};
Node::Node(){
  left = right = NULL;
}
Node::Node(int value){
  this->value = value;
  left = right = NULL;
}
void Node::printInfo(){
  cout << "value: " << value << endl;
}


// Tree Class
class BST{
  public:
    Node *root;
    BST();
    bool isEmpty();
    void insert(int value);
    void inOrderTraversal(Node *node);
    void postOrderTraversal(Node *node);
    void preOrderTraversal(Node *node);
    bool recursiveSearch(Node *node, int value);
    bool iterativeSearch(int value);
    int getMin(Node *node);
    int getMax(Node *node);
    int successor(int value);
    int predecessor(int value);
    int getParentValue(int v);
    void deletion(int v);
};
BST::BST(){
  root = NULL;
}

bool BST::isEmpty(){
  return root == NULL;
}

// Insertion - O(log n)
void BST::insert(int v){
  // Make a new Node with value "n"
  Node *newNode = new Node(v);

  // Check if tree is empty
  if(isEmpty()){
    root = newNode;
    return;
  }

  // Find Parent by traversing the tree
  Node *parent = NULL;
  Node *current = root;

  while(current != NULL){
    parent = current;
    if(v < current->value)
      current = current->left;
    else
      current = current->right;
  }

  // Insert it as a child to the parent
  if(v < parent->value)
    parent->left = newNode;
  else
    parent->right = newNode;
}

// Inorder traversal - O(n)
void BST::inOrderTraversal(Node *node){
  if(node != NULL){
    inOrderTraversal(node->left);
    cout << node->value << endl;
    inOrderTraversal(node->right);
  }
}

// Postorder traveral 
void BST::postOrderTraversal(Node *node){
  if(node != NULL){
    postOrderTraversal(node->left);
    postOrderTraversal(node->right);
    cout << node->value << endl;
  }
}

// PreOrder Traversal
void BST::preOrderTraversal(Node *node){
  if(node != NULL){
    cout << node->value << endl;
    preOrderTraversal(node->left);
    preOrderTraversal(node->right);
  }
}

// Recursive Lookup - O(log n)
bool BST::recursiveSearch(Node *node, int v){
  if(node == NULL)
    return false;
  else{
    if(node->value == v)
      return true;
    else if(v < node->value)
      return recursiveSearch(node->left,v);
    else
      return recursiveSearch(node->right,v);
  }
}

// Iterative Lookup - O(log n)
bool BST::iterativeSearch(int v){
  if(isEmpty()){
    cout << "Empty Tree" << endl;
    return false;
  }

  Node *current = root;
  while(current != NULL){
    if(current->value == v)
      return true;

    else if(v < current->value)
      current = current->left;

    else
      current = current->right;
  }

  // If the program has reached here that means
  // It couldn't find the value and the only other
  // reason it came here cos current became NULL
  return false;
}

// Get the smallest element in the tree
// Need to pass root/node if you want to find min in a subtree
// Time: O(h) / O(log n)
int BST::getMin(Node *node){
  Node *current = node;
  while(current->left != NULL){
    current = current->left;
  }
  return current->value;
}


// Get the biggest element in the tree
int BST::getMax(Node *node){
  Node *current = node;
  while(current->right != NULL){
    current = current->right;
  }
  return current->value;
}

// Get the inorder successor in a BST
int BST::successor(int value){
  // Check if tree is empty or not
  if(isEmpty())
    return -1;

  // if v is the biggest number in the tree
  if(value == getMax(root))
    return -1;

  // Check if v is in the tree or not
  if(!iterativeSearch(value))
    return -1;

  // Find the node (After this loop current points to "v" in the tree)
  Node *current = root;
  while(current->value != value){
    if(value < current->value)
      current = current->left;
    else
      current = current->right;
  }

  // Case 1: If right subtree is not empty
  if(current->right != NULL)
    return getMin(current->right);

  // Case 2: If the right subtree is empty
  Node *succ = NULL;
  Node *p = root;

  while(p != NULL && p != current){
    if(value < p->value){
      succ = p;
      p = p->left;
    }
    else 
      p = p->right;
  }
  return succ->value;
}

int BST::getParentValue(int v){
  // Case 1: Empty Tree
  if(isEmpty())
    return -1;

  // Case 2: If its the root - no parent
  if(root->value == v)
    return -1;

  // Case 3: "v" is not in the tree
  if(!recursiveSearch(root,v))
    return -1;

  // Case 4: Normal case
  Node *parent = NULL;
  Node *current = root;
  // Traverse until current hits the "v" node
  while(current->value != v){
    parent = current;
    if(v < current->value)
      current = current->left;
    else
      current = current->right;
  }
  return parent->value;
}

int BST::predecessor(int v){
  // Case 1: Empty Tree
  if(isEmpty())
    return -1;

  // Case 2: Check if "v" exists in the tree or not
  if(!recursiveSearch(root,v))
    return -1;

  // Case 3: If its the smallest no
  if(v == getMin(root))
    return -1;

  // Locate the node
  Node *current = root;
  while(current->value != v){
    if(v < current->value)
      current = current->left;
    else
      current = current->right;
  }

  // Case 4a: If the node has no left subtree
  // Return the biggest element of its left subtree 
  if(current->left != NULL)
    return getMax(current->left);

  // Case 4b: If the node has left subtree
  // Traverse from the root
  Node *pred = NULL;
  Node *p = root;
  while(p != current){
    if(v < p->value)
      p = p->left;
    else{
      pred = p;
      p = p->right;
    }
  }

  // Now pred points to predecessor node
  return pred->value;
}

// Cant name the funciton "delete" its a keyword
void BST::deletion(int v){
  // Case 1: empty tree
  if(isEmpty())
    return;

  // Case 2: 1 node
  if(root->value == v){
    Node *temp = root;
    root = NULL;
    delete temp;
    return;
  }

  // Case 3: Normal Cases
  Node *current = root;
  Node *parent = NULL;
  int flag = 0, deletion_case;

  // Try locating the node, if not return
  while(current != NULL && current->value != v){
    parent = current;
    if(v < current->value)
      current = current->left;
    else
      current = current->right;
  }

  // If current is NULL, that means the value is not there
  if(current == NULL)
    return;

  // Lets find the deletion case
  if(current->left == NULL && current->right == NULL)
    deletion_case = 0; // 0 children
  else if(current->left != NULL && right != NULL)
    deletion_case = 2; // 2 children 
  else
    deletion_case = 1; // 1 child

  // Deletion Case 0: 0 Children
  if(deletion_case == 0){
    if(parent->left == current) // If current is left child
      parent->left = NULL;
    else
      parent->right = NULL; // If current is right child
    delete current; // delete current now
  }

  // Deletion Case 1: 1 children
  if(deletion_case == 1){
    // If current is the left child of its parent
    if(parent->left == current){
      if(current->left != NULL) // 1 child is on left of current
        parent->left = current->left;
      else
        parent->left = current->right; // 1 child is on the right of current
    }
    else{
      if(current->right != NULL)
        parent->right = current->right; 
      else
        parent->right = current->left; 
    }
    // Delete the node
    delete current;
  }

  if(deletion_case == 2){
    int successorValue = successor(v);
    deletion(successorValue);
    current->value = successorValue; 
  }
}

int main(){
  BST *tree = new BST();
  BST *deletionTree = new BST();

  int a[] = {10,8,14,6,9,12,15};
  int d[] = {5,2,12,-4,3,4,9,21,19,25};
  for (int i = 0; i < 7; i++){
    tree->insert(a[i]);
  }
  for(int i =0 ; i< 10; i++){
    deletionTree->insert(d[i]);
  }

  tree->inOrderTraversal(tree->root);

  // Search test cases
  cout << "Search 10: " << tree->recursiveSearch(tree->root,10) << endl;
  cout << "Search 6: " << tree->recursiveSearch(tree->root,6) << endl;
  cout << "Search 14: " << tree->recursiveSearch(tree->root,14) << endl;
  cout << "Search 99: " << tree->recursiveSearch(tree->root,99) << endl;

  // Search test cases
  cout << endl;
  cout << "Search 10: " << tree->iterativeSearch(10) << endl;
  cout << "Search 6: " << tree->iterativeSearch(6) << endl;
  cout << "Search 14: " << tree->iterativeSearch(14) << endl;
  cout << "Search 99: " << tree->iterativeSearch(99) << endl;
  
  cout << "The smallest element in the tree is: " << tree->getMin(tree->root) << endl;
  cout << "The largest element in the tree is: " << tree->getMax(tree->root) << endl;
  
  std::cout<<'\n';
  std::cout<<"Parent of 10 is: "<<tree->getParentValue(10)<<'\n';
  std::cout<<"Parent of 8 is: "<<tree->getParentValue(8)<<'\n';
  std::cout<<"Parent of 9 is: "<<tree->getParentValue(9)<<'\n';
  std::cout<<"Parent of 99 is: "<<tree->getParentValue(99)<<'\n';

  std::cout<<"The successor of 99 is: "<<tree->successor(99)<<'\n';
  std::cout<<"The successor of 8 is: "<<tree->successor(8)<<'\n';
  std::cout<<"The successor of 10 is: "<<tree->successor(10)<<'\n';
  std::cout<<"The successor of 15 is: "<<tree->successor(15)<<'\n';
  std::cout<<"The predecessor of 8 is: "<<tree->predecessor(8)<<'\n';
  std::cout<<"The predecessor of 15 is: "<<tree->predecessor(15)<<'\n';
  std::cout<<"The predecessor of 12 is: "<<tree->predecessor(12)<<'\n';
  std::cout<<"The predecessor of 6 is: "<<tree->predecessor(6)<<'\n';
  std::cout<<"The predecessor of 99 is: "<<tree->predecessor(99)<<'\n';
  
  cout<< endl;
  deletionTree->inOrderTraversal(deletionTree->root);
  //deletionTree->deletion(3);
  //deletionTree->deletion(-4);
  //deletionTree->deletion(4);
  deletionTree->deletion(12);


  cout<<endl;
  deletionTree->inOrderTraversal(deletionTree->root);
  

  return 0;
}
