// This program shows how to implement a Binary Search Tree 
// reference: http://leetcode.com/2010/04/binary-search-tree-in-order-traversal.html
// reference: http://www.geeksforgeeks.org/find-the-minimum-element-in-a-binary-search-tree/
// BST operations: 3 types of traversal, insert, delete, getMin, getMax, find, parent, successor, predecessor
#include <iostream>

// Node class
class Node{
  private:
    int value;
    Node *left;
    Node *right;
  public:
    Node();
    Node(int n);
    int getValue();
    Node* getLeft();
    Node* getRight();
    void setValue(int n);
    void setLeft(Node *l);
    void setRight(Node *r);
};
Node::Node(){
  left = right = NULL;
}
Node::Node(int n){
  value = n;
}
int Node::getValue(){
  return value;
}
Node* Node::getLeft(){
  return left;
}
Node* Node::getRight(){
  return right;
}
void Node::setValue(int n){
  value = n;
}
void Node::setLeft(Node *l){
  left = l;
}
void Node::setRight(Node *r){
  right = r;
}

// Tree class
class BST{
  private:
    Node * root;
  public:
    BST();
    Node* getRoot();
    void insert(Node *node, int n);
    void inOrder(Node *node);
    void preOrder(Node *node);
    void postOrder(Node *node);
    Node* search(Node *node, int n);
    Node* iterativeSearch(Node *node, int n);
    int getMin(Node *node);
    int getMax(Node *node);
    int successor(Node *node, int n);
    int predecessor(Node *node, int n);
    int getParentValue(Node *node, int n);
    void deleteNode(Node *node, int n);
};
/* Constructor */
BST::BST(){
  root = NULL;
}

/* Returns root of the tree */
Node* BST::getRoot(){
  return root;
}

/*
 * Inserts a node into a BST
 * @param   root      Pointer to the root of the BST
 * @param   n         Value of the node to be inserted
 * @param   prev      Keep tracks of the parent
 * @param   p         Keep tracks of the location of insertion
 * @param   *newNode  Pointer to the dynamically allocated new node with value n to be inserted
 * Time:    O(h)/O(log n) on a tree of height h.
 */
void BST::insert(Node *node, int n){
  Node *prev = NULL;
  Node *p = node;
  while(p != NULL){
    prev = p;
    if(n < p->getValue())
      p = p->getLeft();
    else
      p = p->getRight();
  }
  Node * newNode = new Node(n);
  if(prev == NULL)
    root = newNode;
  else if(n < prev->getValue())
    prev->setLeft(newNode);
  else
    prev->setRight(newNode);
}

/*
 * Inorder traversal of the BST
 * @param *root  pointer to the root of the BST
 * Time:  O(n) time to walk an n-node binary search tree, 
 *        since after the initial call, the procedure calls itself recursively
 *        exactly twice for each node in the tree—once for its left child and 
 *        once for its right child
 */
void BST::inOrder(Node *node){
  if(node != NULL){
    inOrder(node->getLeft());
    std::cout<<node->getValue()<<'\n';
    inOrder(node->getRight());
  }
}

/*
 * preOrder traversal of BST
 */
void BST::preOrder(Node *node){
  if(node != NULL){
    std::cout<<node->getValue()<<'\n';
    preOrder(node->getLeft());
    preOrder(node->getRight());
  }
}

/*
 * postOrder traversal of BST
 */
void BST::postOrder(Node *node){
  if(node != NULL){
    postOrder(node->getLeft());
    postOrder(node->getRight());
    std::cout<<"\t"<<node->getValue()<<'\n';
  }
}

/*
 * Searches a node in the BST
 * @param   node  Points to the root of the BST
 * @param   n     Value to be searched
 * @return  Returns pointer to the node (if found in tree) or NULL if not found
 * Time:    O(h)  where h is the height of the tree
 */
Node *BST::search(Node *node, int n){
  if(node == NULL || node->getValue() == n)
    return node;
  else if(n < node->getValue())
    return search(node->getLeft(),n);
  else
    return search(node->getRight(),n);
}

/*
 * Searches a node in the BST iteratively (more efficient)
 * @param   node  Points to the root of the BST
 * @param   n     Value to be searched
 * @return  Returns pointer to the node (if found in tree) or NULL if not found
 * Time:    O(h)  where h is the height of the tree
 */
Node *BST::iterativeSearch(Node *node, int n){
  while(node != NULL && node->getValue() != n){
    if(n < node->getValue())
      node = node->getLeft();
    else
      node = node->getRight();
  }
  return node;
}

/*
 * Returns the smallest element in the tree
 * @param node  Points to the root of the tree initially
 * @return      Smallest value 
 * Note:        After the while loop node points to the Node with smallest value in tree 
 * Time:        O(h) / O(log n) where h is the height of the tree
 */
int BST::getMin(Node *node){
  while(node->getLeft() != NULL){
    node = node->getLeft();
  }
  return node->getValue();
}

/*
 * Returns the biggest element in the tree
 * @return      Smallest value 
 * Note:        After the while loop node points to the Node with biggest value in tree 
 * Time:        O(h) / O(log n) where h is the height of the tree
 */
int BST::getMax(Node *node){
  while(node->getRight() != NULL){
    node = node->getRight();
  }
  return node->getValue();
}

/*
 * Returns the successor of a node
 * @param n         Value for which we have to find a successor 
 * @param currNode  Node for which we have to find the successor
 * @param *node     Points to the root of the tree at the start of the function call
 * @param *succ     Points to the successor 
 * @return          Returns the successor of value n (could have returned the actual node too)
 * @referrence      http://www.geeksforgeeks.org/inorder-successor-in-binary-search-tree/
 * @referrence      http://algorithmsandme.blogspot.ca/2013/08/binary-search-tree-inorder-successor.html#.U9aNRIBdWaY
 * Time:            O(logN)/O(h)
 */
int BST::successor(Node *node, int n){
  /* Check if the node is in the BST or not*/
  if(node == NULL){
    std::cout<<"Sorry Empty Tree"<<'\n';
    return -1;  
  }
  Node *currNode = search(node,n);
  if(currNode == NULL){
    std::cout<<"Sorry node with value "<<n<<" was not found"<<'\n';
    return -1;
  }
  /* If its the maximum value then no successor */
  if (n == getMax(node)){
    std::cout<<"Biggest value: no successor"<<'\n';
    return -1;
  }
  /* That means the value is in the tree*/
  /* If the right subtree is not empty */
  if(currNode->getRight() != NULL)
    return getMin(currNode->getRight());
  /* If the right subtree is empty, start from the root (not actual node) */
  Node *succ = new Node();
  while(node != NULL){
    if(n < node->getValue()){
      /* succ points to the root */
      succ = node;
      node = node->getLeft();
    }
    else if(n > node->getValue()){
      /* just iterate the root, do not change the succ*/
      node = node->getRight();
    }
    /* if you found the value */
    else 
      break;
  }
  return succ->getValue();
}

/*
 * Returns the predecessor of a value in the tree
 * @param *node   Pointer to the root of the tree
 * @param n       Value for which we have to find the predecessor
 * @return        Returns the predecssor of the value n
 * Time:          O(logN)
 */
int BST::predecessor(Node *node, int n){
  /* Check if the node is in the BST or not*/
  if(node == NULL){
    std::cout<<"Sorry Empty Tree"<<'\n';
    return -1;  
  }
  Node *currNode = search(node,n);
  if(currNode == NULL){
    std::cout<<"Sorry node with value "<<n<<" was not found"<<'\n';
    return -1;
  }
  /* If its the minimum value then no predecessor */
  if (n == getMin(node)){
    std::cout<<"Smallest value: no predecessor"<<'\n';
    return -1;
  }
  /* That means the value is in the tree*/
  Node *pred = NULL;
  Node *current = node; //Points to the root of the tree now
  while(current != NULL && current->getValue() != n){
    if(n < current->getValue())
      current = current->getLeft();
    else{
      pred = current;
      current = current->getRight();
    }
  }
  if(current != NULL && current->getLeft() != NULL){
    return getMax(current->getLeft());
  }
  return pred->getValue();
}
/*
 * Returns the value of the parent node
 * @param node    Points to the root of the tree
 * @param n       Value for whose we have to find the parent
 * @param parent  Points to the parent of n (if n is in the tree)
 * @return        Returns the value of the parent node
 */
int BST::getParentValue(Node *node, int n){
  /* Empty tree */
  if(node == NULL){
    std::cout<<"Sorry empty tree"<<'\n';
    return -1;
  }
  /* If n is the root, then we don't have a parent to return */
  if(node->getValue() == n){
    std::cout<<"Sorry no parent for the root node"<<'\n';
    return -1;
  }
  Node *parent = NULL;
  Node *current = node; // Points to the root initially
  while (current != NULL && current->getValue() != n){
    parent = current;
    if(n < current->getValue())
      current = current->getLeft();
    else
      current = current->getRight();
  }
  /* If current is pointing to null, that means n is not in the tree */
  if(current == NULL){
    std::cout<<"Sorry "<<n<<" is not in the tree"<<'\n';
    return -1;
  }
  /* If current is not pointing to NULL, its pointing to the n and parent is pointing to n's parent */
  else{
    return parent->getValue();
  }
}

/*
 * Deletes a node from the BST
 * @param node  Points to the root of the BST
 * @return      Deletes the key and returns the new root 
 * referrence   http://geeksquiz.com/binary-search-tree-set-2-delete/
 */
void BST::deleteNode(Node *node, int n){
  /* Empty Tree */
  if(node == NULL){
    std::cout<<"Sorry empty tree"<<'\n';
    return ;
  }

  Node *current = node;
  Node *succ;
  Node *prev;
  int flag = 0, deletionCase;

  /* Find the location of the node to be deleted */
  while(current != NULL && current->getValue() != n){
    if(n < current->getValue()){
      prev = current;
      current = current->getLeft();
    }
    else{
      prev = current;
      current = current->getRight();
    }
  }

  if(current == NULL){
    std::cout<<"Sorry the value you want to delete is not in the tree"<<'\n';
    return ;
  }

  /* Find out the case of deletion */
  if(current->getLeft() == NULL && current->getRight() == NULL)
    deletionCase = 1; //Node has no children
  else if(current->getLeft() != NULL && current->getRight() != NULL)
    deletionCase = 3; //Node has 2 children
  else
    deletionCase = 2; //Node only has 1 child
  
  /* Deletion case 1: Node has no children */
  if(deletionCase == 1){
    if(prev->getLeft() == current) //If the node is a left child
      prev->setLeft(NULL);
    else
      prev->setRight(NULL);
    delete(current);
  }
  /* Deletion case 2: Node only has 1 child */
  if(deletionCase == 2){
    if(prev->getLeft() == current){
      /* If the node is a left child */
      if(current->getLeft() != NULL)
        prev->setLeft(current->getLeft());
      else
        prev->setLeft(current->getRight());
    }
    else{
      /* If the node is a right child */
      if(current->getRight() != NULL){
        prev->setRight(current->getRight());
      }
    }
    delete(current);
  }
  /* Deletion case 3: Node has 2 children */
  if(deletionCase == 3){
    int successorValue = successor(node,current->getValue());
    deleteNode(node,successorValue);
    current->setValue(successorValue);
  }
}

/* Test Client */
int main(){
  BST *tree = new BST();
  //int data[] = {20,8,22,4,12,10,14};
  int data[] = {10,8,14,6,9,12,15};
  for (int i = 0; i < 7 ; i++){
    tree->insert(tree->getRoot(), data[i]);
  }
  std::cout<<"inOrder traversal"<<'\n';
  tree->inOrder(tree->getRoot());
  std::cout<<"preOrder traversal\n";
  tree->preOrder(tree->getRoot());
  std::cout<<"postOrder traversal\n";
  tree->postOrder(tree->getRoot());
  std::cout<<"Doing recursive search in tree"<<'\n';
  std::cout<<"Checking if 87 is in the tree or not: "<<tree->search(tree->getRoot(),87)<<'\n';
  std::cout<<"Checking if 41 is in the tree or not: "<<tree->search(tree->getRoot(),41)<<'\n';
  std::cout<<"Doing iterative search in tree"<<'\n';
  std::cout<<"Checking if 87 is in the tree or not: "<<tree->iterativeSearch(tree->getRoot(),87)<<'\n';
  std::cout<<"Checking if 41 is in the tree or not: "<<tree->iterativeSearch(tree->getRoot(),41)<<'\n';
  std::cout<<"The smallest element in the tree is: "<<tree->getMin(tree->getRoot())<<'\n';
  std::cout<<"The biggest element in the tree is: "<<tree->getMax(tree->getRoot())<<'\n';
  std::cout<<"The successor of 99 is: "<<tree->successor(tree->getRoot(),99)<<'\n';
  std::cout<<"The successor of 8 is: "<<tree->successor(tree->getRoot(),8)<<'\n';
  std::cout<<"The successor of 10 is: "<<tree->successor(tree->getRoot(),10)<<'\n';
  std::cout<<"The successor of 15 is: "<<tree->successor(tree->getRoot(),15)<<'\n';
  std::cout<<"The predecessor of 8 is: "<<tree->predecessor(tree->getRoot(),8)<<'\n';
  std::cout<<"The predecessor of 15 is: "<<tree->predecessor(tree->getRoot(),15)<<'\n';
  std::cout<<"The predecessor of 12 is: "<<tree->predecessor(tree->getRoot(),12)<<'\n';
  std::cout<<"The predecessor of 6 is: "<<tree->predecessor(tree->getRoot(),6)<<'\n';
  std::cout<<"The predecessor of 99 is: "<<tree->predecessor(tree->getRoot(),99)<<'\n';
  
  /* Testing getParentValue() */
  std::cout<<'\n';
  /* root */
  std::cout<<"Parent of 10 is: "<<tree->getParentValue(tree->getRoot(),10)<<'\n';
  /* middle nodes */
  std::cout<<"Parent of 8 is: "<<tree->getParentValue(tree->getRoot(),8)<<'\n';
  /* leaf nodes */
  std::cout<<"Parent of 9 is: "<<tree->getParentValue(tree->getRoot(),9)<<'\n';
  /* something that is not in the tree */
  std::cout<<"Parent of 99 is: "<<tree->getParentValue(tree->getRoot(),99)<<'\n';

  /* Testing Delete */
  std::cout<<'\n';
  /* Delete the root */
  std::cout<<"Deleting 10: "<<'\n';
  tree->deleteNode(tree->getRoot(),10);
  tree->inOrder(tree->getRoot());
  return 0;
}