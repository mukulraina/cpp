// Program to find the max depth of tree (not necess BST) 
// maxDepth = # of nodes along the longest path from root to leaf
#include <iostream>
using namespace std;

// Node Class
class Node{
  public:
    int value;
    Node *left;
    Node *right;
    Node();
    Node(int value);
    void printInfo();
};
Node::Node(){
  left = right = NULL;
}
Node::Node(int value){
  this->value = value;
  left = right = NULL;
}
void Node::printInfo(){
  cout << "value: " << value << endl;
}


// Tree Class
class BST{
  public:
    Node *root;
    BST();
    bool isEmpty();
    void insert(int v);
    void inOrderTraversal(Node *node);
    int maxDepth(Node *node);
};
BST::BST(){
  root = NULL;
}
bool BST::isEmpty(){
  return root == NULL;
}
void BST::insert(int v){
  // Make a node
  Node *newNode = new Node(v);

  // Case 1: if tree is empty
  if(isEmpty()){
    root = newNode;
    return;
  }

  // Normal Case
  Node *current = root;
  Node *parent = NULL;
  while(current != NULL){
    parent = current;
    if (v < current->value)
      current = current->left;
    else
      current = current->right;
  }

  // Now current points to the parent
  // Decide whether to put as left or right child
  if(v < parent->value)
    parent->left = newNode;
  else
    parent->right = newNode;
}

void BST::inOrderTraversal(Node *node){
  if(node != NULL){
    inOrderTraversal(node->left);
    cout << node->value << endl;
    inOrderTraversal(node->right);
  }
}

// Time: O(n)
int BST::maxDepth(Node *node){
  if(node == NULL){
    return 0;
  }
  else{
  // Compute the depth of each subtree
  int l_h = maxDepth(node->left);
  int r_h = maxDepth(node->right);
  
  // Use the larger one
  if(l_h > r_h)
    return (l_h + 1);
  else
    return (r_h + 1);
  }
}

// Test Client
int main(){
  BST *tree = new BST();
  BST *tree2 = new BST();
  tree2->insert(3);
  tree2->insert(2);
  tree2->insert(1);
  tree2->insert(4);
  tree2->insert(5);

  tree->insert(8);
  tree->insert(3);
  tree->insert(10);
  tree->insert(1);
  tree->insert(6);
  tree->insert(14);
  tree->insert(4);
  tree->insert(7);
  tree->insert(13);
  
  tree2->inOrderTraversal(tree2->root);
  //cout << tree->maxDepth(tree->root) << endl;
  cout << tree2->maxDepth(tree2->root) << endl;
  
  return 0;
}
