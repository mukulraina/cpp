// Find vertical sum of the nodes that are in same vertical line.
#include <iostream>
#include <unordered_map>
using namespace std;
typedef std::unordered_map<int,int> Mymap;

// Node Class
class Node{
  public:
    int value;
    Node *left;
    Node *right;
    Node();
    Node(int value);
    void printInfo();
};
Node::Node(){
  left = right = NULL;
}
Node::Node(int value){
  this->value = value;
  left = right = NULL;
}
void Node::printInfo(){
  cout << "value: " << value << endl;
}


// Tree Class
class BST{
  public:
    Node *root;
    BST();
    bool isEmpty();
    void insert(int v);
    void inOrderTraversal(Node *node);
    void verticalSum(Node *node, int level, int count[]);
    void printVerticalSum(Node *node, int count[]);
};
BST::BST(){
  root = NULL;
}
bool BST::isEmpty(){
  return root == NULL;
}
void BST::insert(int v){
  // Make a node
  Node *newNode = new Node(v);

  // Case 1: if tree is empty
  if(isEmpty()){
    root = newNode;
    return;
  }

  // Normal Case
  Node *current = root;
  Node *parent = NULL;
  while(current != NULL){
    parent = current;
    if (v < current->value)
      current = current->left;
    else
      current = current->right;
  }

  // Now current points to the parent
  // Decide whether to put as left or right child
  if(v < parent->value)
    parent->left = newNode;
  else
    parent->right = newNode;
}

void BST::inOrderTraversal(Node *node){
  if(node != NULL){
    inOrderTraversal(node->left);
    cout << node->value << endl;
    inOrderTraversal(node->right);
  }
}

void BST::verticalSum(Node *node, int level, int count[]){
  if(node != NULL){
    verticalSum(node->left,level - 1, count);
    count[level] += node->value;
    verticalSum(node->right, level + 1, count);
  }
  return;
}

void BST::printVerticalSum(Node *node, int count[]){
  int i = 0;
  Node *temp = node;
  while(temp->left != NULL){
    i--;
    temp = temp->left;
  }
  verticalSum(node, i * (-1), count);

  for(int i = 0; count[i]; i++)
    cout << "The sum of level " << i + 1 << " is " << count[i] << endl;  
}

// Test Client
int main(){
  BST *tree = new BST();
  int count[100] = {0};

  tree->insert(6);
  tree->insert(3);
  tree->insert(7);
  tree->insert(2);
  tree->insert(5);
  tree->insert(9);
  
  tree->inOrderTraversal(tree->root);
  
  cout << endl;
  tree->printVerticalSum(tree->root,count);
  return 0;
}
