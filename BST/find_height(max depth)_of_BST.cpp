// Find the max depth of the BST
// Referrence: http://www.geeksforgeeks.org/how-to-determine-if-a-binary-tree-is-balanced/
#include <iostream>
#include <cmath>
using namespace std;

// Node
class Node{
  public:
    int value;
    Node *left;
    Node *right;
    Node();
    Node(int v);
};
Node::Node(){
  left = right = NULL;
}
Node::Node(int v){
  value = v;
  left = right = NULL;
}

// Tree
class BST{
  public:
    Node *root;
    BST();
    bool isEmpty();
    void insert(int v);
    void printInOrder(Node *node);
    int getMaxDepth(Node *node);
};
BST::BST(){
  root = NULL;
}
bool BST::isEmpty(){
  return root == NULL;
}
void BST::insert(int v){
  Node *newNode = new Node(v);
  
  // Case 1: If empty tree
  if(isEmpty()){
    root = newNode;
    return;
  }

  // Case 2: Find parent (by traversing the tree)
  Node *parent = NULL;
  Node *current = root;
  while(current != NULL){
    parent = current;
    if(v < current->value)
      current = current->left;
    else
      current = current->right;
  }

  // Now parent points to the parent 
  if(v < parent->value)
    parent->left = newNode;
  else
    parent->right = newNode;
}

void BST::printInOrder(Node *node){
  if(node != NULL){
    printInOrder(node->left);
    cout << node->value << '\t';
    printInOrder(node->right);
  }
}

int max(int a, int b){
  return ((a > b) ? a : b);
}

/*
  So you get the height of both left and right subtree and
  then you take the bigger one (as we are trying to find out the max depth)
  BUT then you also + 1 so that after the recursive calls comes back, the
  height has been increased by 1 point (which how it should be)

  Time: O(n)
*/
int BST::getMaxDepth(Node *node){
  if(node == NULL)
    return 0;

  int leftMaxDepth = getMaxDepth(node->left);
  int rightMaxDepth = getMaxDepth(node->right);
  return max(leftMaxDepth,rightMaxDepth) + 1;
}

int main(){
  BST *tree = new BST();
  tree->insert(8);
  tree->insert(6);
  tree->insert(10);
  tree->insert(4);
  tree->insert(7);
  tree->insert(9);
  tree->insert(13);
  tree->insert(3);
  tree->insert(5);
  tree->insert(12);
  tree->insert(14);

  tree->printInOrder(tree->root);
  cout << endl;
  
  cout << tree->getMaxDepth(tree->root) << endl;
  return 0;
}
