#include <iostream>
using namespace std;

class Node{
public:
  int value;
  Node *left;
  Node *right;
  Node(int v);
};
Node::Node(int v){
  this->value = v;
  left = right = NULL;
}

class BST{
public:
  Node *root;
  BST();
  void insert(int v);
  int getMin(Node *n);
  void inOrderTraversal(Node *n);
  };
BST::BST(){
  root = NULL;
}

void BST::insert(int v){
  // Make a new node with value v
  Node *newNode = new Node(v);

  //Case 1: 0 node
  if(root == NULL){
    root = newNode;
    return;
  }

  //Case 2: 1 or more nodes
  Node *p = NULL;
  Node *c = root;
  
  while(c != NULL){
    p = c;
    if(v < c->value)
      c = c->left;
    else
      c = c->right;
  }
  // now p points to the parent where node needs to be inserted
  if(v < p->value)
    p->left = newNode;
  else
    p->right = newNode;
}

void BST::inOrderTraversal(Node *n){
  if(n != NULL){
    inOrderTraversal(n->left);
    cout << n->value << '\t';
    inOrderTraversal(n->right);
  }
}

int BST::getMin(Node *n){
  if(n == NULL)
    return -1;
  else if(n->left == NULL)
    return n->value;
  else
    return getMin(n->left);
}

int main(){
  int a[] = {10,8,14,6,9,12,15};
  BST *tree = new BST();

  for (int i = 0; i < 7; i++){
    tree->insert(a[i]);
  }

  tree->inOrderTraversal(tree->root);
  cout << endl;

  cout << tree->getMin(tree->root) << endl;
  return 0;
}
