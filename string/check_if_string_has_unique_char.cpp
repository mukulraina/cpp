#include <iostream>
#include <unordered_map>
using namespace std;
typedef unordered_map<char,int> Mymap;


bool isUnique(string s){
  if(s.size() == 0 || s.size() > 256){
    cout << "empty string" << endl;
    return false;
  }

  if(s.size() == 1)
    return true;

  if(s.size() == 2){
    if(s[0] != s[1])
      return true;
    return false;
  }
    
  // Traverse the string and put char in hash
  // make hash
  Mymap m;
  Mymap::iterator iter;

  for(int i = 0; i < s.size(); i++){
    iter = m.find(s[i]);
    if(iter == m.end())
      m.insert(Mymap::value_type(s[i],1));
    else
      iter->second++;
  }

  // Traverse again and check if theres a char with value > 1
  for(int i = 0; i < s.size(); i++){
    iter = m.find(s[i]);
    if(iter->second > 1)
      return false;
  }
  return true;
}

// Test Client
int main(){
  // Normal Case
    // no space 
    std::cout<<isUnique("abcdef")<<'\n';
    // 1 space
    std::cout<<isUnique("abcdef ghijklmnop")<<'\n';

  // Extreme Case
    // 0
    std::cout<<isUnique("")<<'\n';
    // 1
    std::cout<<isUnique("a")<<'\n';
    // large 
    std::cout<<isUnique("abcdefghabcdefghabcdefghabcdefghabcdefghabcdefghabcdefghabcdefghabcdefghabcdefgh")<<'\n';

  // Null / illegal 
    // whats not possible in problem

  // Strange input
    // Already sorted
    // Reverse sorted
  return 0;
}
