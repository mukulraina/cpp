// Given two strings, write a method to decide if one is a permutation of the other.

/*
Approach 1: sort both strings, that would be O(n log n) and then compare both of them
Approach 2: check if all the chars in the string appear equal # of times
  Approach 2a: use hash key: char value: # of occurences
  Approach 2b: use asciiSet: index: char value of the index: # of occurences
*/
#include <iostream>
using namespace std;

bool checkAnagram(string s1, string s2){
  // Case 1: if any of strings are null 
  if(s1.size() == 0 || s2.size() == 0)
    return false;

  // Case 2: if they are not of the same size
  else if(s1.size() != s2.size())
    return false;

  // Case 3: Normal Case: 
  else{
    // Fill the asciiSet for the first string
    int asciiSetS1[256] = {0};
    for(int i = 0; i < s1.size(); i++){
      int v = s1[i];
      asciiSetS1[v]++;
    }

    // Fill the asciiSet for the second string
    int asciiSetSetS2[256] = {0};
    for(int i = 0; i < s2.size(); i++){
      int v = s2[i];
      asciiSetSetS2[v]++;
    }

    // Compare the 2 asciiSet arrays, if they are equal retrun true
    for(int i = 0; i < 256; i++){
      if(asciiSetS1[i] != asciiSetSetS2[i])
        return false;
    }
    return true; // That means all chars had same # of occurences
  }
}

// Test Client
int main(){
  // Normal Case
    std::string s1 = "apple";
    std::string s2 = "mukul";
    std::cout<<checkAnagram(s1,s2)<<'\n';

    std::string s3 = "madam god";
    std::string s4 = "dog madam";
    std::cout<<checkAnagram(s3,s4)<<'\n';
  
  // Extreme Case
    std::string s5 = "";
    std::string s6 = "dog madam";
    std::cout<<checkAnagram(s5,s6)<<'\n';

    std::string s7 = "dog madam";
    std::string s8 = "";
    std::cout<<checkAnagram(s7,s8)<<'\n';
    
    // large

  // Null / illegal 

  // Strange input

  return 0;
}