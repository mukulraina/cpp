#include <iostream>
using namespace std;

void replace_spaces(char str[], int length, int space_count){
  
  // length of the modified string
  int new_length = length + space_count * 2;

  // Make the last character of the new string to hold a null char
  str[new_length] = '\0';

  for(int i = length - 1; i >= 0; i--){
    if(str[i] == ' '){
      str[new_length - 1] = '0';
      str[new_length - 2] = '2';
      str[new_length - 3] = '%';
      new_length -= 3;
    }
    else{
      str[new_length - 1] = str[i];
      new_length--;
    }
  }
}

int main(){
  string s = "hello world etc etc";
  int space_count = 0;

  // Count the number of spaces in the string
  for(int i = 0; i < s.size(); i++){
    if(s[i] == ' ')
      space_count++;
  }

  // Make a new string with extra space 
  char *new_string = new char[s.size() + space_count * 2 + 1];

  // Copy the contents of the original string to the new one 
  for(int i = 0; i < s.size(); i++){
    new_string[i] = s[i];
  }

  cout << "the original string is: " << s << endl;

  replace_spaces(new_string,s.size(),space_count);
  cout << "The updated string is: " << new_string << endl;
  return 0;
}
