//Program to reverse a string
#include <iostream>
using namespace std;

void reverse(string &s){
  if(s.size() == 0)
    return;

  int j = s.size() - 1;
  for(int i = 0; i < s.size() / 2; i++){
    swap(s[i],s[j]);
    j--;
  }
}

int main(){
  // Case 1: Normal
  string s = "mukul";
  reverse(s);
  cout << s << endl;

  // Case 2: Extreme
  s = "m";
  reverse(s);
  cout << s << endl;
  
  // Case 3: null/illegal
  s = "";
  reverse(s);
  cout << s << endl;

  // Case 4: strange input
  s = "madam";
  reverse(s);
  cout << s << endl;
}
