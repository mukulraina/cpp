#include <iostream>
#include <unordered_map>
using namespace std;
typedef unordered_map<char,int> Mymap;

bool isUniqueUsingHash(string s){
  // Case 1: empty string
  if(s.size() == 0){
    cout << "sorry empty string: ";
    return false;
  }

  // Case 2: 1 character
  else if(s.size() == 1)
    return true;

  // Case 3: More than 1 character
  else{
    // Insert the characters in the hash
    // If you find someone already in the hash return false,
    // If not then return true
    Mymap hash;
    Mymap::iterator iter;
    for(int i = 0; i < s.size(); i++){
      iter = hash.find(s[i]);
      if(iter != hash.end())
        return false;
      else
        hash.insert(Mymap::value_type(s[i],1));
    }
    // If the program came till here that means no character
    // was already found in the hash, so all char are unique
    return true;
  }
}


//Test Client
void testHashTableApproach(){
    std::cout<<isUniqueUsingHash("abcdef")<<'\n';
    std::cout<<isUniqueUsingHash("abcdef ghijklmnop")<<'\n';
    std::cout<<isUniqueUsingHash("")<<'\n';
    std::cout<<isUniqueUsingHash("a")<<'\n';
    std::cout<<isUniqueUsingHash("abcdefghabcdefghabcdefgefgh")<<'\n';
    std::cout<<isUniqueUsingHash("mukul raina")<<'\n';
}

int main(){
  testHashTableApproach(); 
  return 0;
}
