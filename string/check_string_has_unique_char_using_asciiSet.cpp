#include <iostream>
using namespace std;

bool isUniqueUsingAsciiSet(string s){
  // Case 1: empty string
  if(s.size() == 0){
    cout << "empty string: ";
    return false;
  }
  
  // Case 2: 1 char
  else if(s.size() == 1)
    return true;
  
  // Case 3: More than 256 char
  else if(s.size() > 256)
    return false;

  // Case 4: Normal Case
  else{
    bool asciiSet[256] = {false};
    // Traverse throught the string + 
    // If you find any char that is already true +
    // That means it has already appeared once + 
    // So return false
    for(int i = 0; i < s.size(); i++){
      int v = s[i];
      // if already found true, that means its already there
      // not unique, return false
      if(asciiSet[v] == true)
        return false;
      // That means this is the first time its appearing + 
      // so change its value to true in the bool array
      else
        asciiSet[v] = true;
    }
    // If the loop came till here that means + 
    // A recurring character was never found 
    return true;
  }
}


//Test Client
void testAsciiSetApproach(){
    std::cout<<isUniqueUsingAsciiSet("abcdef")<<'\n';
    std::cout<<isUniqueUsingAsciiSet("abcdef ghijklmnop")<<'\n';
    std::cout<<isUniqueUsingAsciiSet("")<<'\n';
    std::cout<<isUniqueUsingAsciiSet("a")<<'\n';
    std::cout<<isUniqueUsingAsciiSet("abcdefghabcdefghabcdefgefgh")<<'\n';
    std::cout<<isUniqueUsingAsciiSet("mukul raina")<<'\n';
}

int main(){
  testAsciiSetApproach(); 
  return 0;
}
