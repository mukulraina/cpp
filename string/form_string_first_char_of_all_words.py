'''
Given a string. Write a program to form a string with first character of all words.
'''

def formString(s):
  # Get all the words in the string + put them in a list
  list = s.split()

  # For every word get the first character + store into a list
  reqdList = []
  for word in list:
    reqdList.append(word[0])

  # Join the list to form the string 
  return ''.join(reqdList)

def main():
  s = "msdasda uasdasdsdas kasdasda uasdasd lasdasda"
  print formString(s)

if __name__ == "__main__":
  main()
