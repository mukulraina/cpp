#include <iostream>
using namespace std;

void runLengthEncoded(string a){
  int sameCount = 1;
  for(int i = 0; i < a.size(); i++){
    if(a[i] != a[i + 1]){
      cout << a[i] << sameCount;
      sameCount = 1;
    }
    else
      sameCount++;
  }
}

int main(){
  string s = "wwwaaawwwdexxxeeexxd";
  runLengthEncoded(s);
  cout << endl;
  return 0;
}
