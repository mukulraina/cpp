'''
Reverse the order of words in a string
"i like this program very much" -> "much very program this like i"

reference: http://stackoverflow.com/questions/1546226/the-shortest-way-to-remove-multiple-spaces-in-a-string-in-python
'''

def reverseOrder(s):

  # Get the words into a list
  list = s.split()

  # Put the words into a stack
  stack = []
  for word in list:
    stack.append(word)

  # Keep poping elements + put into a different list
  # to get the reverse order
  reverseList = []
  while stack:
    reverseList.append(stack.pop())

  # Join the reverseList list into a string
  return " ".join(reverseList)

def main():
  s = "i   like  this      program   very   much"
  print reverseOrder(s);


if __name__ == "__main__":
  main()
  