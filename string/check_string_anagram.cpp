#include <iostream>
using namespace std;

bool checkAnagram(string s1, string s2){
  if(s1.size() != s2.size())
    return false;

  int asciiSet1[256] = {0};
  for(int i = 0; i < s1.size(); i++){
    int v = s1[i];
    asciiSet1[v]++;
  }

  int asciiSet2[256] = {0};
  for(int i = 0; i < s2.size(); i++){
    int v = s2[i];
    asciiSet2[v]++;
  }

  // compare 2 asciiSet arrays
  for(int i = 0; i < 256; i++){
    if(asciiSet1[i] != asciiSet2[i])
      return false;
  }
  return true;
}


// Test Client
int main(){
  // Normal Case
    std::string s1 = "applasdasde";
    std::string s2 = "papel";
    std::cout<<checkAnagram(s1,s2)<<'\n';

    std::string s3 = "madam god";
    std::string s4 = "dog madam";
    std::cout<<checkAnagram(s3,s4)<<'\n';
  
  // Extreme Case
    std::string s5 = "";
    std::string s6 = "dog madam";
    std::cout<<checkAnagram(s5,s6)<<'\n';

    std::string s7 = "dog madam";
    std::string s8 = "";
    std::cout<<checkAnagram(s7,s8)<<'\n';
    
    // large

  // Null / illegal 

  // Strange input

  return 0;
}