//
//  stringReverse.h
//  Interviews
//
//  Created by Mukul Raina on 2014-06-24.
//  Copyright (c) 2014 Mukul Raina. All rights reserved.
//

#ifndef Interviews_stringReverse_h
#define Interviews_stringReverse_h

class stringReverse{
public:
    void reverse(char* str);
};

#endif