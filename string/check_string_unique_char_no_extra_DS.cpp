#include <iostream>
using namespace std;

bool isUnique(string s){
  if(s.size() == 0 || s.size() > 256)
    return false;

  if(s.size() == 1)
    return true;

  if(s.size() == 2){
    if(s[0] != s[1])
      return true;
    else 
      return false;
  }

  bool asciiSet[256] = {false};

  // Traverse once only
  for(int i = 0; i < s.size(); i++){
    int v = s[i];
    if(asciiSet[v] == true)
      return false;
    else
      asciiSet[v] = true;
  }
  return true;
}

// Test Client
int main(){
  std::cout<<isUnique("mukul")<<'\n';
  std::cout<<isUnique("abcdef")<<'\n';
  return 0;
}
