#include <iostream>
using namespace std;

void reverseString(char *s){
  // Get *last to point to the last char in string
  char *last = s;
  while(*last){
    last++;
  }
  last--;

  while(s < last){
    char c = *s;
    *s = *last;
    *last = c;
    s++;
    last--;
  }
}

int main(){
  char a[] = {"mu"};
  cout << a << '\t' << endl;
  reverseString(a);
  cout << a << '\t' << endl;
  return 0;
}
