// Program to find the index of first repeating character in a string
#include <iostream>
using namespace std;

int getIndex(string s){
  // Case 1: empty string
  if(s.size() == 0)
    return -1;

  // Traverse through the string + insert in asciiSet
  int asciiSet[256] = {0};
  for(int i = 0; i < s.size(); i++){
    int v = s[i];
    asciiSet[v]++;
  }

  // Traverse again and find the first char with value >=2
  for(int i = 0; i < s.size(); i++){
    int v = s[i];
    if(asciiSet[v] >= 2)
      return i;
  }
  cout << "unique character" << endl;
  return -1;
}

int main(){
  string s = "mukul";
  cout << getIndex(s) << endl;
  return 0;
}
