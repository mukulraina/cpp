//
//  stringReverse.cpp
//  Interviews
//
//  Created by Mukul Raina on 2014-06-24.
//  Copyright (c) 2014 Mukul Raina. All rights reserved.
//

#include <iostream>
#include "stringReverse.h"

using std::cout;
using std::endl;

void stringReverse::reverse(char *str){
    char *p = str;
    char temp;
    if (str) {
        while (*p) {
            p++;
        }
        p--;
        
        while (str < p) {
            temp = *str;
            *str++ = *p;
            *p-- = temp;
        }
    }
}
