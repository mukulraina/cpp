// Check for balanced parentheses in an expression
#include <iostream>
#include <stack>
using namespace std;

bool isOpenExp(char c){
  return (c == '[' || c == '(' || c == '{');
}

bool isMatching(char closeExp, char openExp){
  if(closeExp == ']' && openExp == '[')
    return 1;
  else if(closeExp == '}' && openExp == '{')
    return 1;
  else if(closeExp == ')' && openExp == '(')
    return 1;
  else 
    return 0; 
}

bool isBalanced(string s){
  stack<char> charStack;

  // Traverse throught the expression
  for(int i = 0; i < s.size(); i++){
    
    // if is an opening exp - push it
    if(isOpenExp(s[i]))
      charStack.push(s[i]);
    else{
     if(isMatching(s[i],charStack.top()))
        charStack.pop();
    }
  }

  // If stack is empty that means its balanced 
  return charStack.empty();
}

int main(){
  string s = "{()}[]";
  cout << isBalanced(s) << endl;
  return 0;
}
