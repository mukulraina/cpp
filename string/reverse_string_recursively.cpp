// Program to reverse a string recursively
#include <iostream>
#include <string>
using namespace std;

void reverse(char *s, int len){
  if(len > 1){
    char temp = s[0];
    s[0] = s[len - 1];
    s[len - 1] = temp;
    reverse(s+1,len-2);
  }
}

int main(){
  char s1[] = "mukul";
  char s2[] = "m";
  char s3[] = "";

  reverse(s1,5);
  reverse(s2,1);
  reverse(s3,0);

  cout << s1 << endl;
  cout << s2 << endl;
  cout << s3 << endl;
  
  return 0;
}
