// given a string return character whose count is 1 and position is right most
//example :- aabccddefff so b and e count 1 :- so return e.
#include <iostream>
#include <string>
#include <unordered_map>
#include <string>
typedef std::unordered_map<char,int> Mymap;
using std::cout;
using std::string;
using std::endl;

// Hash Approach
char getCharHashApproach(string s){
  if(s.size() == 0)
    return 'x';

  if(s.size() == 1)
    return s[0];

  // Declare hash + iterator
  Mymap m;
  Mymap::iterator iter;

  // Insert into hash
  for (int i = 0; i < s.size(); i++){
    iter = m.find(s[i]);
    // not in hash
    if(iter == m.end())
      m.insert(Mymap::value_type(s[i],1));
    else
      iter->second++;
  }

  // Traverse from the end of the string 
  // And return the first character you find whose "value" in hash is 1
  for (int i = s.size()-1; i >= 0; i--){
    iter = m.find(s[i]);
    if(iter->second == 1)
      return iter->first;
  }
}

// bool asciiSet[] array approach
char getCharBoolAsciiSetApproach(string s){
  if(s.size() == 0)
    return 'x';

  if(s.size() == 1)
    return s[0];

  // 256 element array that could hold all the possible characters
  int asciiSet[256] = {0};

  // Populate the bool array
  for (int i = 0; i<s.size(); i++){
    int v = s[i];
    asciiSet[v]++;
  }

  // Traverse from the end of the string + and return the required char
  for (int i = s.size() - 1; i >= 0; i--){
    int v = s[i];
    if(asciiSet[v] == 1)
      return s[i];
  }
}
int main(){
  string s = "aabccddefff";
  cout << getCharHashApproach(s) << endl;
  cout << getCharBoolAsciiSetApproach(s) << endl;
  return 0;
}
