// Remove duplicated from a string in O(n) without using hash.
#include <iostream>
#include <string>
using std::string;
using std::cout;
using std::endl;

void removeDuplicates(string &s){
  if(s.size() == 0 || s.size() == 1){
    return; 
  }

  bool asciiSet[256] = {false};
  string sCopy;
  for (int i = 0; i < s.size(); i++){
    int v = s[i];
    if(!asciiSet[v]){
      asciiSet[v] = 1;
      sCopy += s[i];
    }
  }
  s = sCopy;
}

// Test Client
int main(){
  string s = "mukul";
  removeDuplicates(s);
  cout<<s<<endl;

  string s1 = "aaabbacbccd";
  removeDuplicates(s1);
  cout<<s1<<endl;

  return 0;
}