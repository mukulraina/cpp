// Program to check if a string is palindrome or not
#include <iostream>
#include <stack>
#include <string>
using std::cout;
using std::endl;
using std::stack;
using std::string;

/*
  Checks if a string is palindrome or not using Stacks
  Algorithm:
    1) Iterate to the half of the string and keep pushing the values on stack
    2) As soon as you go above stack, pop if the value you got currently from string
      s[i] is same as pop, if stack empty in the end its palindrome
    3) for odd: it doesnt matter whats the middle character as the first 2 values 
        will be onto the stack and as soon as it goes in else it only pop if top()
        matches the s[i] so the stack sequence [ul] should match the remaining string
        sequence [ul]....now if you think what if there was 2 character before ul 
        Then we would have had equal splits in the first place and we wouldnt have gotten
        in this case anyways
*/
bool isPalindrome(const string& s){
  if(s.size() == NULL)
    return false;
  
  stack<char> charStack;

  for (int i = 0; i < s.size(); i++){
    if(i < s.size() / 2){
      charStack.push(s[i]);
      cout<<s[i];
    }
    else{
      if(charStack.top() == s[i])
        charStack.pop();
    }
  }

  if (charStack.empty())
    return true;
  else
    return false;
}

int main(){
  string s1 = "abcdef";
  string s2 = "maddam";
  string s3 = "mukul";
  string s4 = "mukkal";
  string s5 = "madam";

  //cout<< s1 << " : " << isPalindrome(s1) << endl;
  //cout<< s2 << " : " << isPalindrome(s2) << endl;
  cout<< s3 << " : " << isPalindrome(s3) << endl;
  //cout<< s4 << " : " << isPalindrome(s4) << endl;
  cout<< s5 << " : " << isPalindrome(s5) << endl;

  return 0;
}
