/*
  This program implements binary minHeap in c++ and performs the following operations on it:
  1) Insert an element
  2) Delete the root element (the minimum)
  3) Extract the root element (the minimum)
  4) Print heap
*/
#include <iostream>
#include <cstdlib>
#include <vector>
#include <iterator>
using namespace std;
/*
 * NOTE: its a min-heap (not a max-heap)
 */
class BinaryHeap
{
    private:
        vector <int> heap;
        int left(int parent);
        int right(int parent);
        int parent(int child);
        void heapifyup(int index);
        void heapifydown(int index);
    public:
        BinaryHeap()
        {}
        void Insert(int element);
        void DeleteMin();
        int ExtractMin();
        void DisplayHeap();
        int Size();
};

// Return Heap size
int BinaryHeap::Size(){
  return heap.size();
}

// Insert element into heap
void BinaryHeap::Insert(int element){
  // Insert the element in the end (last location)
  heap.push_back(element);
  // BubbleUp the element to put it into its appropriate place
  heapifyup(heap.size() - 1);
}

// Delete minimum element
void BinaryHeap::DeleteMin(){
  if(heap.size() == 0){
    cout<<"Sorry Heap is empty\n";
    return;
  }
  // swap the 1st element with the last element
  heap[0] = heap.at(heap.size() - 1);
  // Remove the last element (which was the 1st element before (and the 1st element was also the minimum))
  heap.pop_back();
  // Since the last element is the root now, we need to bubble down to put it into the right place
  heapifydown(0);
}

// Extract minimum element
int BinaryHeap::ExtractMin(){
  if(heap.size() == 0){
    return -1;
  }
  else
    return heap.front();
}

// Display Heap
void BinaryHeap::DisplayHeap(){
  vector<int>::iterator pos = heap.begin();
  cout<<"Heap-> ";
  while (pos != heap.end()){
    cout<<*pos<<" ";
    pos++;
  }
  cout<<endl;
}

// Return left child
// left = 2i + 1
int BinaryHeap::left(int parent){
  int l = 2 * parent + 1;
  if(l < heap.size())
    return l;
  else 
    return -1;
}

// Return right child
// right = 2i + 2
int BinaryHeap::right(int parent){
  int r = 2 * parent + 2;
  if (r < heap.size())
    return r;
  else 
    return -1;
}


// Return parent
// parent = (i-1)/2
int BinaryHeap::parent(int child){
  int p = (child - 1)/2;
  if (child == 0)
    return -1;
  else 
    return p;
}

// Swaps 2 values
void swap(int &a, int &b){
  int temp = a;
  a = b;
  b = temp;
}

/*
 * Heapify- Maintain Heap Structure bottom up
 * So it does stuff to make sure the root element is the minimum in the end
 */
void BinaryHeap::heapifyup(int index){
  if(index >= 0 && parent(index) >= 0 && heap[parent(index)] > heap[index]){
    swap(heap[parent(index)],heap[index]);
    heapifyup(parent(index)); // this time you are passing the parent of heap[index] not the original index - thats bubbling up
  }
}

/*
 * Heapify- Maintain Heap Structure top down
 */
void BinaryHeap::heapifydown(int index){
  int child = left(index);
  int child1 = right(index);
  if (child >= 0 && child1 >= 0 && heap[child] > heap[child1]){
    child = child1;
  }
  if(child > 0){
    swap(heap[index],heap[child]);
    heapifydown(child);
  }
}

/*
 * Main Contains Menu
 */
int main()
{
    BinaryHeap h;
    while (1)
    {
        cout<<"------------------"<<endl;
        cout<<"Operations on Heap"<<endl;
        cout<<"------------------"<<endl;
        cout<<"1.Insert Element"<<endl;
        cout<<"2.Delete Minimum Element"<<endl;
        cout<<"3.Extract Minimum Element"<<endl;
        cout<<"4.Print Heap"<<endl;
        cout<<"5.Exit"<<endl;
        int choice, element;
        cout<<"Enter your choice: ";
        cin>>choice;
        switch(choice)
        {
        case 1:
            cout<<"Enter the element to be inserted: ";
            cin>>element;
            h.Insert(element);
            break;
        case 2:
            h.DeleteMin();
            break;
        case 3:
            cout<<"Minimum Element: ";
            if (h.ExtractMin() == -1)
            {
                cout<<"Heap is Empty"<<endl;
            }
            else
                cout<<"Minimum Element:  "<<h.ExtractMin()<<endl;
            break;
        case 4:
            cout<<"Displaying elements of Hwap:  ";
            h.DisplayHeap();
            break;
        case 5:
            exit(1);
        default:
            cout<<"Enter Correct Choice"<<endl;
        }
    }
    return 0;
}
