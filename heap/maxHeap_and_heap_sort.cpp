// This progam implements a maxHeap and heap sort algorithm
#include <iostream>
using namespace std;

// Bubble up stuff
void max_heapify(int *a, int i, int n){
    int j, temp;
    temp = a[i];
    j = 2*i;
    while (j <= n)
    {
        if (j < n && a[j+1] > a[j])
            j = j+1;
        if (temp > a[j])
            break;
        else if (temp <= a[j])
        {
            a[j/2] = a[j];
            j = 2*j;
        }
    }
    a[j/2] = temp;
    return;
}

// Heap sort implementation 
void heapsort(int *a, int n)
{
    int i, temp;
    for (i = n; i >= 2; i--)
    {
        temp = a[i];
        a[i] = a[1];
        a[1] = temp;
        max_heapify(a, 1, i - 1);
    }
}

// Building max heap
void build_maxheap(int *a, int n)
{
    int i;
    for(i = n/2; i >= 1; i--)
    {
        max_heapify(a, i, n);
    }
}

// Test client
int main()
{
    int n, i, x;
    int a[] = {50,35,40,12,7,10,15,4,1,3}; 
    build_maxheap(a,10);
    heapsort(a, 10);
    cout<<"sorted output\n";
    for (i = 1; i <= 10; i++){
        cout<<a[i]<<endl;
    }
    return 0;
}
