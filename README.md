cpp
===

**Algorithms**
  - smallest range that includes atleast 1 number from each of k lists

**Arrays**
  - check array sorted
  - Sum of list of numbers
  - 2nd max in array
  - 3 way partitioning 
  - binary search
  - equilibrium index
  - leaders
  - majority element
  - reverse elements
  - stack using array
  - happy numbers
  - check if 2 #s have same digits
  - find # of 0s in sorted array of 0s and 1s
  - given a number x find a pair whose sum is x
  - max subarray problem
  - overlapping intervals
  - min distance between 2 numbers in array
  - move even numbers to left and odd numbers to right
  - number first increase then decrease
  - # of occurences of each element
  - odd numbers from 1to 99
  - nth fibnocci 
  - pass vector to function
  - remove duplicates in array
  - pythagorean triplets
  - reverse digits of a number
  - search for element in array recursively
  - sum of all multiples
  - sum of array
  - prime number (sieve of erastothenes)

**BST**
  - Basic BST (operations like insert, delete, printInorder)
  - BFS
  - Calculate size of the tree
  - Count # of leaf nodes
  - Max depth
  - Level of a node
  - Vertical sum
  - Check if a binary tree is balanced
  - Delete a binary tree
  - Find max sum from left to root
  - get maximum in a binary tree
  - get minimum in a binary tree
  - get parent 
  - find the predecessor of a node
  - Print all nodes that are k distance from the root
  - print all nodes of the same level
  - search (recursive + iterative)
  - successor
  - sum of all elements

**Concepts**
  - Class templates
  - Funtion templates
  - Function templates with multiple parameters

**Recursion**
  - palindrome
  - factorial
  - fibnocci 
  - fibnocci (iterative)
  - find max in array using recrusion
  - reverse string
  - find min in array using recursion
  - sum of digits
  - find sum of array using recursion

**Sorting**
  - merge sort
  - quick sort
  - counting sort

**Tutorials**
  - 2D arrays
  - class templates
  - copy constructor 
  - data abstraction
  - dynamic memory arrays
  - dynamic memory objects
  - dynamic memory variables
  - encapsulation
  - enum
  - exception handling
  - friend functions
  - function templates
  - inheritance
  - inline functions
  - multiple inheritance
  - namespace
  - namespace with using
  - passing array to a function
  - point to array
  - polymorphism without virtual
  - pure virtual function
  - passing array back from function
  - static functions
  - static variables
  - structures
  - this pointer
  - unsigned signed
  - virtual function

**Doubly Linked List**
  - creation
  - print from the end
  - reversal

**graphs**
  - BFS
  - DFS
  - adjacency list representation of graph
  - check if there's a path between 2 nodes

**Heap**
  - max heap
  - min heap
  - heap sort

**matrix**
  - matrix basic
  - matrix with pointers
  - transpose
  - transpose with pointers

**stack**
  - stack implementation using array
  - stack implementation using linked list

**queue**
  - queue implementation using array
  - queue implementation using linked list

**Linked List**
  - middle node iterative
  - palindrome using recursion
  - palindrome using stack
  - rotate
  - sort
  - basic (insert, delete, print)
  - reverse
  - shift even to left and odd nodes to right
  - swap every 2 nodes

**String**
  - balanced parantheses (using stack)
  - 2 string permutation
  - palindrome
  - all unique
  - anagram
  - index of first repeating char
  - run length encoding
  - reverse order of words

**Cracking the Coding Interview**
  - 1.1
  - 1.2
  - 1.3
  - 1.4
  - 1.5
  - 1.6
  - 1.7
  - 1.8
  - 2.1
  - 2.2
  - 2.3
  - 2.4
  - 2.5
  - 2.6
  - 2.7
  - 3.1
  - 3.2
  - 4.1
  - 4.2
  - 4.3
  - 4.4
  - 4.5


