// Program to implement queue using LL
#include <iostream>
using std::cout;
using std::endl;

class Node{
  private:
    int value;
    Node *next;
  public:
    Node();
    Node(int value);
    ~Node();
    int getValue();
    Node* getNext();
    void setValue(int value);
    void setNext(Node *next);
};
Node::Node(){
  next = NULL;
}
Node::Node(int value){
  this->value = value;
  next = NULL;
}
Node::~Node(){
}
int Node::getValue(){
  return value;
}
Node* Node::getNext(){
  return next;
}
void Node::setValue(int value){
  this->value = value;
}
void Node::setNext(Node *next){
  this->next = next;
}

class Queue{
  private:
    int value;
    Node *head;
    Node *tail;
  public:
    Queue();
    Queue(int value);
    bool isEmpty();
    void enqueue(int value);
    int dequeue();
    void printQueue();
};

Queue::Queue(){
  head = tail = NULL;
}

Queue::Queue(int value){
  this->value = value;
  head = tail = NULL;
}

bool Queue::isEmpty(){
  if (head == NULL && tail == NULL)
    return true;
  return false;
}

/*
  Function to enqueue a value in the Queue
  Algorithm:
    1) Case 1: No node
    2) Case 2: 1 node
    3) Case 3: General case
*/
void Queue::enqueue(int value){
  Node *newNode = new Node(value);

  // Case 1: No node
  if(isEmpty()){
    head = newNode;
    tail = newNode;
    return;
  }
  // Case 2: 1 node
  else if(head->getNext() == NULL && head == tail){
    tail = newNode;
    head->setNext(tail);
  }
  // Case 3: Normal case
  else{
    Node *temp = tail;
    temp->setNext(newNode);
    tail = newNode;
  }
}

// Prints Queue
void Queue::printQueue(){
  if(isEmpty()){
    cout << "Sorry empty Queue" << endl;
    return;
  }

  Node *temp = head;
  while(temp != NULL){
    cout << temp->getValue() << endl;
    temp = temp->getNext();
  }
}

// Function to dequeue an element from the Queue
int Queue::dequeue(){
  // Case 1: Empty Queue
  if(isEmpty()){
    cout << "Sorry empty queue" << endl;
    return -1;
  }

  // Case 2: 1 node in the queue
  else if(head == tail && head->getNext() == NULL){
    Node *temp = head;
    int value = temp->getValue();
    head = tail = NULL;
    delete temp;
    return value;
  }

  // Case 3: Normal Cas
  else{
    Node *temp = head;
    head = head->getNext();
    int value = temp->getValue();
    delete temp;
    return value;
  }
}


// Test Client
int main(){
  Queue *queue = new Queue();

  cout << "Just dequeued: " << queue->dequeue() << endl;

  queue->enqueue(10);
  queue->enqueue(20);
  queue->enqueue(30);
  cout << "Printing queue: " << endl;
  queue->printQueue();
  cout << endl;

  queue->enqueue(40);
  queue->enqueue(50);
  cout << "Printing queue: " << endl;
  queue->printQueue();
  cout << endl;

  cout << "Just dequeued: " << queue->dequeue() << endl;
  cout << "Just dequeued: " << queue->dequeue() << endl;
  cout << "Printing queue: " << endl;
  queue->printQueue();
  cout << endl;

  return 0;
}
