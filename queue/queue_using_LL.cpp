// Implement queue using LL
#include <iostream>
using namespace std;

// Node
class Node{
public:
  int value;
  Node *next;
  Node();
  Node(int v);
};
Node::Node(){
  next = NULL;
}
Node::Node(int v){
  value = v;
  next = NULL;
}

// Queue
class Queue{
public:
  Node *head;
  Node *tail;
  Queue();
  void enqueue(int v);
  int dequeue();
  void printQueue();
};
Queue::Queue(){
  head = tail = NULL;
}

// Time: O(1)
void Queue::enqueue(int v){
  // Make a node 
  Node *newNode = new Node(v);

  // Case 1: empty queue
  if(head == tail && head == NULL)
    head = tail = newNode;
  // Case 2: 1 or more nodes
  else{
    // Make the last node point to newNode
    tail->next = newNode;
    // Make tail point to the last node of the updated list
    tail = tail->next;
  }
}

// Time: O(1)
int Queue::dequeue(){
  // Case 1: 0 node
  if(head == tail && head == NULL){
    cout << "sorry empty queue, cant dequeue" << endl;
    return -1;
  }
  // Case 2: 1 or more node
  else{
    // Make a temp pointer to the 1st node
    Node *t = head;
    // Save the value
    int v = t->value;
    
    // Case 2a: Check if there 1 or more than 1 node in the list
    if(head == tail && head->next == NULL)
      head = tail = NULL;
    else
      head = head->next;

    // Delete the node
    delete t;
    // return the value
    return v;
  }
}

// Time: O(n)
void Queue::printQueue(){
  if(head == tail && head == NULL)
    return;

  Node *t = head;
  while(t != NULL){
    cout << t->value << '\t';
    t = t->next;
  }
}

// Test Client
int main(){
  Queue *queue = new Queue();

  cout << "Just dequeued: " << queue->dequeue() << endl;

  queue->enqueue(10);
  queue->enqueue(20);
  queue->enqueue(30);
  cout << "Printing queue: " << endl;
  queue->printQueue();
  cout << endl;

  queue->enqueue(40);
  queue->enqueue(50);
  cout << "Printing queue: " << endl;
  queue->printQueue();
  cout << endl;

  cout << "Just dequeued: " << queue->dequeue() << endl;
  cout << "Just dequeued: " << queue->dequeue() << endl;
  cout << "Printing queue: " << endl;
  queue->printQueue();
  cout << endl;

  return 0;
}
