// Implement queue using LL
#include <iostream>
using namespace std;

// Node
class Node{
public:
  int value;
  Node *next;
  Node();
  Node(int v);
};
Node::Node(){
  next = NULL;
}
Node::Node(int v){
  value = v;
  next = NULL;
}

class Queue{
  public:
    Node *head;
    Node *tail;
    Queue();
    bool isEmpty();
    void enqueue(int v);
    int dequeue();
    void printQueue();
};

Queue::Queue(){
  head = tail = NULL;
}

bool Queue::isEmpty(){
  if(head == NULL && tail == NULL)
    return true;
  return false;
}

// Time: O(1)
void Queue::enqueue(int v){
  Node *newNode = new Node(v);

  // Case 1: Empty Queue
  if(isEmpty()){
    head = tail = newNode;
    return;
  }

  // Case 2: 1 node in queue
  if(head == tail && head->next == NULL){
    head->next = newNode;
    tail = tail->next;
    return;
  }

  // Case 3: More than 1 node
  tail->next = newNode;
  tail = tail->next;
}

// Time: O(1)
int Queue::dequeue(){
  // Case 1: empty queue
  if(isEmpty())
    return -1;

  // Case 2: 1 or more than 1 node in the queue
  int v = head->value;
  Node *temp = head;
  
  if(head == tail && head->next == NULL)
    head = tail = NULL; // 1 node in the list
  else 
    head = head->next; // More than 1 node in the list

  delete temp;
  return v;
}

// Time: O(n)
void Queue::printQueue(){
  if(isEmpty())
    return;

  Node *t = head;
  while(t != NULL){
    cout << t->value << '\t';
    t = t->next;
  }
}

// Test Client
int main(){
  Queue *queue = new Queue();

  cout << "Just dequeued: " << queue->dequeue() << endl;

  queue->enqueue(10);
  queue->enqueue(20);
  queue->enqueue(30);
  cout << "Printing queue: " << endl;
  queue->printQueue();
  cout << endl;

  queue->enqueue(40);
  queue->enqueue(50);
  cout << "Printing queue: " << endl;
  queue->printQueue();
  cout << endl;

  cout << "Just dequeued: " << queue->dequeue() << endl;
  cout << "Just dequeued: " << queue->dequeue() << endl;
  cout << "Printing queue: " << endl;
  queue->printQueue();
  cout << endl;

  return 0;
}