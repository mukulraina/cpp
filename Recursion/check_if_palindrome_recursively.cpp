//Program to check if a string is palindrome or not recursively
#include <iostream>
using namespace std;

bool isPalindrome(char *s, int len){
  if(len <= 1)
    return true;

  if(s[0] != s[len - 1])
    return false;
  return isPalindrome(s+1,len - 2);
}

int main(){
  char s1[] = "mukul";
  char s2[] = "madam";
  cout << isPalindrome(s1,5) << endl;
  cout << isPalindrome(s2,5) << endl;
}
