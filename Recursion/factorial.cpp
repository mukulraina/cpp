/* This program shows how to calculate factorial of a number using recursion */
#include <iostream>
using std::cout;
using std::endl;
using std::cin;

/* 
 * Returns the factorial of a number 
 * @param n Number for which factorial needs to be found
 * @return  Factorial of the number
 */
int factorial(int n){
  /* Base case */
  if (n == 0)
    return 1;
  else 
    return n * factorial(n-1);
}

/* Test Client */
int main(){
  int n;
  cout<<"Enter the number for which you wanna find the factorial\n";
  cin>>n;
  cout<<"Factorial of "<<n<<" is: "<<factorial(n)<<endl;
  return 0;
}
