// Program to find the smallest element in an array using recursion
#include <iostream>
using namespace std;

int getMinHelper(int *p, int &min, int size){
  if(size < 1)
    return min;

  if(p[0] < min)
    min = p[0];
  return getMinHelper(p + 1,min,size-1);
}

int getMin(int *p, int size){
  int min = p[0];
  return getMinHelper(p,min,size);
}

int main(){
  int a[] = {32,13,45,76,43,11,56,89};
  cout << getMin(a,8) << endl;

  int b[] = {1};
  cout << getMin(b,1) << endl;

  int c[] = {1,13,45,76,43,11,56,89};
  cout << getMin(c,8) << endl;

  int d[] = {32,13,45,76,43,11,56,1};
  cout << getMin(d,8) << endl;

  return 0;
}
