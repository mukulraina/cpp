// Program to calculate the sum of the digits of a number recursively
#include <iostream>
using namespace std;

int getSumHelper(int n, int &sum){
  if(n < 1)
    return sum;

  int digit = n % 10;
  sum += digit;
  n = n / 10;
  return getSumHelper(n,sum);
}

int getSum(int n){
  int sum = 0;
  return getSumHelper(n,sum);
}

int main(){
  cout << getSum(1234) << endl;
  cout << getSum(1) << endl;
}
