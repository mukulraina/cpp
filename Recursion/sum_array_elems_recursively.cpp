// This program calculates the sum of the array elements recursively
#include <iostream>
using std::cout;
using std::endl;

// Returns the sum of the array elements
int sumOfList(int *p, int size){
  if(size == 1)
    return *p; // or p[0] - both works
  return *p + sumOfList(p+1,size-1);
}

// Test Client
int main(){
  int a[] = {5,5,5,5,5,5,5,5,5,5};
  cout<<"Sum of array elements: "<<sumOfList(a,10)<<endl;
}
