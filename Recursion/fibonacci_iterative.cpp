/*
 * This program implements an iterative solution for fibonacci sequence
 */
#include <iostream>
using std::cin;
using std::cout;
using std::endl;

/*
 * Calculates fibonacci sequence iteratively 
 * @param n Number of elements in the sequence
 * @return  Returns the squence 
 */
int fib(int n){
  if (n <= 1)
    return n;
  else{
    int f0 = 0;
    int f1 = 1;
    int f;
    for (int i = 2; i <= n; i++){
      f = f0 + f1;
      f0 = f1;
      f1 = f;
    }
    return f;
  }
}

/* Test Client */
int main(){
  int n;
  cout<<"Give me an n"<<endl;
  cin>>n;
  cout<<fib(n)<<endl;
}
