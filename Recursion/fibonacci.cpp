/* 
 * This program shows why recursion is not always good
 * In this program we calculate fib(3) and f(2) multiple times 
 */
#include <iostream>
using std::cin;
using std::cout;
using std::endl;

/*
 * Calcultes the fibonacci sequence
 * @param n Number of whom the fibonacci sequences needs to be found
 */
int fibonacci(int n){
  if (n <= 1)
    return n;
  else
    return fibonacci(n-1) + fibonacci(n-2);
}

/* Test Client */
int main(){
  int n;
  cout<<"How many elements do you want in the finbonacci sequence"<<endl;
  cin>>n;
  cout<<"Here's the sequence: "<<fibonacci(n)<<endl;
}
