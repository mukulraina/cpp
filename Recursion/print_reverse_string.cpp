/* 
 * This program prints reverse of a string
 * reference: http://www.geeksforgeeks.org/reverse-a-string-using-recursion/
 * 
 * Pointer to Char array: (e.g. char a[] = "mukul")
 * 1) *str    -> M
 * 2) str     -> Mukul
 * 3) str + 1 -> ukul
 * 3) str[0]  -> M
 *
 * Pointer to normal array (e.g int a[] = {1,2,3})
 * 1) *p    -> 1
 * 2) p     -> [address of a[0]]
 * 3) p[0]  -> Value of a[0]
 */
#include <iostream>
using std::cout;
using std::endl;

/* 
 * Reveres a string
 * @param *str  Pointer to the string that needs to be reveresed
 */
 void reverse (char *str){
  if(*str){
    reverse(str+1);
    cout<<*str;
  }
 }

/* Test Client */
int main()
{
   char s[] = "Mukul Raina";
   reverse(s);
   cout<<endl;
   return 0;
}


