/* This program calculautes the sum of list of numbers iteratively */
#include <iostream>
using std::cout;
using std::endl;

/*
 * Returns the sum of all elements in the list
 */
int sumOfList(int *p, int size){
  int sum = 0;
  for (int i = 0 ; i < size ; i++){
    sum += p[i];
  }
  return sum;
}

/* Test Client */ 
int main(){
  int a[] = {5,5,5,5,5,5,5,5,5,5};
  cout<<sumOfList(a,10)<<endl;
  return 0;
}
