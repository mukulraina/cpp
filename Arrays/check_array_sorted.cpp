/* Check if an array is sorted */
#include <iostream>
using namespace std;

bool isArraySorted(int *p, int size){
  if(size == 0){
    cout<<"EMPTY ARRAY\n";
    return false;
  }
  for (int i = 0 ; i < size - 1 ; i++){
    if (p[i] > p[i + 1])
      return false;
  }
  return true;
}

int main(){
  int a[] = {86, 75, 84, 15, 85, 84, 13, 77, 72, 13};
  int b[] = {1,2,3,4,5,6,7,8,9,10};
  cout<<"Result: "<<isArraySorted(b,10)<<endl;
  return 0;
}
