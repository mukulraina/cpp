#include <iostream>
#include <cstring>
using namespace std;

struct Student
{
  char name[50];
  char city[50];
  int id;
};

int main()
{
  // Declare student1 and student2 of type Student
  struct Student student1;
  struct Student student2;

  // Assign stuff to student1
  strcpy(student1.name,"Mukul Raina");
  strcpy(student1.city,"Toronto");
  student1.id = 7;

  // Printing details about student1
  cout<<student1.name<<endl;
  cout<<student1.city<<endl;
  cout<<student1.id<<endl;

  return 0;
}