/*
By declaring a function member as static, 
you make it independent of any particular object of the class. 

A static member function can be called even if no objects of the class exist

The static functions are accessed using only the class name and the scope resolution operator ::

A static member function can only access: 
  1)  static data member
  2)  other static member functions
  3)  any other functions from outside the class.

Static member functions have a class scope and 
they do not have access to the this pointer of the class

You could use a static member function to determine 
whether some objects of the class have been created or not.

In previous file (for static variables), One good use of using 
static variables could be if you wanna keep track of how many 
objects of that class has been created
*/
#include <iostream>
using namespace std;

class Box
{
  private:
    int length;
    int breath;
    int height;

  public:
    static int objectCount;

    // Constructor definition
    Box(int l = 1, int b = 2, int h = 3)
    {
      length = l;
      breath = b;
      height = h;
      // Increase everytime an object is created
      objectCount++;
    }

    // Calculates the volume of the box
    int volume()
    {
      return length * breath * height;
    }

    // Define a static member function
    static int getCount()
    {
      return objectCount;
    }
};

// Initialize the static member of the class
int Box::objectCount = 0;

int main()
{
    
  // Print the total number of objects before creating object
  cout<<"Initial stage count: "<<Box::getCount()<<endl;

  // Make some Box objects
  Box box1(1,2,3);
  Box box2(4,5,6);
  Box box3(7,8,9);

  // Print the total number of objects again
  cout<<"Total number of objects created by far: "<<Box::getCount()<<endl;  

  return 0;
}




















