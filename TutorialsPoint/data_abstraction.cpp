/*
Data abstraction is a programming (and design) technique that relies on the separation of interface and implementation.

C++ classes provides great level of data abstraction. 
They provide sufficient public methods to the outside world to play with the functionality of the object and to manipulate object data, 
i.e., state without actually knowing how class has been implemented internally.

For example, your program can make a call to the sort() function without knowing what algorithm the function actually uses to sort the given values. 
In fact, the underlying implementation of the sorting functionality could change between releases of the library, 
and as long as the interface stays the same, your function call will still work.

ACCESS LABELS FORCE ABSTRACTIONS (e.g. private and public)
  1)  Members defined with a public label are accessible to all parts of the program. The data-abstraction view of a type is defined by its public members.
  2)  Members defined with a private label are not accessible to code that uses the class. The private sections hide the implementation from code that uses the type.

Abstraction separates code into interface and implementation. So while designing your component, 
you must keep interface independent of the implementation so that if you change underlying implementation then interface would remain intact.
*/

// In this class we have an interace we add a constant value to the value passed, but we dont show the user how we are actually doing it
#include <iostream>
using namespace std;

class Adder{
   public:
      // constructor
      Adder(int i = 0)
      {
        total = i;
      }
      // interface to outside world
      void addNum(int number)
      {
          total += number;
      }
      // interface to outside world
      int getTotal()
      {
          return total;
      };
   private:
      // hidden data from outside world
      int total;
};
int main( )
{
   Adder a;
   
   a.addNum(10);
   a.addNum(20);
   a.addNum(30);

   cout << "Total " << a.getTotal() <<endl;
   return 0;
}
