/*
We can define class members static using static keyword. 
When we declare a member of a class as static it means no matter how many objects of the class are created, 
there is only one copy of the static member.

A static member is shared by all objects of the class.

All static data is initialized to zero when the first object is created, 
if no other initialization is present.

We can't put it in the class definition but it can be initialized outside the class as done in the 
following example by redeclaring the static variable, using the scope resolution operator :: to identify which class it belongs to.
*/
#include <iostream>
using namespace std;

class Box
{
  private:
    int length;
    int breadth;
    int height;

  public:
    static int objectCount;

    // Constructor definition
    Box (int l = 1, int b = 2, int h = 3)
    {
      length = l;
      breadth = b;
      height = h;
      // Increase every time object is created
      objectCount++;
    }

    // Calculates volume of the box
    int volume()
    {
      return length * breadth * height;
    }
};

// This is where you initialize the static member of the class - IMPORTANT
int Box::objectCount = 0;

int main()
{
  Box box1(1,2,3);
  Box box2(4,5,6);

  // Print the total numeber of objects - IMPORTANT
  cout<<"Total # of objects: "<<Box::objectCount<<endl;
  return 0;
}