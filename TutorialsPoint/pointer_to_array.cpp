// Pointer to an Array
#include <iostream>
using namespace std;

int main()
{
  // Declare an array
  int a[10] = {10,20,30,40,50,60,70,80,90,100};

  // Declare a pointer
  int *p;

  // Pointer points the array
  p = a; //same as p = &a[0]

  // Print the values using p
  cout<<"Using the pointer to access the data";
  for (int i = 0 ; i < 10; i++)
  {
    cout<< *(p + i)<<endl;
  }

  return 0;
}