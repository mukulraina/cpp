// Passing array to function
#include <iostream>
using namespace std;

// As a pointer
int add_using_pointer(int *p);

// As a sized array
int add_using_sized_array(int a[10]);

// As an unsized array
int add_using_unsized_array(int a[]);

int main()
{
  int a[10] = {1,2,3,4,5,6,7,8,9,10};

  cout<<add_using_pointer(a);
  return 0;
}
