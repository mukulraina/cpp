/*
A friend function of a class is defined outside that class' scope 
but it has the right to access all private and protected members of the class.

Even though the prototypes for friend functions appear in the class definition, 
friends are not member functions.

A friend can be a function, function template, or member function, or a class or class template, 
in which case the entire class and all of its members are friends.

To declare all member functions of class ClassTwo as friends of class ClassOne, 
place a following declaration in the definition of class ClassOne:
  friend class ClassTwo;
*/
#include <iostream>
using namespace std;

class Box
{
  private:
    int width;
  public:
    friend void printWidth(Box b);
    void setWidth(int w);
};

// Member functions definition
void Box::setWidth(int w)
{
  width = w;
}

// Note: printWidth() is not a member function of any class
void printWidth(Box b)
{
  // Since friend functions have access to private and protected members of the class,
  // We can access the width variable here
  cout<<"Width: "<<b.width<<endl;
}

int main()
{
  Box b;

  //set the width by calling class member function
  b.setWidth(10);

  //print the width by calling the friend function
  printWidth(b);
  return 0;
}
