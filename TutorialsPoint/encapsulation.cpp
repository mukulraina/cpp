/*
Data encapsulation is a mechanism of bundling the data, 
and the functions that use them and data abstraction is a mechanism of exposing only the interfaces and hiding the implementation details from the user.

C++ supports the properties of encapsulation and data hiding through the creation of user-defined types, called classes.

We already have studied that a class can contain private, protected and public members. By default, all items defined in a class are private.

The variables length, breadth, and height are private. This means that they can be accessed only by other members of the Box class, and not by any other part of your program. This is one way encapsulation is achieved.
*/