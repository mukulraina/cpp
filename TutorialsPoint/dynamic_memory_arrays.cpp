#include <iostream>
#include <string.h>
using namespace std;

int main()
{
  // Initialize the pointer to NULL
  char *p = NULL;;

  // Allocate memory 
  p = new char[20];

  // Put data - just copies one character
  strcpy(p,"mukul");

  // Print data
  cout<<"data: "<<*p<<endl;

  // Free up the memory
  delete [] p;

  return 0;
}