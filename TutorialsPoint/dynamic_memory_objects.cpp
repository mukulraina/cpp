#include <iostream>
using namespace std;

class Box
{
  public:
    static int objectCount;
    Box()
    {
      objectCount++;
      cout<<"Constructor called: #"<<objectCount<<endl;
    }
    ~Box()
    {
      objectCount--;
      cout<<"Destructor called: #"<<objectCount<<endl;
    }
};

int Box::objectCount = 0;

int main()
{
  // Make instances of Box type - dynamically
  Box *box1 = new Box();
  Box *box2 = new Box();
  Box *box3 = new Box();

  // Free up the memory
  delete box1;
  delete box2;
  delete box3;

  // Do it using arrays
  cout<<"Doing it using arrays this time"<<endl;
  Box *arrayOfBoxObjects = new Box[4];

  delete [] arrayOfBoxObjects;

  return 0;
}