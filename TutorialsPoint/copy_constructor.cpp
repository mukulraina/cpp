/*
The copy constructor is a constructor which creates an object by initializing it with an object of the same class, 
which has been created previously. 

The copy constructor is used to:
  1)  Initialize one object from another of the same type.
  2)  Copy an object to pass it as an argument to a function.
  3)  Copy an object to return it from a function.

If the class has pointer variables and has some dynamic memory allocations, 
then it is a must to have a copy constructor.

The most common form of copy constructor is shown here:
  classname (const classname &obj) 
  {
   // body of constructor
   // Here, obj is a reference to an object that is being used to initialize another object.
  }
*/
#include <iostream>
using namespace std;

class Line
{
  private:
    int *ptr;

  public:
    int getLength(void);
    Line(int len);          // Simple constructor
    Line(const Line &obj);  // Copy constructor 
    ~Line();                // Destructor 
};

// Deifiniton for simple constructor
Line::Line(int len)
{
  cout<<"simple constructor"<<endl;
  // Allocate memory for the pointer
  ptr = new int;
  *ptr = len;
}

// Definition for copy constructor
Line::Line(const Line &obj)
{
  cout<<"Copy constructor"<<endl;
  ptr = new int;
  *ptr = *obj.ptr;
}

// Destructor
Line::~Line(void)
{
  cout<<"Destructor"<<endl;
}

int Line::getLength(void)
{
  return *ptr;
}

void display(Line obj)
{
  cout<<"Length of line: "<<obj.getLength()<<endl;
}

// Main function of the program
int main()
{
  Line line(10);
  display(line);
  return 0;
}















































