/*
Templates are the foundation of generic programming, which involves writing code in a 
way that is independent of any particular type.

A template is a blueprint or formula for creating a generic class or a function. 

There is a single definition of each container, such as vector, but we can define many different kinds of vectors for example, vector <int> or vector <string>.
*/
#include <iostream>
#include <string>
using namespace std;

template <typename T>
inline T const& Max(T const& a, T const& b)
{
  return a < b ? b:a;
}

int main()
{
  // Integers
  int i = 10;
  int j = 20;
  cout << "Max(i,j): " << Max(i,j) <<endl;

  // Floats
  double f1 = 13.5; 
  double f2 = 20.7; 
  cout << "Max(f1, f2): " << Max(f1, f2) << endl; 

  // Strings
  string s1 = "Hello"; 
  string s2 = "World"; 
  cout << "Max(s1, s2): " << Max(s1, s2) << endl; 
}
