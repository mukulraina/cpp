/*
For example, you might be writing some code that has a function called xyz() and there
is another library available which is also having same function xyz(). Now the compiler 
has no way of knowing which version of xyz() function you are referring to within your 
code.

A namespace is designed to overcome this difficulty and is used as additional 
information to differentiate similar functions, classes, variables etc. with the same 
name available in different libraries.

Let us see how namespace scope the entities including variable and functions:
*/
#include <iostream>
using namespace std;

// First namespace
namespace first_space
{
  void func()
  {
    cout<<"Inside first_space"<<endl;
  }
}

// Second namespace
namespace second_space
{
  void func()
  {
    cout<<"Inside second_space"<<endl;
  }
}

int main()
{
  // Call function from first namespace
  first_space::func();

  // Call function from second namespace
  second_space::func();

  return 0;
}














































