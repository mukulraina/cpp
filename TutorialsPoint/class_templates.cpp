/*
Following is the example to define class Stack<> and implement generic methods to push and pop the elements from the stack:
*/
#include <iostream>
#include <vector>
#include <cstdlib>
#include <string>
#include <stdexcept>

using namespace std;

template <class T>
class Stack
{
  private:

    // Elements
    vector<T> elems;

  public:

    // Pushes new elements on stack
    void push(T const&);

    // Pops an element from the stack
    void pop();

    // Gives the top most element of the stack
    T top() const;

    // Checks if the stack is empty
    bool empty() const{
      return elems.empty();
    }
};

// Definition for Push()
template <class T>
void Stack<T>::push (T const& elem)
{
  // Push new element
  elems.push_back(elem);
}

// Definition for pop()
template <class T>
void Stack<T>::pop()
{
  if(elems.empty()){
    throw out_of_range("Empty Stack");
  }
  // Pop an element
  elems.pop_back();
}

// Definition for top
template <class T>
T Stack<T>::top() const
{
  if(elems.empty())
  {
    throw out_of_range("Empty Stack, so no top element");
  }
  // Return copy of the last element
  return elems.back();
}

// Main
int main()
{
  try{
    // Stack of ints
    Stack<int> intStack;

    // Stack of strings
    Stack<string> stringStack;

    // Manipulate int stack
    intStack.push(7);
    intStack.push(8);
    intStack.push(9);

    // Print the top element of the intStack
    cout << intStack.top() << endl;

    // Manipulate the string stack
    stringStack.push("hello");
    cout << stringStack.top() << std::endl; 
    stringStack.pop(); 
    stringStack.pop(); 
  }
  catch (exception const& ex){
    cerr << "Exception: "<<ex.what()<<endl;
    return -1;
  }
  return 0;
}
