/*
C++ polymorphism means that a call to a member function will cause a different function to be executed
depending on the type of object that invokes the function.
*/
#include <iostream>
using namespace std;

// Base class
class Shape
{
  protected:
    int width;
    int height;
  public:
    Shape(int w=0, int h=0)
    {
       width = w;
       height = h; 
    }
    int area()
    {
      cout<<"Parent class area: "<<endl;
      return 0;
    }
};

// Derived class 1
class Rectangle: public Shape
{
  public:
    Rectangle(int w=0, int h=0):Shape(w,h){}
    int area()
    {
      cout<<"Rectangle class area: "<<endl;
      return (width * height);
    }
};

// Derived class 2
class Triangle: public Shape
{
  public:
    Triangle(int w=0, int h=0):Shape(w,h){} //not declaring 'w' and 'h' here cos we declared them above already
    int area()
    {
      cout<<"Triangle class area: "<<endl;
      return (width * height/2);
    }
};

// Main function of the program
int main()
{
  Shape *shape;
  Rectangle rec(10,7);
  Triangle  tri(10,5);

  // Store the address of Rectangle
  shape = &rec;
  // call Rectangle area
  shape->area();  // calls function from the base class

  // Store the address of Triangle
  shape = &tri;
  // Call triangle area
  shape->area();  // calls function from the base class
  return 0;
}




























