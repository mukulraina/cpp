/*
A pointer to a C++ class is done exactly the same way as a pointer to a structure and 
to access members of a pointer to a class you use the member access operator -> operator, 
just as you do with pointers to structures. Also as with all pointers, you must initialize the pointer before using it.
*/
#include <iostream>
using namespace std;

class Box
{
  private:
    int length;
    int breadth;
    int height;

  public:
    // Constructor definition
    Box(int l = 1, int b = 2, int h = 3)
    {
      length = l;
      breadth = b;
      height = h;
    }

    int volume()
    {
      return length * breadth * height;
    }
};

int main()
{
  Box Box1(1,2,3);
  Box Box2(4,5,6);
  Box *p;

  // p points to Box1
  p = &Box1;

  // print Box1 detail
  cout<<"Printing the volume of the first box: "<<p->volume()<<endl;

  // p points to Box2
  p = &Box2;

  // print Box2 details
  cout<<"Printing the volume of the second box: "<<p->volume()<<endl;
  return 0;
}
