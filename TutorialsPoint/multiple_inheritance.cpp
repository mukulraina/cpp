#include <iostream>
using namespace std;


// Base class shape
class Shape
{
  protected:
    int width;
    int height;

  public:
    void setWidth(int w)
    {
      width = w;
    }
    void setHeight(int h)
    {
      height = h;
    }
};

// Base class PaintCost
class paintCost
{
  public:
    int getCost(int area)
    {
      return area * 70;
    }
};

// Derived class 
class Rectangle: public Shape, public paintCost
{
  public:
    int getArea()
    {
      return (height * width);
    }
};

int main()
{
  Rectangle Rect;
  int area;

  Rect.setWidth(5);
  Rect.setHeight(7);

  area = Rect.getArea();

  // Print the area 
  cout<<"The area of Rect: "<<area<<endl;

  // Print the cost
  cout<<"The cost of it: "<<Rect.getCost(area)<<endl;
  return 0;
}
