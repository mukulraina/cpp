/*
*/
#include <iostream>
using namespace std;

// Base class 
class Shape
{
  protected: 
    int width;
    int height;

  public:
    void setWidth(int w)
    {
      width = w;
    }
    void setHeight(int h)
    {
      height = h;
    }
};

// Derived class 
class Rectangle: public Shape
{
  public:
    int getArea()
    {
      return (height * width);
    }
};

int main()
{
  Rectangle Rect;

  Rect.setWidth(5);
  Rect.setHeight(7);

  // Print the area of the object
  cout<<"Area of the rectangle: "<<Rect.getArea()<<endl;
  return 0;
}
