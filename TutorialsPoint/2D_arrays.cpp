//2D arrays 

#include <iostream>
using namespace std;

int main()
{
  //initialization
  int a[3][4] = {
    {1,2,3,4},
    {5,6,7,8},
    {9,10,11,12}
  };

  // Another way of initialization 
  int b[3][4] = {0,1,2,3,4,5,6,7,8,9,10,11};

  // printing the stuff
  for (int i = 0 ; i < 3 ; i++)
  {
    for (int j = 0 ; j < 4 ; j++)
    {
      cout << a[i][j];
      if (j != 3)
        cout<<",";
    }
    cout<<endl;
  }
  return 0;
}