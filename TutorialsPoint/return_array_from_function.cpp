// How to return arrays from a function 
// 1) If you want to return a single-dimension array from a function, you would have to declare a function returning a pointer
// 2) C++ does not advocate to return the address of a local variable to outside of the function so you would have to define the local variable as static variable

#include<iostream>
using namespace std;

// Function to generate and return random numbers
int * getRandom()
{
  static int r[10];

  srand((unsigned) time(NULL));
  for (int i = 0 ; i < 10 ; ++i)
  {
    r[i] = rand();
  }
  return r;
}

int main()
{
  // Pointer to store the address of the returning array
  int *p;

  p = getRandom();

  // Printing the returned array now
  for (int i = 0 ; i < 10 ; i++)
  {
    cout<< * (p + i)<<endl;
  }
  return 0;
}