/*
C++ polymorphism means that a call to a member function will cause a different function to be executed
depending on the type of object that invokes the function.

PURE VIRTUAL CLASS

It's possible that you'd want to include a virtual function in a base class so that it may be redefined in a derived class to suit the objects of that class, 
but that there is no meaningful definition you could give for the function in the base class.

The = 0 tells the compiler that the function has no body and above virtual function will be called pure virtual function
*/
#include <iostream>
using namespace std;

// Base class
class Shape
{
  protected:
    int width;
    int height;
  public:
    Shape(int w=0, int h=0)
    {
       width = w;
       height = h; 
    }
    virtual int area() = 0;
};

// Derived class 1
class Rectangle: public Shape
{
  public:
    Rectangle(int w=0, int h=0):Shape(w,h){}
    int area()
    {
      cout<<"Rectangle class area: "<<endl;
      return (width * height);
    }
};

// Derived class 2
class Triangle: public Shape
{
  public:
    Triangle(int w=0, int h=0):Shape(w,h){} //not declaring 'w' and 'h' here cos we declared them above already
    int area()
    {
      cout<<"Triangle class area: "<<endl;
      return (width * height/2);
    }
};

// Main function of the program
int main()
{
  Shape *shape;
  Rectangle rec(10,7);
  Triangle  tri(10,5);

  // Store the address of Rectangle
  shape = &rec;
  // call Rectangle area
  shape->area();  // calls function from the base class

  // Store the address of Triangle
  shape = &tri;
  // Call triangle area
  shape->area();  // calls function from the base class
  return 0;
}
