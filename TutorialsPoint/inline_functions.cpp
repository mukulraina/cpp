/*
If a function is inline, the compiler places a copy of the code of that function at each point 
where the function is called at compile time.

Using the inline keyword is simple, just put it before the name of a function. 
Then, when you use that function, pretend it is a non-inline function. 

However, once the program is compiled, the call to hello(); 
will be replaced by the code making up the function. 

Any change to an inline function could require all clients of the function to be recompiled 
because compiler would need to replace all the code once again otherwise it will continue with old functionality.


*/
#include <iostream>
using namespace std;

inline int findBiggerNumber(int x, int y)
{
  return (x > y)? x : y;
}

int main()
{
  int a = 10;
  int b = 20;

  cout<<"so here's the bigger number: "<<findBiggerNumber(a,b)<<endl;
  return 0;
}
