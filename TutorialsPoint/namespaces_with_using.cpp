/*
You can also use namespaces with using directive
*/
#include <iostream>
using namespace std;

// First namespace
namespace n1
{
  void func()
  {
    cout<<"You are using func() from n1 namespace\n";
  }
}

// Second namespace
namespace n2
{
  void func()
  {
    cout<<"You are using func() from n2 namespace\n";
  }
}

using namespace n1; // So now if you don't explicitedly tell to use n2, it will use n1 by defualt

int main()
{
  func();
  return 0;
}

