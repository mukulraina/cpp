// Program to implement merge sort
#include <iostream>
using namespace std;

void printArray(int *p, int size){
  for (int i = 0; i < size; i++){
    cout << p[i] << '\t';
  }
  cout << endl;
}

void merge(int *a, int low, int mid, int high){
  int i, j, k, tempArray[high + 1];
  i = low;
  j = mid + 1;
  k = low;

  while(i <= mid && j <= high){
    if(a[i] < a[j]){
      tempArray[k] = a[i];
      k++;
      i++;
    }
    else{
      tempArray[k] = a[j];
      k++;
      j++;
    }
  }

  while(i <= mid){
    tempArray[k] = a[i];
    k++;
    i++;
  }

  while(j <= high){
    tempArray[k] = a[j];
    k++;
    j++;
  }

  for(int i = low; i < k; i++){
    a[i] = tempArray[i];
  }
}

void mergeSort(int *a, int low, int high){
  if(low < high){
    int mid = (low + high) / 2;
    mergeSort(a,low,mid);
    mergeSort(a,mid+1,high);
    merge(a,low,mid,high);
  }
}

int main(){
  int a[10] = {17, 37, 7, 11, 75, 90, 46, 65, 11, 78};

  mergeSort(a,0,9);

  printArray(a,9);
  return 0;
}