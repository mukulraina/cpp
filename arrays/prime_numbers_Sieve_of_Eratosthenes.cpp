// Program to return all prime numbers smaller than N
#include <iostream>
#include <math.h>
using namespace std;

void runEratosthenesSieve(int upperBound){
  int upperBoundSquareRoot = (int)sqrt((double)upperBound);
  bool *isComposite = new bool[upperBound + 1];
  memset(isComposite,0,sizeof(bool) * (upperBound + 1));
  
  for(int m = 2; m <= upperBoundSquareRoot; m++){
    if(!isComposite[m]){
      cout << m << endl;
      for(int k = m * m; k <= upperBound; k += m)
        isComposite[k] = true;
    }
  }

  for(int m = upperBoundSquareRoot; m <= upperBound; m++){
    if(!isComposite[m])
      cout << m << endl;
  }
  delete [] isComposite;
}

int main(){
  int n = 100;
  runEratosthenesSieve(n);
  return 0;
}
