//You are given array of numbers which increasing first then decreasing. Find the greates number
#include <iostream>
using namespace std;

int getMax(int *a,int l, int h){
  // Case 1: 1 element in the array
  if(l == h)
    return a[l];

  // Case 2a: 2 element in the array (1st one is bigger)
  if((h == l + 1) && a[l] >= a[h])
    return a[l];

  // Case 2b: 2 element in the array (2nd one is bigger)
  if((h == l + 1) && a[h] > a[l])
    return a[h];

  int m = (l + h) / 2;

  // Case 3: if we reach a point where m is bigger than both of its adjacent elements
  if(a[m] > a[m + 1] && a[m] > a[m - 1])
    return a[m];

  // Case 4: if a[m] is greater than the next element but smaller than the previous
  if(a[m] > a[m + 1] && a[m] < a[m-1])
    return getMax(a,l,m-1);
  else
  // Case 5: if a[m] is greater than a[m - 1] and smaller than a[m + 1]
    return getMax(a,m+1,h);
}

int main(){
  int a[] = {1, 3, 50, 10, 9, 7, 6};
  int l = 0;
  int size = 7;
  int h = size - 1;
  cout << "max no is at index: " << getMax(a,l,h) << endl;
  return 0;
}
