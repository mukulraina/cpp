/*
 Write an algorithm that brings all nonzero elements to the left of the array, and returns the number of nonzero elements.

 Example input: [ 1, 0, 2, 0, 0, 3, 4 ]
 Example output: 4

 [1, 4, 2, 3, 0, 0, 0]

 * The algorithm should operate in place, i.e. shouldn't create a new array.
 * The order of nonzero elements does not matter
 */

#include <iostream>
using std::cout;


//function to swap 2 values in an array
void swap (int *a, int *b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}

/*
  In this approach you iterate from the beginning of the array.
  Have a variable loc (tracks how many 0s have been inserted in the end of array)
  (if 1 zero has been moved, loc points to 2nd last element (size - 1) )
  As soon as you find a 0, swap elements at i and counter locations
  As soon as i == counter, you are done
 */
void shiftNonZeroElementsToLeft1(int *p, int size)
{
    if(size == 0 || size == 1)
    	return;

	int loc = size - 1;
    for (int i = 0 ; i < size ; i++)
    {
        if (i == loc)
            break;
        else
        {
            if (p[i] == 0)
            {
                swap(p + i,p + loc);
                loc--;
            }
        }
    }
}

//function to print values in an array
void printArray (int *p, int size)
{
    for (int i = 0 ; i < size ; i++)
    {
        cout<<p[i]<<"-";
    }
    cout<<'\n';
}

//main
int main()
{
    int a[] = {1, 0, 2, 0, 0, 3, 4};

    printArray(a,7);
    shiftNonZeroElementsToLeft1(a,7);
    printArray(a, 7);

    return 0;
}
