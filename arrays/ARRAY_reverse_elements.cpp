// Program to reverse the array
#include <iostream>
using std::cout;
using std::endl;

void swap(int *a, int *b){
  int temp = *a;
  *a = *b;
  *b = temp;
}

// Time: O(n)
void reverseArray(int *a, int size){
  if(size == 0)
    return;

  int last = size - 1;
  for(int i = 0; i < size/2; i++){
    swap(&a[i],&a[last]);
    last--;
  }
}

void printArray(int *a, int size){
  if(size == 0)
    return;

  for(int i = 0; i < size; i++){
    cout << a[i] << '\t';
  }
  cout << endl;
}

// Test Client
int main(){
  int a[] ={4,7,3,11,8,6,5};
  int size = 7;

  cout << "Array contents before reversing: " << endl;
  printArray(a,7);

  cout << "Reversing the array" << endl;
  reverseArray(a,7);

  cout << "Printing the contents of the revered array" << endl;
  printArray(a,7);

  return 0;
}
