// Program to implement merge sort
#include <iostream>
using namespace std;

void merge(int *a, int low, int mid, int high){
  int i = low;
  int j = mid + 1;
  int k = low;
  int tempArray[high + 1];

  // Put elements from subarray into the tempArray
  while(i <= mid && j <= high){
    if(a[i] < a[j]){
      tempArray[k] = a[i];
      i++;
    }
    else{
      tempArray[k] = a[j];
      j++;
    }
    k++;
  }

  // Check if first subarray has elements left
  while(i <= mid){
    tempArray[k++] = a[i++];
  }

  // Check if second subarray has elements left
  while(j <= high)
    tempArray[k++] = a[j++];

  // Copy tempArray into the original array
  // NOTE: its < k not <= k
  for(int i = low; i < k; i++){
    a[i] = tempArray[i];
  }
}

void mergeSort(int *a, int low, int high){
  if(low < high){
    int mid = (low + high) / 2;
    mergeSort(a,low,mid);
    mergeSort(a,mid+1,high);
    merge(a,low,mid,high);
  }
}

void printArray(int *a, int size){
  for (int i = 0; i < size; i++)
    cout << a[i] << '\t';
  cout << endl;
}
/*
  Categories of test cases:
    1) Normal case:
      - more than 2 elements
      - odd # of elements
      - even # of elements
    2) Extreme 
      - 1 element
      - 2 element
    3) Null / illegal 
      - 0 elements
    4) Strange input
      - already sorted
      - reverse sorted
*/
int main(){
  int a[] = {42, 31, 54, 12, 55, 87,23, 51, 1};
  printArray(a,9);
  mergeSort(a,0,8);
  printArray(a,9);
  return 0;
}