#include <iostream>
using namespace std;

bool isEven(int n){
  return (n % 2) == 0;
}

bool isOdd(int n){
  return (n % 2) == 1;
}

void shiftEvenOdd(int *a, int size){
  int i = 0;
  int j = size - 1;
  while(i < j){
    while(isEven(a[i])){
      i++;
    }

    while(isOdd(a[j])){
      j--;
    }

    swap(a[i],a[j]);
    i++;
    j--;
  }
}

int main(){
  int a[] = {12, 34, 45, 9, 8, 90, 3};
  shiftEvenOdd(a,7);
  for(int i = 0; i < 7; i++){
    cout << a[i] << '\t';
  }
  cout << endl;
  return 0;
}
