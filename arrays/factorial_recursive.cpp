// Program to calculate factorial of a number recursively 
#include <iostream>
using namespace std;

int fac(int n){
  if(n == 0)
    return 1;

  return n * fac(n - 1);
}

int main(){
  int n;
  cout << "enter a no: " << endl;
  cin >> n;
  cout << "here's the factorial: " << fac(n) << endl;
}
