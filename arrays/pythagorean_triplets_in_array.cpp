/*
Given an array, find three numbers a, b and c such that a^2 + b^2 = c^2

Example: for array  [9, 2, 3, 4, 8, 5, 6, 10] the output of the algorithm should be {3, 4, 5} and {6, 8, 10}
*/
#include <iostream>
#include <vector>
#include <unordered_map>
#include <math.h>
using std::cout;
using std::endl;
using std::vector;

typedef std::unordered_map<int,int> Mymap;

void printTriplets(int *p, int size){
  vector<int> v;

  // Square every element and put it into hash 
  // To reduce the problem to x + y = z
  for (int i = 0; i < size; i++){
    v.push_back(p[i] * p[i]);
  }

  // Put the contents of the vector into the hash
  Mymap m;
  for(int i = 0; i < v.size(); i++){
    m.insert(Mymap::value_type(v[i],0));
  }

  // Traverse through v
  Mymap::iterator iter;

  for(int i = 0; i < v.size(); i++){
    for(int j = 0; j < v.size(); j++){
      iter = m.find(v[i] + v[j]);
      if(iter != m.end())
        cout << sqrt(v[i]) <<" + " << sqrt(v[j]) << " = " << sqrt(iter->first) << endl;
    }
  }
}

// In the above function we do get to print the triplets but,
// We print the same triplet twice, now we just do it once
void printTripletsOnlyOnce(int *p, int size){
  vector<int> v;

  // Square every element and put it into hash 
  // To reduce the problem to x + y = z
  for (int i = 0; i < size; i++){
    v.push_back(p[i] * p[i]);
  }

  // Put the contents of the vector into the hash
  Mymap m;
  for(int i = 0; i < v.size(); i++){
    m.insert(Mymap::value_type(v[i],0));
  }

  // Traverse through v
  Mymap::iterator iter;
  Mymap::iterator iterTriplets;
  Mymap triplets;

  for(int i = 0; i < v.size(); i++){
    for(int j = 0; j < v.size(); j++){
      int sum = v[i] + v[j];
      iter = m.find(sum);
      iterTriplets = triplets.find(sum);
      if(iter != m.end() && iterTriplets == triplets.end())
        cout << sqrt(v[i]) <<" + " << sqrt(v[j]) << " = " << sqrt(iter->first) << endl;
        triplets.insert(Mymap::value_type(sum,0));
    }
  }
}


int main(){
  int a[] = {9, 2, 3, 4, 8, 5, 6, 10};
  printTriplets(a,8);
  cout<<endl;
  printTripletsOnlyOnce(a,8);
  return 0;
}