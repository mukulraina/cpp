/*
Find the minimum distance btw 2 numbers in array
NOTE: keep duplicates in mind
*/
#include <iostream>
using std::cout;
using std::endl;

int minDistance(int *p, int size, int a, int b){
  // If 0 or 1 element
  if(size == 0 || size == 1){
    return -1;
  }

  // If there are 2 elements 
  if(size == 2){
    if((p[0] == a && p[1] == b) || (p[0] == b && p[1] == a))
      return 1;
    else
      return -1;
  }

  // Traverse through the array until you find one of the elements
  int i = 0;
  for (int i = 0; i < size; i++){
    if(p[i] == a || p[i] == b){
      break;
    }
  }

  int prev = i;
  int minDist = size;

  // Traverse through the rest of the array
  for (; i < size; i++)
  {
    // As soon as we find any of those 2 values
    if(p[i] == a || p[i] == b)
    {
      // Make sure its not same as the old one + the new minDist is smaller than the old one
      if(p[prev] != p[i] && (i - prev) < minDist)
      {
        minDist = i - prev;
        prev = i;
      }
      else
        prev = i;
    } 
  }
  return minDist;
}

int main(){
  int a1[] = {1,2};
  cout << minDistance(a1,2,1,2) << endl;

  int a2[] = {3,4,5};
  cout << minDistance(a2,3,3,5) << endl;

  int a3[] = {3, 5, 4, 2, 6, 5, 6, 6, 5, 4, 8, 3};
  cout << minDistance(a3,12,3,6) << endl;

  int a4[] = {2, 5, 3, 5, 4, 4, 2, 3};
  cout << minDistance(a4,8,3,2) << endl;

  int a5[] = {2,1,3, 5, 4, 2, 6, 3, 0, 0, 5, 4, 8, 3,4};
  cout << minDistance(a5,15,3,6) << endl;
  return 0;
}











































