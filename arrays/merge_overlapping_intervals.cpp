// Merge overlapping intervals
#include <iostream>
#include <vector>
#include <algorithm>
using std::cout;
using std::endl;
using std::vector;

struct Interval{
  int first;
  int second;
};

void printVector(vector<int> v){
  for (int i = 0; i < v.size(); i++){
    cout << v[i] << '\t' << endl;
  }
}

void mergeIntervals(vector<Interval> intervals){
  vector<int> v;
  // Put the values into the vector
  for(int i = 0; i < intervals.size(); i++){
    v.push_back(intervals[i].first);
    v.push_back(intervals[i].second);
  }

  // Sort the vector - O(nlogn)
  std::sort(v.begin(), v.end());

  // Traverse the sorted vector 
  int f, s;
  f = 0;
  for (int i = 0; i < v.size(); i++){
    if(v[i+1] != v[i] + 1){
      s = i;
      // To handle the condition, when a number doesn't have another number
      // To make a pair
      if(v[i] != v[f])
        cout << v[f] << '\t' << v[s] << endl;
      f = i + 1;
    }
  }
}

// Test Cases:
void testCase1(){
  // Create a set of intervals
  Interval arrayIntervals[] = { {6,8}, {1,9}, {2,4}, {3,7}, {13,14}};
  int size = 5;
  vector<Interval> intervals(arrayIntervals, arrayIntervals + size);

  // Merge Intervals
  mergeIntervals(intervals);
}

void testCase2(){
  // Create a set of intervals
  Interval arrayIntervals[] = { {1,3},{7,9},{4,6},{10,13} };;
  int size = 4;
  vector<Interval> intervals(arrayIntervals, arrayIntervals + size);

  // Merge intervals
  mergeIntervals(intervals);
}


int main(){
  testCase1();
  cout << endl;
  testCase2();
  return 0;
}








































