//Get a number just bigger than n, using the same digits
#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

// Time: O(nlogn)
bool isSameDigits(int a, int b){
  if (a == b)
    return true;

  // get each digit in number + put it in vector - O(n)
  vector<int> v1;
  while (a != 0){
    int digit = a % 10;
    v1.push_back(digit);
    a = a / 10;
  }
  vector<int> v2;
  while (b != 0){
    int digit = b % 10;
    v2.push_back(digit);
    b = b / 10;
  }

  // Sort those 2 vectors - O(nlogn) - TRY COUNTING SORT
  sort(v1.begin(), v1.end());
  sort(v2.begin(), v2.end());

  // Compare 2 vectors
  for(int i = 0; i < v1.size(); i++){
    if (v1[i] != v2[i])
      return false;
  }
  return true;
}

int getNextNumberWithSameDigits(int a){
  int originalNumber = a;
  do{
    a++;
  }while(isSameDigits(originalNumber,a) != 1);
  return a;
}

int main(){
  int a = 38376;
  cout << getNextNumberWithSameDigits(a) << endl;
  return 0;
}