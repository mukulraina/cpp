// Program to find the largest no in the array using recursion
#include <iostream>
using namespace std;

int getMaxHelper(int *p, int &max, int size){
  if(size < 1)
    return max;

  if(p[0] > max)
    max = p[0];
  return getMaxHelper(p + 1,max,size - 1);
}

int getMax(int *p, int size){
  int max = p[0];
  return getMaxHelper(p + 1,max,size - 1);
}

int main(){
  int a[] = {32,13,45,176,43,11,56,89};
  cout << getMax(a,8) << endl;

  int b[] = {1};
  cout << getMax(b,1) << endl;

  int c[] = {100,13,45,76,43,11,56,89};
  cout << getMax(c,8) << endl;

  int d[] = {32,13,45,76,43,11,56,100};
  cout << getMax(d,8) << endl;

  return 0;
}
