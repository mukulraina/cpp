// You can pass vector to a function: by value, by referrence, by pointer
#include <iostream>
#include <vector>
using namespace std;

void printVector(vector<int> &v){
  for(int i = 0; i < v.size(); i++){
    cout << v[i] << '\t';
  }
  cout << endl;
}

int main(){
  vector<int> v;
  
  v.push_back(1);
  v.push_back(2);
  v.push_back(3);
  v.push_back(4);
  v.push_back(5);

  printVector(v);
  return 0;
}
