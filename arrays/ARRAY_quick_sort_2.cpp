// Program to implement quick sort
#include <iostream>
using namespace std;

void printArray(int *a, int size){
  for (int i = 0; i < size; i++)
    cout << a[i] << '\t';
  cout << endl;
}

void quickSort(int *a,int l, int h){
  int i = l;
  int j = h;
  int pI = (l + h) / 2;
  int p = a[pI];

  while(i <= j){
    while(a[i] < p)
      i++;

    while(a[j] > p)
      j--;

    if(i <= j){
      swap(a[i],a[j]);
      i++;
      j--;
    }
  }

  if(l < j)
    quickSort(a,l,j);
  if(i < h)
    quickSort(a,i,h);
}

int main(){
  int a[10] = {17, 37, 7, 11, 75, 90, 46, 65, 11, 78};
  printArray(a,10);
  quickSort(a,0,9);
  printArray(a,10);
  return 0;
}