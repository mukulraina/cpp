// Given an array of 0s and 1s sorted. Find the first occurrence of 1
#include <iostream>
using namespace std;

// Time: O(log n) better than the linear traversal
int firstOneLocation(int *a, int low, int high){
  int mid = (low + high) / 2;

  if(a[mid] == 0 && a[mid + 1] == 1)
    return mid + 1;

  else if(a[mid] == 0)
    return firstOneLocation(a,mid,high);
  
  else
    return firstOneLocation(a,low,mid);
}

int main(){
  int a[] = {0,0,0,0,1};
  int size = 5;
  cout << "Index : " << firstOneLocation(a,0,size-1) << endl; // index 7
  return 0;
}
