// http://en.wikipedia.org/wiki/Maximum_subarray_problem
#include <iostream>
using namespace std;

int getMaxSubarraySum(int *a, int size){
  int maxSum = a[0];
  int currentSum = a[0];

  int begin = 0;
  int begin_temp = 0;
  int end = 0;

  for(int i = 0; i < size; i++){
    if(currentSum < 0){
      currentSum = a[i];
      begin_temp = i;
    }
    else{
      currentSum = currentSum + a[i];
    }

    if(currentSum >= maxSum){
      maxSum = currentSum;
      begin = begin_temp;
      end = i;
    }
  }
  cout << "begin: " << a[begin] << endl;
  cout << "end: " << a[end] << endl;
  return maxSum;
}

int main(){
  int a[] = {-2,1,-3,4,-1, 2, 1,-5,4};
  int size = 9;
  cout << getMaxSubarraySum(a,size) << endl;
  return 0;
}
