// Program to calculate the sum of elements in the array
#include <iostream>
#include <string>
#include <vector>
using std::cout;
using std::endl;
using std::string;
using std::vector;

int sum(int *p, int size){
  if(size <= 1)
    return size;

  int sum = 0;
  for(int i = 0; i < size; i++){
    sum = sum + p[i];
  } 
  return sum;
}

int main(){
  int a[] = {1,4,5,6,7,13};
  int size = 6;
  cout << "sum of the elements in the array: " << sum(a,size) << endl;
  return 0;
}
