#include <iostream>
using namespace std;

bool binarySearch(int *a, int v, int low, int high){
  if(low > high)
    return false;

  int mid = (low + high) / 2;
  if(a[mid] == v)
      return true;
    else if(v < a[mid])
      return binarySearch(a,v,low,mid);
    else 
      return binarySearch(a,v,mid+1,high);
}

int main(){
  int a[10] = {7,11,12,17,37,46,65,75,78,90};
  cout << binarySearch(a,17,0,9) << endl;
  cout << binarySearch(a,7,0,9) << endl;
  cout << binarySearch(a,90,0,9) << endl;
  cout << binarySearch(a,99,0,9) << endl;
  return 0;
}
