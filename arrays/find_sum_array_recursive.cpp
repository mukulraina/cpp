// Program to find the sum of the elements in an array recursively 
#include <iostream>
using namespace std;

int getSumHelper(int *a, int &sum, int size){
  if(size < 1)
    return sum;

  sum += a[0];
  return getSumHelper(a + 1,sum,size - 1);
}

int getSum(int *a, int size){
  int sum = a[0];
  return getSumHelper(a + 1,sum,size - 1);
}

int main(){
  int a[] = {1,2,3,4,5,6,7,8,9,10};
  cout << getSum(a,10) << endl;

  int b[] = {1};
  cout << getSum(b,1) << endl;

  int c[] = {1,2};
  cout << getSum(c,2) << endl;  
  return 0;
}
