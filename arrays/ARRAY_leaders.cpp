// Program to find the leaders in the array
#include <iostream>
using namespace std;

void printLeaders(int *a, int size){
  if(size == 0)
    return;

  // Rightmost element is always leader
  cout << a[size - 1] << endl;

  int max = a[size - 1];
  for (int i = size - 2; i >= 0; i--){
    if(a[i] > max){
      cout << a[i] << endl;
      max = a[i];
    }
  }
}

int main(){
  int a[] = {16, 17, 4, 3, 5, 2};
  int size = 6;
  printLeaders(a,6);
  return 0;
}
