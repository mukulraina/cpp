// Program to check if a number is a happy number or not
#include <iostream>
#include <unordered_map>
using namespace std;
typedef unordered_map<int,int> Mymap;

int getSumOfDigits(int n){
  int sum = 0;
  while(n > 0){
    // Get the last digit
    int digit = n % 10;
    // Add it to the sum
    sum += digit * digit;
    // Remove the last digit from the number
    n = n / 10;
  }
  return sum;
}

bool isHappyHelper(int n, Mymap &hash){
  int sum = getSumOfDigits(n);
  if(sum == 1)
    return true;

  Mymap::iterator iter;
  iter = hash.find(sum);
  if(iter != hash.end())
    return false;
  else{
    hash.insert(Mymap::value_type(sum,0));
    return isHappyHelper(sum,hash);
  }
}

bool isHappy(int n){
  Mymap hash;
  return isHappyHelper(n,hash);
}

int main(){
  cout << isHappy(7) << endl;
  return 0;
}
