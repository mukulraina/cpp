//Given an array, return the second largest number. Write code for this.
//You can not modify the array, just traverse the array once and return the required number.
//Handle all the edge cases. What should be the function signature.
#include <iostream>
using std::cout;
using std::endl;

int secondLargest(int *p, int size){
  // 0 element
  if(size == 0){
    cout <<"Sorry emtpy array" << endl;
    return -1;
  }

  // 1 element
  if(size == 1){
    cout << "Sorry only 1 element in the array" << endl;
    return -1;
  }

  // 2 elements
  if(size == 2){
    return p[0] > p[1] ? p[1] : p[0];
  }

  // More than 2 elements
  int max, secondMax;

  if(p[0] > p[1]){
    max = p[0];
    secondMax = p[1];
  }
  else{
    max = p[1];
    secondMax  = p[0];
  }

  // Iterate through the array
  for(int i = 0; i < size; i++){
    if(p[i] > max){
      secondMax = max;
      max = p[i];
    }
  }
  return secondMax;
}

int main(){
  int a[] = {5,3,2,1,0};
  cout << secondLargest(a,5) << endl;
  return 0;
}
