#include <iostream>
using namespace std;

void merge(int *a, int low, int mid, int high){
  int i = low;
  int k = low;
  int j = mid + 1;
  int tempArray[high + 1];

  while(i <= mid && j <= high){
    if(a[i] < a[j]){
      tempArray[k] = a[i];
      k++;
      i++;
    }
    else{
      tempArray[k] = a[j];
      k++;
      j++;
    }
  }

  while(i <= mid){
    tempArray[k] = a[i];
    k++;
    i++;
  }

  while(j <= high){
    tempArray[k] = a[j];
    k++;
    j++;
  }

  for(int i = low; i < k; i++){
    a[i] = tempArray[i];
  }
}

void mergeSort(int *a, int low, int high){
  if(low < high){
    int mid = (low + high) / 2;
    mergeSort(a,low,mid);
    mergeSort(a,mid + 1,high);
    merge(a,low,mid,high);
  }
}

void printArray(int *a, int size){
  for(int i = 0; i < size; i++){
    cout << a[i] << '\t';
  }
  cout << endl;
}

int main(){
  int a[] = {42, 31, 54, 12, 55, 87,23, 51, 1};
  printArray(a,9);
  mergeSort(a,0,8);
  printArray(a,9);
  return 0;
}
