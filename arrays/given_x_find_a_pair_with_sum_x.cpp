// Given an array A[] and a number x, check for pair in A[] with sum as x
// Return true if such 2 elements exists, false if they dont
// Assume: only 2 elements form x (no more than 1 pair)
#include <iostream>
#include <unordered_map>
using namespace std;
typedef std::unordered_map<int,int> Mymap;


// Hash Approach: O(n)
bool checkPair(int *a, int size, int x){
  if(size <= 1)
    return false;

  if(size == 2){
    if(a[0] + a[1] == x)
      return true;
    return false;
  }

  // Put the values into the hash
  Mymap m;
  Mymap::iterator iter;
  for(int i = 0; i < size; i++){
    iter = m.find(a[i]);
    if(iter == m.end())
      m.insert(Mymap::value_type(a[i],0));
  }

  // traverse through the array again
  for(int i = 0; i < size; i++){
    int v = x - a[i];
    iter = m.find(v);
    if(iter != m.end()){
      cout << a[i] << ":" << iter->first << endl;
      return true;
    }
  }
  return false;
}

// Test Client
int main(){
  int a[] = {1, 4, 45, 6, 10, -8};
  int x = 16;

  cout << checkPair(a,6,x) << endl;
  return 0;
}
