#include <iostream>
#include <unordered_map>
using namespace std;
typedef unordered_map<int,int> Mymap;

int sumOfDigitsSquared(int n){
  int sum = 0;
  while(n > 0){
    int digit = n % 10;
    sum += digit * digit;
    n = n / 10;
  }
  return sum;
}

bool isHappyHelper(int n, Mymap &hash){
  int v = sumOfDigitsSquared(n);
  if(v == 1)
    return true;
  else{
    Mymap::iterator iter;
    iter = hash.find(v);
    if(iter != hash.end())
      return false;
    else{
      hash.insert(Mymap::value_type(v,0));
      return isHappyHelper(v,hash);
    }
  }
}

bool isHappy(int n){
  Mymap hash;
  return isHappyHelper(n,hash);
}

int main(){
  cout << isHappy(6) << endl;
  cout << isHappy(7) << endl;
  return 0;
}
