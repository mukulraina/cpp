//
//  findMax.cpp
//  Interviews
//
//  Created by Mukul Raina on 2014-06-24.
//  Copyright (c) 2014 Mukul Raina. All rights reserved.
//

#include <iostream>
#include "findMax.h"

using std::cout;
using std::endl;

//returns the largest element in the array
int findMax(int *p,int size)
{
    //empty array
    if (size == 0)
        return -1;
    
    //single element
    if (size == 1)
        return p[0];
    
    //2 elements
    if (size == 2)
    {
        return (p[0] > p[1]) ? p[0] : p[1];
    }
    
    //more than 1 element
    int max = p[0];
    for (int i = 0 ; i < size ; i++)
    {
        if(p[i] > max)
            max = p[i];
    }
    return max;
}
