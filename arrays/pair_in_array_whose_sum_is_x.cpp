// Given an array A[] and a number x, check for pair in A[] with sum as x
#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>
using std::cout;
using std::endl;
using std::string;
using std::vector;
typedef std::unordered_map<int,int> Mymap;

// This function print the required pairs BUT twice
void printPairs1(int *p, int size, int v){
  // Less than 2 elements
  if(size < 2)
    return;

  // 2 elements
  if(size == 2){
    if(p[0] == p[1]){
      cout << p[0] << ":"<< p[1] << endl;
      return;
    }
    else
      return;
  }

  // More than 2 elements
  Mymap m;
  Mymap::iterator iter;

  // Insert array elements into hash
  for(int i = 0; i < size; i++){
    iter = m.find(p[i]);
    if(iter == m.end())
      m.insert(Mymap::value_type(p[i],0));
  }

  // Traverse the array again - O(n)
  int valueToFind;
  for(int i = 0; i < size; i++){
    valueToFind = v - p[i];
    iter = m.find(valueToFind);
    if(iter != m.end())
      cout << p[i] << ":" << iter->first << endl;
  }
}

// This function print the required pairs once
void printPairs2(int *p, int size, int v){
  // Less than 2 elements
  if(size < 2)
    return;

  // 2 elements
  if(size == 2){
    if(p[0] == p[1]){
      cout << p[0] << ":"<< p[1] << endl;
      return;
    }
    else
      return;
  }

  // More than 2 elements
  Mymap m;
  Mymap::iterator iter;

  // Insert array elements into hash
  for(int i = 0; i < size; i++){
    iter = m.find(p[i]);
    if(iter == m.end())
      m.insert(Mymap::value_type(p[i],0));
  }

  // Traverse the array again - O(n)
  int valueToFind;
  for(int i = 0; i < size; i++){
    valueToFind = v - p[i];
    iter = m.find(valueToFind);

    if(iter != m.end() && iter->second == 0){
      cout << p[i] << ":" << iter->first << endl;
      iter = m.find(p[i]);
      iter->second = 1;
    }
  }
}

int main(){
  int a[] = {1, 4, 45, 6, 10, 8};
  int v = 16;
  int size = 6;
  printPairs2(a,size,v); 
  return 0;
}
