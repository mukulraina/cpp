// Program to reverse digits of a number
#include <iostream>
#include <queue>
#include <math.h>
using std::cout;
using std::endl;
using std::queue;

// Reverse digits of a number in O(n) 
// NOT inplace
// Doesn't work for numbers like 1000
int reverseDigits(int n){
  // 1 digit
  if(n / 10 == 0)
    return n;

  // Push the digits in the queue
  queue<int> q;
  int elem;
  while (n != 0){
    elem = n % 10;
    q.push(elem);
    n = n / 10;
  }

  // Print the contents of a queue by keeping poping
  int power = q.size() - 1;
  int newNum = 0;
  while(q.size() != 0){
    newNum += q.front() * pow(10,power);
    q.pop();
    power--;
  }
  return newNum;
}



// Test Client
int main(){
  int n = 4562;
  cout << reverseDigits(n) << endl;
  return 0;
}
