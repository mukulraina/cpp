// Program to implement stack using array
#include <iostream>
using namespace std;

#define MAX_SIZE 5
#define MAX_INDEX 4

class Stack{
  private:
    int a[MAX_SIZE];
    int top;
  public:
    Stack();
    bool isEmpty();
    bool isFull();
    void push(int v);
    int pop();
    int peek();
    void printStack();
};

Stack::Stack(){
  top = -1;
}

bool Stack::isEmpty(){
  return top == -1;
}

bool Stack::isFull(){
  return top == MAX_INDEX;
}

void Stack::push(int v){
  if(isFull()){
    cout << "stack overlflow" << endl;
    return;
  }

  top++;
  a[top] = v;
}

int Stack::peek(){
  if(isEmpty()){
    cout << "sorry empty stack cant peek" << endl;
    return -1;
  }

  return a[top];
}

int Stack::pop(){
  if(isEmpty()){
    cout << "sorry empty stack cant pop" << endl;
    return -1;
  }

  top--;
  return a[top + 1];
}

void Stack::printStack(){
  if(isEmpty()){
    cout << "sorry empty stack cant print" << endl;
    return;
  }

  for (int i = 0; i <= top; i++){
    cout << a[i] << '\t';
  }
  cout << endl;
}

int main(){
  Stack stack;

  // Poping an empty stack
  cout << "Just poped: " << stack.pop() << endl;

  // Push some elements
  stack.push(10);
  stack.push(20);
  stack.push(30);
  
  // Print stack
  stack.printStack();
  cout << "peek value: " << stack.peek() << endl;
  cout << endl;

  // Pop and push
  cout << "Just poped: " << stack.pop() << endl;
  stack.push(40);
  stack.printStack();
  cout << "peek value: " << stack.peek() << endl;
  cout << endl;

  //Try fulling the stack now
  stack.push(50);
  stack.push(60);
  stack.printStack();
  cout << "peek value: " << stack.peek() << endl;

  stack.push(70);
  stack.printStack();
  cout << "peek value: " << stack.peek() << endl;
  cout << endl;
  return 0;
}
