#include <iostream>
#include <unordered_map>
using std::cout;
using std::endl;

typedef std::unordered_map<int,int> Mymap;

int majorityElement(int *p, int size){
  if(size == 0)
    return -1;
  if(size == 1)
    return p[0];

  Mymap m;
  Mymap::iterator iter;

  // Insert the elements into hash
  // Increment their # of occur (value) if already present
  for(int i = 0; i < size; i++){
    iter = m.find(p[i]);
    if(iter == m.end())
      m.insert(Mymap::value_type(p[i],1));
    else
      iter->second++;
  }

  // traverse through the array again
  // And find an element whose ->second >= size / 2
  for(int i = 0; i < size; i++){
    iter = m.find(p[i]);
    if(iter->second >= (size /2))
      return iter->first;
  }
  cout << "Sorry there's no majorityElement in the array" << endl;
  return -1;
}

// Test Client
int main(){
  int a[] = {3,3,4,2,4,4,2,4,4};
  int size = 9;
  cout << majorityElement(a,size) << endl;
}
