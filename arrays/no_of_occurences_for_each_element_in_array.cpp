/*
 This program finds how many times all elements appear in the array
 */

#include <iostream>
#include <unordered_map>
using std::cout;

typedef std::unordered_map<int,int> myMap;

/*
 *  numberOfTimesElementsAppear() takes an array and prints the number of occurences of each element
 *  @param p       pointer to the array
 *  @param size    size of the array
 *  @param m       unordered_map used to store the elements as keys and their occurences as values
 *  @param iter    an unoredered_map iterator to work with the map m
 */
void numberOfTimesElementsAppear(int *p, int size){
    //if empty
    if (size == 0)
        return;
    
    //if single element
    if (size == 1){
        cout<<p[0]<<" appeared once"<<'\n';
        return;
    }
    
    //create a hash
    int i;
    myMap m;
    myMap::iterator iter;
    
    //iterate and insert values in the array
    for (i = 0 ; i < size ; i++){
        //lookup if the values already exist in the hash
        iter = m.find(p[i]);
        
        if(iter == m.end()){
            m.insert(myMap::value_type(p[i],1));
        }
        else{
            iter->second++;
        }
    }
    
    //iterate through the loop again
    //print all the elements (keys) and their occurences(values)
    for (i = 0 ; i < size ; i++){
        iter = m.find(p[i]);
        cout<<"element: "<<p[i]<<"  "<<"no of occurences: "<<iter->second<<'\n';
    }
}

//main()
int main()
{
    int a[] = {1,2,3,4,5,1,2,3,4,5};
    int b[] = {};
    int c[] = {1};
    
    numberOfTimesElementsAppear(a, 10);
    numberOfTimesElementsAppear(b, 0);
    numberOfTimesElementsAppear(c, 1);
    
    return 0;
}
