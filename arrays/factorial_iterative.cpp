// Program to calculate factorial of a number iteratively
#include <iostream>
using namespace std;

int fac(int n){
  int result = 1;
  for(int i = 1; i <= n; i++){
    result *= i;
  }
  return result;
}

int main(){
  int n;
  cout << "enter a no: " << endl;
  cin >> n;
  cout << "here's the factorial: " << fac(n) << endl;
  return 0;
}