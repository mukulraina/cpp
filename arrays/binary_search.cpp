#include <iostream>
using namespace std;

int binarySearch(int *a, int l, int h, int v){
  if(h < l)
    return -1;
  else{
    int m = (l + h ) / 2;

    if(v < a[m])
      return binarySearch(a,l,m-1,v);
    else if(v > a[m])
      return binarySearch(a,m+1,h,v);
    else
      return m;
  }
}

int main(){
  int a[] = {2,3,4,10,40};
  int size = 5;
  int v = 10;
  cout << binarySearch(a,0,size-1,v) << endl;
  return 0;
}
