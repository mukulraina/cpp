// Program to find the missing element in an array
// e.g. input {1,2,3,4,6,7,8} -> 5
#include <iostream>
using std::cout;
using std::endl;

// a[i + 1] != a[i] + 1 approach
int getMissingElement(int *a, int size){
  if(size <= 1)
    return -1;

  // Traverse through the array
  for(int i = 0; i < size; i++){
    if(a[i + 1] != a[i] + 1)
      return i + 2;
  }

  // If there's no such element
  cout << "Sorry no such element" << endl;
  return -1;
}

// Sum of all elements - Approach
int getMissingElement2(int *a, int size){
  if(size <= 1)
    return -1;

  // Sum of the range (range = last element in the array)
  int range = a[size - 1];
  int sumOfRange = 0;
  for(int i = 0; i <= range; i++){
    sumOfRange += i;
  }

  // Sum of array elements
  int sumOfElems = 0;
  for(int i = 0; i < size; i++){
    sumOfElems += a[i];
  }

  // Get missing element
  return sumOfRange - sumOfElems;
}

// Test Client
int main(){
  int a[] = {1,2,3,4,6,7,8};
  int size = 7;
  cout << getMissingElement2(a,7) << endl;
  return 0;
}
