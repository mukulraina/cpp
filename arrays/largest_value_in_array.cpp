// Program to find the largest value in array
#include <iostream>
using namespace std;

int getMax(int *a, int size){
  if(size == 0)
    return -1;

  int max = a[0];
  for(int i = 0; i < size; i++){
    if(a[i] > max)
      max = a[i];
  }
  return max;
}

int main(){
  int a[] = {11,43,12,64,21,67,864,21,345,5};
  cout << getMax(a,10) << endl;
  return 0;
}
