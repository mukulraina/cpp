// Recursively search for an element in an array
#include <iostream>
using namespace std;;

bool searchElem(int *a, int v, int size){
  if(size == 0)
    return false;

  if(a[0] == v)
    return true;
  else
    return searchElem(a + 1,v,size - 1);
}

int main(){
  int a[] = {8,5,10,4,7,9,14,3};
  int size = 8;
  int v = 3;
  cout << searchElem(a,v,size) << endl;
}
