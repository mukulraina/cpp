/*
 Prints all the elements that appear odd number of times 
 time complexity O(n)
 */

#include <unordered_map>
#include <iostream>

using std::cout;
using std::endl;

typedef std::unordered_map<int,int> myMap;

void elementsThatOccurOddNumberOfTimes(int *p, int size)
{
    if (size == 0)
        return ;
    
    if (size == 1)
    {
        cout<<p[0];
        return;
    }
    
    //create an empty hash
    myMap m;
    int i;
    myMap::iterator iter;
    
    //iterate and insert values in the map
    for (i = 0 ; i < size ; i++)
    {
        //look up if key already exists in the map
        iter = m.find(p[i]);
        
        if(iter == m.end())
            m.insert(myMap::value_type(p[i],1));
        else
            iter->second++;
        }
    
    //iterating through the array again
    //If an element's value in hash is even, print it
    for (i = 0 ; i < size ; i++)
    {
        iter = m.find(p[i]);
        if (iter->second % 2)
        {
            cout<<iter->first<<'\n';
            //since we have already printed the element once, so we delete it from hash
            m.erase(iter->first);
        }
    }
}

int main()
{
    int a[] = {1,4,5,6,2,7,8,2,1,4};
    int b[] = {};
    int c[] = {1};
    
    elementsThatOccurOddNumberOfTimes(a, 10);
    cout<<'\n';
    
    elementsThatOccurOddNumberOfTimes(b,0);
    cout<<'\n';
    
    elementsThatOccurOddNumberOfTimes(c,1);
    cout<<'\n';
    
    return 0;
}
