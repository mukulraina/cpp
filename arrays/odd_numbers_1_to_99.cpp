// Program to print all odd numbers from 1 to 99
#include <iostream>
using namespace std;

void printOdd(){
  for(int i = 2; i <99; i++){
    if(i % 2 != 0)
      cout << i << endl;
  }
}

int main(){
  printOdd();
  return 0;
}
