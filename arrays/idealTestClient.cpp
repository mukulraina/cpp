// Ideal test client for interview questions

int main(){
  // Normal Case
    // even type of case
    // odd type of case

  // Extreme Case
    // 0
    // 1
    // large

  // Null / illegal 
    // no -ve n in fibonacci 

  // Strange input
    // Already sorted
    // Reverse sorted
  return 0;
}