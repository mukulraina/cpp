// Program to find the equilibrium index of an element
#include <iostream>
using namespace std;

// Assuming we don't count 0 
int equiIndex(int *a, int size){
  if (size == 0)
    return -1;

  // sum initially has sum of all elements
  int sum = 0;
  for(int i = 0; i < size; i++){
    sum += a[i];
  }

  // Traverse through the array 
  int lSum = 0;
  for(int i = 0; i < size; i++){
    // sum is now right sum for index i
    sum = sum - a[i];

    // If the left and right sum are equal
    if(lSum == sum)
      return i;

    // Updating lSum
    lSum = lSum + a[i];
  }
}

int main(){
  int a[] = {-7,1,5,2,-4,3,0};
  cout << equiIndex(a,7) << endl;
  return 0;
}
