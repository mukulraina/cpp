/*
 Removes elements that appeared more than once
 a[] = {1,2,2,3,3,3} -> {1}
 */

#include <unordered_map>
#include <vector>
#include <iostream>
using std::cout;
using std::vector;

typedef std::unordered_map<int,int> myMap;

/*
 Prints the elements of a vector
 @param v   reference to the vector
 */
void printVector(const vector<int> &v){
    for (int i = 0 ; i < v.size() ; i++){
        cout<<v[i]<<'\n';
    }
}

/*
 Removes duplicates from an array
 @param p           pointer to the array
 @param size        size of the array
 @param tempVector  temporary vector
 */
void removeDuplicates(int *p, int size){
    vector<int> tempVector;
    myMap m;
    myMap::iterator iter;
    int i;
    
    if(size == 0 || size == 1)
        return;
   
    //iterate through the array and put the elements into the hash
    //so that we can keep track of how many times an element appeared
    for (i = 0 ; i < size; i++){
        iter = m.find(p[i]);
        
        if(iter == m.end())
            m.insert(myMap::value_type(p[i],1));
        
        else
            iter->second++;
    }
    
    //iterate through the array again
    //put the elements who have appeared only once (value = 1)
    //into a temporary vector
    for (i = 0 ; i < size ; i++){
        iter = m.find(p[i]);
        
        if (iter->second == 1)
            tempVector.push_back(p[i]);
    }
    
    printVector(tempVector);
}


int main(){
    int a[] = {1,2,2,3,3,3,4,5,5,6,7,8};
    removeDuplicates(a,12);
}
