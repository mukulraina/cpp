// Program to print the multiplication table of 12 x 12
#include <iostream>
using namespace std;

void printTable(int n){
  for(int i = 1; i <= n; i++){
    for(int j = 1; j <= n; j++){
      cout << i * j << '\t';
    }
    cout << endl;
  }
  cout << endl;
}

int main(){
  int n = 12;
  printTable(n);
  return 0;
}
