//
//  findMax.h
//  Interviews
//
//  Created by Mukul Raina on 2014-06-24.
//  Copyright (c) 2014 Mukul Raina. All rights reserved.
//

#ifndef Interviews_findMax_h
#define Interviews_findMax_h

int findMax(int *p, int size);

#endif
