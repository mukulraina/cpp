#include <iostream>
using namespace std;

int getMaxVaue(int *a, int low, int high){
  if(low == high)
    return -1;

  int mid = (low + high) / 2;

  if(a[mid - 1] > a[mid])
    return a[mid - 1];
  else
    return getMaxVaue(a,mid,high);
}

int main(){
  int a[] = {8, 10, 20, 80, 100, 200, 400, 500, 3, 2, 1};
  int size = 11;
  int high = size - 1;
  int low = 0;

  cout << getMaxVaue(a,low,high) << endl;
  return 0;
}
