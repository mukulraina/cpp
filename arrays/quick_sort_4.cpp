#include <iostream>
using namespace std;

void printArray(int *a, int size){
  for (int i = 0; i < size; i++)
    cout << a[i] << '\t';
  cout << endl;
}

void quickSort(int *a, int left, int right){
  // get i and j
  int i = left;
  int j = right;

  // Calculate pivot
  int p = (a[i] + a[j]) / 2;

  while(i <= j){
    while(a[i] < p)
      i++;

    while(a[j] > p)
      j--;

    if(i <= j){
      swap(a[i],a[j]);
      i++;
      j--;
    }
  }

  if(left < j)
    quickSort(a,left,j);
  if(i < right)
    quickSort(a,i,right);
}

int main(){
  int a[10] = {17, 37, 7, 11, 75, 90, 46, 65, 11, 78};
  printArray(a,10);
  quickSort(a,0,9);
  printArray(a,10);
  return 0;
}