#include <iostream>
using namespace std;

void swap(int &a, int &b){
  int temp = a;
  a = b;
  b = temp;
}

bool isEven(int n){
  return (n % 2) == 0;
}

bool isOdd(int n){
  return (n % 2) == 1;
}

void shiftElements(int *a, int size){
  int i = 0;
  int j = size - 1;

  while(i < size && j > 0){
    if(isEven(a[i]))
      i = i + 2;
    if(isOdd(a[j]))
      j = j - 2;

    swap(a[i],a[j]);
  }
}

int main(){
  int a[] = {2,3,6,8,9,10,5,4,1,7};
  shiftElements(a,10);
  for (int i = 0; i < 10; ++i)
    cout << a[i] <<"<-value : index->" << i << endl;
  cout << endl;
  return 0;
}
