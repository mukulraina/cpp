/* 
  This program shows how to implement Counting sort algorithm in C++ 
  Helpful resource: http://www.geeksforgeeks.org/counting-sort/
*/
#include <iostream>
using namespace std;

/* 
 * Finds the biggest element in the array
 * @param p Pointer to the array
 * @return Returns the biggest element found
 */
int findMax(int *p, int size){
  int max = p[0];
  for (int i = 0; i < size; i++){
    if (p[i] > max)
      max = p[i];
  }
  return max;
}

/* Function to print array elements 
 * @param p     Pointer to the array
 * @param size  Size of the array
 */
void printArray(int *p, int size){
  for(int i = 0; i < size; i++){
    cout<<"element #"<<i<<" is: "<<p[i]<<endl;
  }
}

/*
 * This function initializes variable length array in C++ (Doing it directly isn't allowed)
 * @param p     Pointer to the array
 * @param size  Size of the array
 * @param value Value to be initialized to the elements
 */
void initVariableLengthArray(int *p, int size, int value){
  for (int i = 0; i < size; i++){
    p[i] = value;
  }
}

/*
 * This function copies array elements from 1 array to another
 * @param copyFrom  Array from which contents need to be copied
 * @param copyTo    Array to which contents are being copied
 * @param size      Size of the arrays  
 */
void copyArray(int *copyFrom, int *copyTo, int size){
  for (int i = 0; i < size; i++){
    copyTo[i] = copyFrom[i];
  }
}

/*
 * Counting sort implementation
 * @param p     Pointer to the array
 * @param size  Size of the array
 */
void countingSort(int *p, int size){
  /* Finds the biggest element in the array */
  int max = findMax(p,size);
  /* Allocate the 2nd array of size [max+1] and assign 0 to all its elements */
  int tempArray[max+1];
  initVariableLengthArray(tempArray,max+1,0);
  /* Make histogram */
  for (int i = 0; i < size; i++){
    tempArray[p[i]]++;
  }
  /* Sum */
  for (int j = 1; j <= max; j++){
    tempArray[j]+=tempArray[j-1];
  }
  /* Make another temp array, which will store the sorted array */
  int sortedArray[size];
  initVariableLengthArray(sortedArray,size,0);
  /* Right to left indices */
  for (int k = size-1; k >= 0; k--){
    tempArray[p[k]]--;
    sortedArray[tempArray[p[k]]] = p[k];
  }
  /* Now copy the contents of the sorted array to the original array */
  copyArray(sortedArray,p,size);
}

/* Test Client */
int main(){
  int size = 8;
  int a[] = {2,5,3,0,2,3,0,3};
  countingSort(a,size);
  printArray(a,size);
  return 0;
}
