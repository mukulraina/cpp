/* 
  This program shows how to implement quick sort in C++ 
  Helpful resource: http://www.algolist.net/Algorithms/Sorting/Quicksort
*/
#include <iostream>
using namespace std;

/*
 * Swaps 2 numbers
 * @param a 1st number to be swapped
 * @param b 2nd number to be swapped
*/
void swap(int &a, int &b){
  int temp;
  temp = a;
  a = b;
  b = temp;
}

/*
 * Implementation of quick sort algorithm
 * @param a     Array to be sorted
 * @param left  Start of the array
 * @param right End of the array
 * @param pivot We could have chosen any element as pivot, but we chose the middle one
*/
void quickSort(int *a, int left, int right){
  int i = left;
  int j = right;
  int temp;
  int pivot = a[(left + right) / 2];
  /* Partition */
  while(i<=j){
    while(a[i] < pivot)
      i++;
    while(a[j] > pivot)
      j--;
    if(i <= j){
      swap(a[i],a[j]);
      i++;
      j--;
    }
  }
  /* Recusrion */
  if(left < j)
    quickSort(a,left,j);
  if(i < right)
    quickSort(a,i,right);
}

/* Test Client */
int main(){
  int i;
  int a[10] = {17, 37, 7, 11, 75, 90, 46, 65, 11, 78};
  quickSort(a, 0, 9);
  cout<<"sorted array\n";
  for (i = 0; i < 10; i++){
      cout<<a[i]<<endl;
  }
return 0;
}
