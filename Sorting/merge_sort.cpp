/* This program merges 2 sorted subarrays 
   This is basically the merge function of the merge sort
   Need to track 3 indices:
    1) i - lower index of 1st subarray
    2) j - lower index of 2nd subarray
    3) k - current index(where we have to put the new element) in the final array which is being formed after merging the 2 subarrays
  special logic cases:
    1) compare the ith and jth element and put the smallest of two in the final array (at k index) and increment the one which was selected (i or j)
    2) if ith and jth elemens are equal, take ith (we could have taken jth, doesn't matter)
    3) if one index (ith or jth gets done) we just copy the full leftover other subarray into the final array
*/
#include <iostream>
using namespace std;


/*
 * Merges 2 sorted subarrays
 * @param a       The main bigger array that has 2 sorted subarrays in it
 * @param low     Start of the 1st subarray
 * @param mid     End of the 1st subarray
 * @param mid+1   Start of the 2nd subarray 
 * @param high    End of the 2nd subarray
 */
void merge(int *a, int low, int mid, int high){
  int i, j, k, tempArray[50];
  i = low;
  k = low;
  j = mid + 1;
  while(i <= mid && j <= high){
    if(a[i] < a[j]){
      tempArray[k] = a[i];
      k++;
      i++;
    }
    else{
      tempArray[k] = a[j];
      k++;
      j++;
    }
  }
  /* If the 1st subarray wasn't done */
  while(i <= mid){
    tempArray[k] = a[i];
    k++;
    i++;
  }
  /* If the 2nd subarray wasn't done */ 
  while(j <= high){
    tempArray[k] = a[j];
    k++;
    j++;
  }
  /* Copy the tempArray into the original one */
  for(int i = low ; i < k; i++){
    a[i] = tempArray[i];
  }
}

/*
 * Implements the merge sort algorithm
 * @param a     The array to be sorted
 * @param low   Start of the array
 * @param high  End of the array
*/
void mergeSort(int *a, int low, int high){
  int mid;
  if (low < high){
    mid = (low + high) / 2;
    mergeSort(a,low,mid);
    mergeSort(a,mid+1,high);
    merge(a,low,mid,high);
  }
}

int main(){
    int i;
    int a[10] = {17, 37, 7, 11, 75, 90, 46, 65, 11, 78};

    mergeSort(a, 0, 9);
    
    cout<<"sorted array\n";
    for (i = 0; i < 10; i++){
        cout<<a[i]<<endl;
    }
  return 0;
}
