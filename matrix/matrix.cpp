// Progrm to implement a 2D array (matrix) in C++
#include <iostream>
using std::cout;
using std::endl;

/*
 * Prints the matrix
 */
void printMatrix(int matrix[][5]){
  int row = 5;
  int col = 5;

  for (int i = 0; i < row; i++){
    for (int j = 0; j < col; j++){
      cout << matrix[i][j] << '\t';
    }
    cout << endl;
  }
}

// Test Client
int main(){
  int matrix[][5] ={{1, 2, 3, 4, 5},
                    {6, 7, 8, 9, 10},
                    {11, 12, 13, 14, 15},
                    {16, 17, 18, 19, 20},
                    {21, 22, 23, 24, 25}};
  printMatrix(matrix);
  return 0;
}
