// Program to Implement a 2D array (Matrix)
#include <iostream>
using std::cout;
using std::endl;

/*
 * Function to print the contents of a matrix
 * @param *matrixPtr  Pointer to the matrix
 * @param row         # of rows in the matrix
 * @param col         # of columns in the matrix
 */
void printMatrix(int *matrixPtr, int row, int col){
  for (int i = 0 ; i < row; i++){
    for (int j = 0; j < col ; j++){
      cout << matrixPtr[i * col + j] << endl;
    }
    cout << endl;
  }
}

// test Client
int main(){
  int matrix[][5] ={{1, 2, 3, 4, 5},
                    {6, 7, 8, 9, 10},
                    {11, 12, 13, 14, 15},
                    {16, 17, 18, 19, 20},
                    {21, 22, 23, 24, 25}};
  int *matrixPtr  = (int *)matrix;
  printMatrix(matrixPtr, 5, 5);
  return 0;
}
