/* 
  Program to find the transpose of the matrix
  e.g. [1 2
        3 4   -> [1 3 5
        5 6]      2 4 6]        
*/
#include <iostream>
using std::cout;
using std::endl;

/*
 * Function to print the contents of a matrix
 * @param *matrixPtr  Pointer to the matrix
 * @param row         # of rows in the matrix
 * @param col         # of columns in the matrix
 */
void printMatrix(int *matrixPtr, int row, int col){
  for (int i = 0 ; i < row; i++){
    for (int j = 0; j < col ; j++){
      cout << matrixPtr[i * col + j] << endl;
    }
    cout << endl;
  }
}

/*
 * Function to make transpose of a matrix 
 * @param *matrixPtr      Pointer to the original matrix
 * @param row             # of rows in the original matrix
 * @parma col             # of columns in the original matrix
 * @param newRow          # of rows in the transpose matrix
 * @param newCol          # of columns in the transpose matrix
 * @param transposeMatrix Transpose of the original matrix 
 */
void transpose(int *matrixPtr, int row, int col){
  int newRow = col;
  int newCol = row;
  int transposeMatrix [newRow][newCol];
  for (int i = 0; i < newRow; i++){
    for (int j = 0; j < newCol; j++){
      transposeMatrix[i][j] = matrixPtr[j * col + i];
    }
  }
  int *transposeMatrixPtr = (int *)transposeMatrix;
  printMatrix(transposeMatrixPtr,newRow, newCol);
}

// Test Client
int main(){
  int matrix[3][2] ={{1, 2}, 
                    {3, 4}, 
                    {5, 6}};
  int *matrixPtr = (int *)matrix;
  printMatrix(matrixPtr, 3, 2);
  transpose(matrixPtr, 3, 2);
  return 0;
}
